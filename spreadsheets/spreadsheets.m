function varargout = spreadsheets(varargin)
% SPREADSHEETS MATLAB code for spreadsheets.fig
%      SPREADSHEETS, by itself, creates a new SPREADSHEETS or raises the existing
%      singleton*.
%
%      H = SPREADSHEETS returns the handle to a new SPREADSHEETS or the handle to
%      the existing singleton*.
%
%      SPREADSHEETS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPREADSHEETS.M with the given input arguments.
%
%      SPREADSHEETS('Property','Value',...) creates a new SPREADSHEETS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spreadsheets_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spreadsheets_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spreadsheets

% Last Modified by GUIDE v2.5 11-Dec-2014 09:20:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @spreadsheets_OpeningFcn, ...
                   'gui_OutputFcn',  @spreadsheets_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before spreadsheets is made visible.
function spreadsheets_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to spreadsheets (see VARARGIN)

% Choose default command line output for spreadsheets
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes spreadsheets wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = spreadsheets_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in setpath_btn.
function setpath_btn_Callback(hObject, eventdata, handles)
% hObject    handle to setpath_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Add a handle field for the path, associated with a related figure or
    % figure descendent:
    
    global sPath sorted_names file_no
    
    % guihandles makes it possible to add fields to the handles structure:
    handles = guihandles(handles.path_edit_box);
    % We create a new field and load the corresponding data in it:
    handles.img_path = uigetdir;    
    % We retrieve the value from the handles, and we write it elsewhere.
    set(handles.path_edit_box,'String',handles.img_path);
    % The last line is needed to make the stored values available outside
    % this function:
    guidata(gcf,handles);

    
    sPath = (handles.img_path);
    list    = dir([sPath,'/*xlsx']);           % Look for *.xlsx files only 
    file_no = numel(list);
    
    [sorted_names,sorted_index] = sortrows({list.name}');
    set(handles.path_content_list,'String',sorted_names);
    

    




function path_edit_box_Callback(hObject, eventdata, handles)
% hObject    handle to path_edit_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of path_edit_box as text
%        str2double(get(hObject,'String')) returns contents of path_edit_box as a double


% --- Executes during object creation, after setting all properties.
function path_edit_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to path_edit_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in path_content_list.
function path_content_list_Callback(hObject, eventdata, handles)
% hObject    handle to path_content_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns path_content_list contents as cell array
%        contents{get(hObject,'Value')} returns selected item from path_content_list


% --- Executes during object creation, after setting all properties.
function path_content_list_CreateFcn(hObject, eventdata, handles)
% hObject    handle to path_content_list (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in merge_btn.
function merge_btn_Callback(hObject, eventdata, handles)
% hObject    handle to merge_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global sPath sorted_names file_no



    sheet = 1;
    xlRange = 4:13;
    
    wbar = waitbar(file_no,'in progress...');
    xlswrite([sPath,'\..\','summary.xlsx'],{'size [um]'}, sheet,['A1']);
    for file_ind = 1:file_no
        fName = strcat(sPath,'\',sorted_names(file_ind));
        conc = [];
        size =[];
        num = [];
        for line = 1:length(xlRange)-1
            [~, cells]=xlsread(fName{1}, sheet, ['F',num2str(xlRange(line))]);
            try
            strings = cells{1};
            tconc = str2num(strings( findstr(strings, 'density: ') + 9:length(strings)));
            conc = [conc, tconc];
            tsize = str2num(strings( findstr(strings, 'of ') + 3:findstr(strings, 'um.Surface')-1));
            size = [size, tsize];
%             tnum = str2num(strings( 1:findstr(strings, 'clusters') -1 ));
%             num = [num, tnum];
            catch
                disp('empty line');
            end
        end
%         if (file_ind == 1)
%             xlswrite([sPath,'\','summary.xlsx'],size, sheet,'B1');
%         end


        xlswrite([sPath,'\..\','summary.xlsx'],{'concentration [1/um]'}, sheet,['A',num2str(1+file_ind)]);
        for sind = 1:length(size)
            if size(sind)<0.126
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'B1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['B',num2str(1+file_ind)]);
            elseif size(sind)<0.251
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'C1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['C',num2str(1+file_ind)]);
            elseif size(sind)<0.376
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'D1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['D',num2str(1+file_ind)]);
            elseif size(sind)<0.501
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'E1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['E',num2str(1+file_ind)]);
            elseif size(sind)<0.626
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'F1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['F',num2str(1+file_ind)]);
            elseif size(sind)<0.751
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'G1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['G',num2str(1+file_ind)]);
            elseif size(sind)<0.876
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'H1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['H',num2str(1+file_ind)]);
            elseif size(sind)<1.001
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'I1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['I',num2str(1+file_ind)]);
            elseif size(sind)<1.126
                xlswrite([sPath,'\..\','summary.xlsx'],size(sind), sheet,'J1');
                xlswrite([sPath,'\..\','summary.xlsx'],conc(sind), sheet,['J',num2str(1+file_ind)]);            
            end
        end       
            xlswrite([sPath,'\..\','summary.xlsx'],fName, sheet,['O',num2str(1+file_ind)]);
        waitbar(file_ind/file_no,wbar);
    end    
    close(wbar);

%     xlswrite([sPath,'\','summary.xlsx'],size, sheet,['2',num2str(1+file_ind)]);
%     xlswrite([sPath,'\','summary.xlsx'],num, sheet,['2',num2str(1+file_ind)]);
