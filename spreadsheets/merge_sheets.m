function merge_sheets(sPath, img_file_name)

    list    = dir([[sPath,'/',img_file_name],'/*xlsx']);           % Look for *.xlsx files only 
    file_no = numel(list);
    
    [sorted_names,sorted_index] = sortrows({list.name}');

    sheet = 1;
    xlRange = 4:13;
    
    wbar = waitbar(file_no,'in progress...');
[sPath,'\',img_file_name,'.xlsx']
    xlswrite([sPath,'\',img_file_name,'.xlsx'],{'size [um]'}, sheet,['A1']);
    for file_ind = 1:file_no
        fName = strcat(sPath(1:end-1),img_file_name,'/',sorted_names(file_ind));
        conc = [];
        size =[];
        num = [];
        for line = 1:length(xlRange)-1
            [~, cells]=xlsread(fName{1}, sheet, ['F',num2str(xlRange(line))]);
            try
            strings = cells{1};
            tconc = str2num(strings( findstr(strings, 'density: ') + 9:length(strings)));
            conc = [conc, tconc];
            tsize = str2num(strings( findstr(strings, 'of ') + 3:findstr(strings, 'um.Surface')-1));
            size = [size, tsize];
            catch
%                 disp('empty line');
            end
        end
        xlswrite([sPath,'\',img_file_name,'.xlsx'],{'concentration [1/um]'}, sheet,['A',num2str(1+file_ind)]);
        for sind = 1:length(size)
            if size(sind)<0.126
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'B1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['B',num2str(1+file_ind)]);
            elseif size(sind)<0.251
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'C1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['C',num2str(1+file_ind)]);
            elseif size(sind)<0.376
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'D1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['D',num2str(1+file_ind)]);
            elseif size(sind)<0.501
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'E1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['E',num2str(1+file_ind)]);
            elseif size(sind)<0.626
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'F1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['F',num2str(1+file_ind)]);
            elseif size(sind)<0.751
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'G1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['G',num2str(1+file_ind)]);
            elseif size(sind)<0.876
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'H1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['H',num2str(1+file_ind)]);
            elseif size(sind)<1.001
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'I1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['I',num2str(1+file_ind)]);
            elseif size(sind)<1.126
                xlswrite([sPath,'\',img_file_name,'.xlsx'],size(sind), sheet,'J1');
                xlswrite([sPath,'\',img_file_name,'.xlsx'],conc(sind), sheet,['J',num2str(1+file_ind)]);            
            end
        end       
            xlswrite([sPath,'\',img_file_name,'.xlsx'],fName, sheet,['O',num2str(1+file_ind)]);
        waitbar(file_ind/file_no,wbar);
    end    
    close(wbar);
end
