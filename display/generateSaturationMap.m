function [indexedImg, saturationMap] = generateSaturationMap(inputImgage, levelCount)

    [indexedImg, saturationMap] = gray2ind(inputImgage,levelCount);
    saturationMap(1,:)  =[0,0,1];
    saturationMap(end,:)=[1,0,0];