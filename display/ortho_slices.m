function varargout = ortho_slices(varargin)
% ORTHO_SLICES MATLAB code for ortho_slices.fig
%      ORTHO_SLICES, by itself, creates a new ORTHO_SLICES or raises the existing
%      singleton*.
%
%      H = ORTHO_SLICES returns the handle to a new ORTHO_SLICES or the handle to
%      the existing singleton*.
%
%      ORTHO_SLICES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ORTHO_SLICES.M with the given input arguments.
%
%      ORTHO_SLICES('Property','Value',...) creates a new ORTHO_SLICES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ortho_slices_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ortho_slices_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ortho_slices

% Last Modified by GUIDE v2.5 20-Jul-2016 18:33:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ortho_slices_OpeningFcn, ...
                   'gui_OutputFcn',  @ortho_slices_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ortho_slices is made visible.
function ortho_slices_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ortho_slices (see VARARGIN)

% Choose default command line output for ortho_slices
handles.output = hObject;



% UIWAIT makes ortho_slices wait for user response (see UIRESUME)
% uiwait(handles.figure1);


   set(handles.figure1, 'UserData',[]);
   userData.stop = false;
   set(handles.figure1, 'UserData',userData);
   
   % Update handles structure
    guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = ortho_slices_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in snap_btn.
function snap_btn_Callback(hObject, eventdata, handles)
% hObject    handle to snap_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function axesXY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axesXY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axesXY


% --- Executes during object creation, after setting all properties.
function axesXZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axesXZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axesXZ


% --- Executes during object creation, after setting all properties.
function axesYZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axesYZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axesYZ


% --- Executes on slider movement.
function backgroundSlider_Callback(hObject, eventdata, handles)
% hObject    handle to backgroundSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global orthoBackgroundThr
global XYproj XZproj YZproj
global tmpXYproj tmpXZproj tmpYZproj
global img_max img_min

    img_min = min([min(XYproj(:)),min(XZproj(:)),min(YZproj(:))]);
    img_max = max([max(XYproj(:)),max(XZproj(:)),max(YZproj(:))]);

    set(hObject,'SliderStep',[1, 1/(img_max-img_min)]);

    orthoBackgroundThr = get(handles.backgroundSlider,'Value')*(img_max - img_min) + img_min;

    set(handles.backgroundBox,'String',num2str(orthoBackgroundThr));
    fprintf('%d threshold selected \n',orthoBackgroundThr);
    
    [ tmpXYproj, ~, ~, ~ ] = thresholdHere( XYproj, orthoBackgroundThr);
    [ tmpXZproj, ~, ~, ~ ] = thresholdHere( XZproj, orthoBackgroundThr);
    [ tmpYZproj, ~, ~, ~ ] = thresholdHere( YZproj, orthoBackgroundThr);

    % Display result:
    imshow(tmpXYproj, [orthoBackgroundThr, img_max],'Parent',handles.axesXY, 'InitialMagnification', 'fit');
    imshow(tmpXZproj, [orthoBackgroundThr, img_max],'Parent',handles.axesXZ, 'InitialMagnification', 'fit');
    imshow(tmpYZproj',[orthoBackgroundThr, img_max],'Parent',handles.axesYZ, 'InitialMagnification', 'fit');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [img] = cleanSparks(img, sizeX, sizeY)

    for indX = 2:sizeX-1
        for indY = 2:sizeY-1                        
            neigh = length(find(img(indX-1:indX+1,indY-1:indY+1)>0))-1;
            if (neigh<0)
                neigh=0;
            end
            if (neigh < 3)
                img(indX, indY) = 0;
            end
        end
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function backgroundSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to backgroundSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.

% global XYproj XZproj YZproj
% global img_max img_min 
% global orthoBackgroundThr

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    set(hObject,'Min',0);
    set(hObject,'Max',1);
    set(hObject,'Value',0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function backgroundBox_Callback(hObject, eventdata, handles)
% hObject    handle to backgroundBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of backgroundBox as text
%        str2double(get(hObject,'String')) returns contents of backgroundBox as a double

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global preview sizeX sizeY prev_win_hdl menu_hdlr
global x_vect y_vect thr_img current_ind HYPERSTACK
    

    page = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
    % HD version
    preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
    % RAM version
%     preview = HYPERSTACK(:,:,page);

    % Find dynamic ranges .................................................
    MX = max(max(preview));
    mx = min(min(preview));
    
    % Display images ......................................................
    imshow(preview,[0 max(max(preview))],...
           'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');
    hist(double(reshape(preview,1,sizeX*sizeY)),double(30),...
           'Parent', handles.histogram_axes);
    
    
    % Ask user for a threshold value ......................................
    % (The callback is executed after pressing [enter] so a value should be
    % typed in the box already)
    input = str2double(get(handles.thr_txtbox,'String'));
    
    % And... do it:
    if (input < mx) || (input > MX)
        % Threshold out of range ..........................................
        fprintf(1,'Threshold out of range \n');
        set(handles.thr_txtbox,'String','Out of range');        
        thr = mx;
        set(handles.thr_slider,'Value',thr);
    else
        % Threshold fine ..................................................
        thr = input;                           % Upgrade threshold globally
        set(handles.thr_slider,'Value',thr);   % Outputs: text & slider
        fprintf(1,'%d threshold selected \n',thr);

        % Apply threshold:
        thr_img = preview;                % Apply changes to a buffer image            
        [ thr_img, x_vect, y_vect, ~ ] = threshold( thr_img, thr );
        
        length(x_vect)    % How many points there exist above the threshold
        
        % Display the thresholded (buffer) image ..........................
        imshow(thr_img,[0 max(max(thr_img))],'Parent',handles.image_axes, 'InitialMagnification', 'fit');
        
        % Upgrade image histogram .........................................
        s_buff = reshape(preview,1,sizeX*sizeY);
        s_ind = s_buff > 100;    
        hst = hist(double(s_buff(s_ind)),double(50));
        %Plot histogram. Set a mark at the threshold point
        hist(double(s_buff(s_ind)),double(50),'Parent', handles.histogram_axes);
        line([thr,thr],[0,max(max(hst))],'Parent', handles.histogram_axes,'Color','r');
    end    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
% --- Executes during object creation, after setting all properties.
function backgroundBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to backgroundBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in proceedButton.
function proceedButton_Callback(hObject, eventdata, handles)
% hObject    handle to proceedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    userData = get(handles.figure1, 'UserData');
    userData.stop = true;
    set(handles.figure1,'UserData',userData);
    
% --- Executes during object creation, after setting all properties.
function proceedButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to proceedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on slider movement.
function filterSlider_Callback(hObject, eventdata, handles)
% hObject    handle to filterSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    global tmpXYproj tmpXZproj tmpYZproj
    global img_max img_min

    img_min = min([min(tmpXYproj(:)),min(tmpXZproj(:)),min(tmpYZproj(:))]);
    img_max = max([max(tmpXYproj(:)),max(tmpXZproj(:)),max(tmpYZproj(:))]);
    
    kernSize = fix(get(handles.filterSlider,'Value'));
    
    kern = fspecial('gaussian',kernSize,kernSize);
    
    tmpXYproj = imfilter(tmpXYproj,kern);
    tmpXZproj = imfilter(tmpXZproj,kern);
    tmpYZproj = imfilter(tmpYZproj,kern);

    % Display result:
    imshow(tmpXYproj, [img_min, img_max],'Parent',handles.axesXY, 'InitialMagnification', 'fit');
    imshow(tmpXZproj, [img_min, img_max],'Parent',handles.axesXZ, 'InitialMagnification', 'fit');
    imshow(tmpYZproj',[img_min, img_max],'Parent',handles.axesYZ, 'InitialMagnification', 'fit');

    
    
% --- Executes during object creation, after setting all properties.
function filterSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filterSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

    set(hObject,'Min',1);
    set(hObject,'Max',10);
    set(hObject,'SliderStep',[1, 2]);
    set(hObject,'Value',1);


% --- Executes on button press in denoiseBtn.
function denoiseBtn_Callback(hObject, eventdata, handles)
% hObject    handle to denoiseBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global tmpXYproj tmpXZproj tmpYZproj    
    global img_max img_min
    
    [szX,szZ] = size(tmpXZproj);
    [szY, ~ ] = size(tmpYZproj);
    
    tmpXYproj = cleanSparks(tmpXYproj, szX, szY);
    tmpXZproj = cleanSparks(tmpXZproj, szX, szZ);
    tmpYZproj = cleanSparks(tmpYZproj, szY, szZ);
    
    % Display result:
    imshow(tmpXYproj, [img_min, img_max],'Parent',handles.axesXY, 'InitialMagnification', 'fit');
    imshow(tmpXZproj, [img_min, img_max],'Parent',handles.axesXZ, 'InitialMagnification', 'fit');
    imshow(tmpYZproj',[img_min, img_max],'Parent',handles.axesYZ, 'InitialMagnification', 'fit');

    
