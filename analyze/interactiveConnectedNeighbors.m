function [xVect, yVect] = interactiveConnectedNeighbors()
        
    global InputFile InputImage
    global RunningParameters
    global ApplicationHandles
    global preview thr_img
    global allowedSeparation

    msg = msgbox('Click on your objects following the same order');

    sendWindowUnder(msg, RunningParameters.InfoWindowPosition, RunningParameters.WindowOffsetY);
    drawnow;
    waitfor(msg);        

    targetWin = ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay);
    
    fName = InputFile(RunningParameters.SelectedFile).Name;
    fPath = strcat(InputFile(RunningParameters.SelectedFile).Path,'/masks');
    mkdir(fPath);

    xVect = [];
    yVect = [];

    confirmBox = done();          %This function has beed defined elsewhere
    sendWindowUnder(confirmBox, RunningParameters.InfoWindowPosition, RunningParameters.WindowOffsetY);
    drawnow;       

    confirmHdl  = guidata(confirmBox);
    dd = guidata(confirmHdl.figure1);
    stopPressed = dd.Stop;


    while(~stopPressed)

            try
                dd = guidata(confirmHdl.figure1);
                stopPressed = dd.Stop;

                if(~stopPressed)
                    
                    [y0,x0] = myginput(1,'crosshair',targetWin);

                    xVect = [xVect, x0];
                    yVect = [yVect, y0];

                    text(y0(end), x0(end),num2str(length(xVect)),'Color',[1,1,0],'FontSize',12);
                else
                    close(confirmBox);
                    break;                
                end
                
                
            catch
                % otherwise stopping the loop generates an error
            end        
    end
    
    for regionInd = 1:length(xVect)
        set(0,'RecursionLimit',1000);
thrValue = 0.5;
        [ thr_img, x_vect, y_vect, ~ ] = threshold( preview, thrValue );
        [ x_group, y_group, ~, ~, ~ ] = conn_neigh( xVect(regionInd), yVect(regionInd), x_vect, y_vect, allowedSeparation, [] );
        selection_buffer = [fName(1:length(fName)-strfind(fliplr(fName),'.')),...
            'Region-', num2str(regionInd),...
            'Frame-', num2str(InputImage(RunningParameters.SelectedDisplay).CurrentFrame),...
            'Slice-', num2str(InputImage(RunningParameters.SelectedDisplay).CurrentSlice),...            
            'Channel-', num2str(InputImage(RunningParameters.SelectedDisplay).CurrentChannel),...
            '.mat']
        save(strcat(fPath,'/',selection_buffer), 'x_group', 'y_group');        
    end
    
    clear('x_group');
    clear('y_group');
    
    for channelInd = 1:InputImage(RunningParameters.SelectedDisplay).ChannelNumber        
        for timeInd = 1:InputImage(RunningParameters.SelectedDisplay).FrameNumber
            for sliceInd = 1:InputImage(RunningParameters.SelectedDisplay).SliceNumber
                for regionInd = 1:length(xVect)
                    
                    selection_buffer = [fName(1:length(fName)-strfind(fliplr(fName),'.')),...
                    'Region-', num2str(regionInd),...
                    'Frame-', num2str(timeInd),...
                    'Slice-', num2str(sliceInd),...            
                    'Channel-', num2str(channelInd),...
                    '.mat'];
                    
                    if (exist(strcat(fPath,'/',selection_buffer), 'file') == 2) % ==7 for folders
                        load(strcat(fPath,'/',selection_buffer), 'x_group', 'y_group')
                        figure(1)
                        hold on
                        scatter(-x_group, y_group);
                        pause(0.1);
                    end
                end
            end
        end
    end

    clear('x_group');
    clear('y_group');
    
