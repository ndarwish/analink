function [bitDepth, maxRange] = findPixDepth(inputImgObject)

    switch inputImgObject.Bitdepth
        case 'uint8'
            bitDepth = 8;
            maxRange = 255;
        case 'uint16'
            bitDepth = 16;
            maxRange = 65535;
        case 'uint32'
            bitDepth = 32;
            maxRange = 4.294967295000000e+09;            
        case 'uint64'
            bitDepth = 64;
            maxRange = 1.844674407370955e+19;
    end   