function [segmentedImage] = entropySegment(entropyThreshold)

	segmentedImage = segmentByTexture(inputImage, entropyThreshold,'entropy');
           