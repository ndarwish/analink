function [ x_group, y_group, x_vect, y_vect, neighb ] = connNeighLimitedRecursion( x0, y0, x_vect, y_vect, dist, neighb, recursionLevelAvail)
%cloud finds areas on the image connected to a given point
%   From a set of coordinate vectors (x_vect, y_vect) it finds the elements 
%   forming a cloud of neighbors

%   As a recursive function, input and output arguments coincide. Some
%   values might be replaced by [] the first time the function is called.

%   x_group, y_group: Mask coordinates (i.e. connected locations)
%   dist:             Maximum distance at which two points are considered
%                     connected.
%   x0, y0:           Startup location. The other points should be
%                     connected to it.
%   neighb:           number of neighbors for each point in the set.


    global mask
            
    lx = length(x_vect);
    d2 = dist^2;
    x_group = [x0];
    y_group = [y0];
 
    if(recursionLevelAvail < 2)
        return;
    else
        recursionLevelAvail = recursionLevelAvail -1;
        for k = 1:length(x0)

            % List points close to (x0,y0) ....................................

            ind = find(((x_vect-x0(k)).^2+(y_vect-y0(k)).^2) <= d2);

            if ~isempty(ind)

                % Store the values found ......................................

                x1 = x_vect(ind);
                y1 = y_vect(ind);
                x_group = [x_group, x1];
                y_group = [y_group, y1];            
                neighb = [neighb, length(ind)];


                % crop the original elements from the check list

                x_vect(ind)=[];
                y_vect(ind)=[];

                % Add whites to the mask on the newer positions

                mask(x1,y1) = 1;

                % Beautiful but useless .......

                % Without specifying figures, plots are updated on the current
                % Beautiful but useless

                figure(88)
                    hold on;                
                    scatter(y1,x1,1,'filled');
                    pause(0.01);

                % find more elements from the last additions

                % Repeat the operation for the newer elements and the
                % shortened check lists

                

                [x2,y2, x_vect, y_vect, neighb] = connNeighLimitedRecursion(x1, y1, x_vect, y_vect, dist, neighb, recursionLevelAvail);
                x_group = [x_group, x2];
                y_group = [y_group, y2];            
            end
        end
    end

