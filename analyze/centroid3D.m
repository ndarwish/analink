function [ centroid ] = centroid3D( coordinates, weigths )
%Centroid3D Calculates the centroid of a cloud of dots
% coordinates is a matrix [x1, y1, z1; x2, y2, z2 ...] of the points
% weigths are the values, like the actual weights for a center of mass or
% the intensities for a centroid on an image.

    location_no = length(coordinates); 
    overal_weigth = sum(weigths);
    
    centroid = zeros(1,3);

    X = sum(coordinates(1,:).*weigths')/overal_weigth;
    Y = sum(coordinates(2,:).*weigths')/overal_weigth;
    Z = sum(coordinates(3,:).*weigths')/overal_weigth;

    centroid = [X, Y, Z];

end

