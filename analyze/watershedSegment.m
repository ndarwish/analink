function [ outputImage ] = watershedSegment( inputImg, threshold )
    
    global RunningParameters InputImage
    
    [~, maxRange] = findPixDepth(InputImage(RunningParameters.SelectedDisplay));

    outputImage = im2bw(double(inputImg)./maxRange, double(threshold)/double(maxRange));

    distTranf = -bwdist(~outputImage);
    
    distTranf(~outputImage) = -Inf;
    
    %50 height threshold for avoiding oversegmentation
    distTranf = imhmin(distTranf,50);     
    labelMatrix = watershed(distTranf);
        
    outputImage =  labelMatrix;
	outputImage(labelMatrix == 0) = 0;
    
    
% figure(12)
%     imshow(outputImage,[]);
end

