function [assignedPoints] = clusterGrouping(filename, pathname)

    global thresholdDistance
    
%     addpath(pwd);
%     clc

    %% set threshold for grouping distance

    if (isempty(thresholdDistance))
        fprintf(1,'setting default threshold distance of 100\n');
        thresholdDistance = 100;
    end
    
    %% Importing multiple csv files and extracting one column of data to form a single matrix

    mat = importdata([pathname,'\',filename]);
    dat = mat.data;                 

    xVec = dat(:,2);
    yVec = dat(:,3);
  
    currentPoint     = [xVec(1),yVec(1)];    
    unassignedX      = xVec(2:end); %start at position 2, as 1rst is set above
    unassignedY      = yVec(2:end); %start at position 2, as 1rst is set above

    currentGroupLabel = 1;  %initialize group label (before running the recursive loop)

	unassignedPoints = [unassignedX,unassignedY];
    assignedPoints   = [currentPoint(1), currentPoint(2), currentGroupLabel];
    [RemainingUnassignedPoints, assignedPoints] =  groupAssignment(currentPoint, unassignedPoints, assignedPoints);
    
    while(~isempty(RemainingUnassignedPoints))
            
    	currentGroupLabel = currentGroupLabel + 1;
        buffer = [RemainingUnassignedPoints(1,1), RemainingUnassignedPoints(1,2), currentGroupLabel];
        assignedPoints = [assignedPoints; buffer];
        RemainingUnassignedPoints(1,:) = [];
            
        [RemainingUnassignedPoints, assignedPoints] = groupAssignment([assignedPoints(end,1),assignedPoints(end,2)], RemainingUnassignedPoints, assignedPoints);
    end
    

    
    
    
end    


%%

function [distance] = euclideanDistance(x1,x2,y1,y2)
    distance = sqrt((x2-x1)^2+(y2-y1)^2);
end

%%

function [SameGroup] =  testDistance(x1, x2, y1, y2)
    global thresholdDistance
    
    distance = euclideanDistance(x1,x2,y1,y2); % call function2 
    if (distance < thresholdDistance)
        SameGroup = true;
    else
        SameGroup = false;
    end
end 

%%

function[RemainingUnassignedPoints, assignedPoints] = groupAssignment(currentPoint, unassignedPoints, assignedPoints)

    RemainingUnassignedPoints = unassignedPoints;
    
    
    if(isempty(unassignedPoints))
        return;
    else
        groupIndex = assignedPoints(end,3);
        
        x1 = currentPoint(1);
        y1 = currentPoint(2);
        
        unassignedIndex = 1; %set first index to 1
        
        while(unassignedIndex <= length(RemainingUnassignedPoints(:,1))) %do as long as there are elements

            x2 = RemainingUnassignedPoints(unassignedIndex,1);
            y2 = RemainingUnassignedPoints(unassignedIndex,2);

            SameGroup = testDistance(x1, x2, y1, y2); % 2 determine if belong to current group

            if (SameGroup)
                
                % really not necessary part ...............................
                figure(1)
                clf;
                hold on
                axis equal;
                drawLimitDisc(assignedPoints(end,1),assignedPoints(end,2));
                gscatter(assignedPoints(:,1),assignedPoints(:,2),assignedPoints(:,3));                
                pause(0.01);
                % end of really not necessary part ........................
                
                buffer = [x2, y2, groupIndex];
                assignedPoints = [assignedPoints; buffer];
                RemainingUnassignedPoints(unassignedIndex,:) = [];% 5b delete from unassigned list

                [RemainingUnassignedPoints, assignedPoints] = groupAssignment([x2,y2], RemainingUnassignedPoints, assignedPoints);

            else
                unassignedIndex = unassignedIndex + 1;
            end                    
        end        
    end
end

function [] = drawLimitDisc(x0,y0)

    global thresholdDistance
    
    t=linspace(0,2*pi);
    color = [1 1 1];
    fill(x0+thresholdDistance*cos(t),y0+thresholdDistance*sin(t),color);                

end
