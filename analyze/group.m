a = [1 0 1 0 ; 0 1 0 0; 1 0 0 1]


outputMatrix = groupMatrix(a, 0)

function outputMatrix = groupMatrix(inputMatrix, groupInd)

    [sz1, ~] = size(inputMatrix);

    outputMatrix = inputMatrix;    
    
    for i = 1:sz1
        for j = 1:sz1
            if (inputMatrix(i,j) > 0)
                
                if(groupInd == 0)
                    groupInd = 1;
                end               
                 
                outputMatrix(i,j) = groupInd;
                outputMatrix(i,j) = outputMatrix(j,i);

                outputMatrix = group(inputMatrix, groupInd);
            else
                groupInd = groupInd + 1;
            end
        
        end
    end
end
