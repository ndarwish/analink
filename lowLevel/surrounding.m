function [ xMinus, xPlus, yMinus, yPlus ] = surrounding( x0, y0, delta )

    global preview

    [width, height] = size(preview);
    
%     width = preview.getSizeX();
%     height = preview.getSizeY();
    
    xm = max(x0-delta, 1);
    ym = max(y0-delta, 1);


    
    xM = min(x0+delta, width );
    yM = min(y0+delta, height );
    
    xMinus = x0 - xm;
    yMinus = y0 - ym;
    xPlus  = xM - x0;
    yPlus  = yM - y0;
    
end

