function [ proj ] = XYprojection_MIP(box)
% XZprojection: displays the orthogonal projection of a given 3D data set

    [box_sz_X, box_sz_Y, ~] = size(box);

    for x_ind = 1:box_sz_X
    	for y_ind = 1:box_sz_Y
        	proj(x_ind, y_ind) = uint16(max(box(x_ind, y_ind, :)));
        end
    end
end