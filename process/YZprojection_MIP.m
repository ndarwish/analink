function [ proj ] = YZprojection_MIP(box)
%XZprojection: displays the orthogonal projection of a given 3D data set

    [~, sY, sZ] = size(box);
    for y_ind =1:sY
        for z_ind =1:sZ
            proj(y_ind,z_ind) = uint16(max(box(:,y_ind,z_ind)));               % Maximum intensity projection
        end
    end
end

