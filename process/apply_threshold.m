function apply_threshold(value)

global page_no img_path img_name img_data sizeX sizeY 
global prev_win_hdl HYPERSTACK
global log_file_name thr

thr_backup = thr;
thr = value;

thresholded_img_name = [img_name,' - thresholded','.tif'];
binary_img_name      = [img_name,' - binary','.tif'];

Tiff([img_path,'/',thresholded_img_name],'w');

% write headers
write_metadata( img_path, thresholded_img_name, img_data);
write_metadata( img_path, binary_img_name, img_data);

wbar = waitbar(page_no,['page 1 of',num2str(page_no)]); % Progress bar
movegui(wbar, [-50,-550]);


for page = 1:page_no
    % HD version
%     preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
    % RAM version
    preview = HYPERSTACK(:,:,page);
    [ thr_img, maskX, maskY, positives ] = threshold( preview, thr );
    bin_img = zeros(sizeY, sizeX);
    for ind = 1:positives
        bin_img(maskX(ind),maskY(ind)) = 1;
    end
%     imshow(bin_img,[0 1],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');

    imwrite(thr_img,[img_path,'/',thresholded_img_name],'WriteMode','append', 'Compression','none');
    imwrite(bin_img,[img_path,'/',binary_img_name],'WriteMode','append', 'Compression','none');

    waitbar((page)/(page_no),wbar,...                    % Progress bar
            ['page',num2str(page),' of ',num2str(page_no),'.']);
end
close(wbar);
imshow(thr_img,[0 max(max(thr_img))],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');
thr = thr_backup;

    log_hdl = fopen([img_path,'\',log_file_name],'a+');
    fprintf(log_hdl,'THRESHOLD THR:%d',thr);
    fprintf(log_hdl,'\n');
    fclose(log_hdl);
