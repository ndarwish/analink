function [ segmentedImage, vertexSubset] = commitEntropySegmentation()

global InputImage InputFile
global preview 
global ApplicationHandles
global RunningParameters
global sigma siz Threshold areaThreshold
global strelSize

    preview = imread([InputFile(RunningParameters.SelectedDisplay).Path,...
        '\',InputFile(RunningParameters.SelectedFile).Name],...
        'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);   
    origView = preview;
    % Smoothing block:

    preview = imgaussian(preview,sigma,siz);
    InputImage(RunningParameters.SelectedDisplay).Buffer = preview;  

    % Threshold block:
    
    [ preview, ~, ~, ~ ] = threshold( preview, Threshold );

    % Entropy segmentation:
    RunningParameters.SegmentationMethod = 'entropy';

    topVal = max(InputImage(RunningParameters.SelectedDisplay).Buffer(:));
    scaledThreshold = Threshold/double(topVal);

    [segmentedImage, ~] = segmentEntropy(InputImage(RunningParameters.SelectedDisplay).Buffer, scaledThreshold, areaThreshold);
    % strelSize: 6 works most of the times.
se6 = strel('cube',strelSize); 
segmentedImage = imopen(segmentedImage, se6);

	[~,vertexSubset] = findBorders(segmentedImage);    
        
    mask = im2bw(segmentedImage,0.001);
    mask = filterIslandsBySize(mask, fix(areaThreshold));

    [segmentedImage, ~] = segmentEntropy(segmentedImage.*uint8(mask), 0, areaThreshold);       
    
    hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');    
        imshow(origView,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
        scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'r');
	hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');
                    
end

