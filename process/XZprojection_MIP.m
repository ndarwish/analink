function [ proj ] = XZprojection_MIP(box)
% XZprojection: displays the orthogonal projection of a given 3D data set

    [box_sz_X, ~, box_sz_Z] = size(box);
        
    for x_ind = 1:box_sz_X
        for z_ind = 1:box_sz_Z
            proj(x_ind, z_ind) = uint16(max(box(x_ind, :, z_ind)));
        end
    end        
end