function varargout = entropySegmentSliders(varargin)
% ENTROPYSEGMENTSLIDERS MATLAB code for entropySegmentSliders.fig
%      ENTROPYSEGMENTSLIDERS, by itself, creates a new ENTROPYSEGMENTSLIDERS or raises the existing
%      singleton*.
%
%      H = ENTROPYSEGMENTSLIDERS returns the handle to a new ENTROPYSEGMENTSLIDERS or the handle to
%      the existing singleton*.
%
%      ENTROPYSEGMENTSLIDERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENTROPYSEGMENTSLIDERS.M with the given input arguments.
%
%      ENTROPYSEGMENTSLIDERS('Property','Value',...) creates a new ENTROPYSEGMENTSLIDERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before entropySegmentSliders_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to entropySegmentSliders_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help entropySegmentSliders

% Last Modified by GUIDE v2.5 07-Aug-2018 20:40:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @entropySegmentSliders_OpeningFcn, ...
                   'gui_OutputFcn',  @entropySegmentSliders_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before entropySegmentSliders is made visible.
function entropySegmentSliders_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to entropySegmentSliders (see VARARGIN)

% Choose default command line output for entropySegmentSliders
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes entropySegmentSliders wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = entropySegmentSliders_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function SigmaSlider_Callback(hObject, eventdata, handles)
% hObject    handle to SigmaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function SigmaSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SigmaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function kernelSlider_Callback(hObject, eventdata, handles)
% hObject    handle to kernelSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global siz

    %Read slider position and convert it to a sigma value:
%     siz = fix(get(hObject,'Value'));
%     set(handles.kernelBox,'String',num2str(siz));
%     fprintf('Kernel size: %d \n',siz);
%     
    
%     doIt(handles);
    commitEntropySegmentation( );

% --- Executes during object creation, after setting all properties.
function kernelSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kernelSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function sigmaSlider_Callback(hObject, eventdata, handles)
% hObject    handle to sigmaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global sigma

    %Read slider position and convert it to a sigma value:
    sigma = get(hObject,'Value');
    set(handles.sigmaBox,'String',num2str(sigma));
    fprintf('Sigma: %d \n',sigma);

%     doIt(handles);
    commitEntropySegmentation( );

% --- Executes during object creation, after setting all properties.
function sigmaSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigmaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function sigmaBox_Callback(hObject, eventdata, handles)
% hObject    handle to sigmaBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigmaBox as text
%        str2double(get(hObject,'String')) returns contents of sigmaBox as a double


% --- Executes during object creation, after setting all properties.
function sigmaBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigmaBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String','0');


function kernelBox_Callback(hObject, eventdata, handles)
% hObject    handle to kernelBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kernelBox as text
%        str2double(get(hObject,'String')) returns contents of kernelBox as a double


% --- Executes during object creation, after setting all properties.
function kernelBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kernelBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String','0');


% --- Executes on slider movement.
function thresholdSlider_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global Threshold

    %Read slider position and convert it to a sigma value:
    Threshold = get(hObject,'Value');
    set(handles.thresholdBox,'String',num2str(Threshold));
    fprintf('Threshold: %d \n',Threshold);

%     doIt(handles);
    commitEntropySegmentation( );

% --- Executes during object creation, after setting all properties.
function thresholdSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global preview Threshold RunningParameters InputFile InputImage
global img_max

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

preview = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedDisplay).Name],...
    'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);  

Threshold = 0;                                           % Set threshold to 0

img_min = min(preview(:));
img_max = max(preview(:));

set(hObject,'Value',Threshold);
set(hObject,'Max',fix(img_max));

set(hObject,'SliderStep',[1, 1/(double(img_max)-double(img_min))]);
set(hObject,'Value',Threshold);

function thresholdBox_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of thresholdBox as text
%        str2double(get(hObject,'String')) returns contents of thresholdBox as a double


% --- Executes during object creation, after setting all properties.
function thresholdBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thresholdBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function areaSlider_Callback(hObject, eventdata, handles)
% hObject    handle to areaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global areaThreshold
       
    LogAreaPixVal = get(hObject,'Value');
    areaThreshold = ceil(exp(double(LogAreaPixVal)));
    set(handles.areaBox,'String',num2str(areaThreshold));  
    
%     doIt(handles);
    commitEntropySegmentation( );


% --- Executes during object creation, after setting all properties.
function areaSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.

global areaThreshold InputImage RunningParameters

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

    minPixArea = 1;
    maxPixArea = InputImage(RunningParameters.SelectedDisplay).SizeX * ...
                 InputImage(RunningParameters.SelectedDisplay).SizeY;

    lMin = log(double(minPixArea));
    lMax = log(double(maxPixArea));
    
    areaThreshold = 1;
        
	set(hObject,'Min',lMin);
    set(hObject,'Max',lMax);
    
    set(hObject,'SliderStep',[1/(lMax-lMin), 0.1]);
    set(hObject,'Value',areaThreshold);
    



function areaBox_Callback(hObject, eventdata, handles)
% hObject    handle to areaBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of areaBox as text
%        str2double(get(hObject,'String')) returns contents of areaBox as a double


% --- Executes during object creation, after setting all properties.
function areaBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

