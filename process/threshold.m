function [ th_img, x_vect, y_vect, count ] = threshold( input_img, thr )

    %THRESHOLD: Threshold frames and returns the masks        
    
    x_vect         = [];
    y_vect         = [];
    th_img = uint16(input_img);
    count = 0;

    [sX, sY] = size(input_img);
    
    for x_ind = 1:sX
        for y_ind = 1:sY
            if (input_img(x_ind,y_ind) < thr)
                th_img(x_ind,y_ind) = 0;
            else
                count = count + 1;
                x_vect = [x_vect, x_ind];       % A list of coordinates for
                y_vect = [y_vect, y_ind];       % the non null locations
            end       
        end
    end
end
