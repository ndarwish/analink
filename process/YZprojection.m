function [ block ] = YZprojection(box)
%XZprojection: displays the orthogonal projection of a given 3D data set

    [~, sY, sZ] = size(box);
    for y_ind =1:sY
        for z_ind =1:sZ
            block(y_ind,z_ind) = uint16(mean(box(:,y_ind,z_ind)));               % mean projection
        end
    end
end

