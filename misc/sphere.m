function [ matrix ] = sphere( msize, diameter )
%SPHERE Builds a matrix of range 'range', of the given 'size' and
%containing an sphere of radius 'radius'
      
    matrix = zeros(msize,msize,msize);
    if mod(msize,2 ~=0)
        center = fix(msize/2)+1;
    else
        center = msize/2 + 0.5;
    end
    for X = 1:msize
        for Y = 1:msize
            for Z = 1:msize
                if ((X-center)^2+(Y-center)^2+(Z-center)^2 <= diameter/2)
                    matrix(X,Y,Z) = 1;
                end     
            end
        end
    end

end

