function str = excel_cols(x,pow)
    % EXCEL_COLS: For a column number x it calculates the corresponding 
    % string on an Excel file: A, B, ...Z, AA, AC, ...AZ, BA, ...,AAA ...

    % A recursive base change plus an offset converts the numbers into ascii
    % strings:
    %         char(97) = 'a';
    %         char(65) = 'A';
    %         fix('AC') = [ 65 67]
    
    % The argument 'pow' must be 0 when calling this function from another.
    
    base = 27;
    
    if(x < base^pow)
        str = [char(x+64)];
    else
        if (pow == 0)
            str = [excel_cols(fix(x/(base^pow)),pow+1)];
        else
            str = [excel_cols(fix(x/(base^pow)),pow+1),...
                   char(mod(x,base^pow)+65)];
        end
    end
end