function varargout = all_sliders(varargin)
% ALL_SLIDERS MATLAB code for all_sliders.fig
%      ALL_SLIDERS, by itself, creates a new ALL_SLIDERS or raises the existing
%      singleton*.
%
%      H = ALL_SLIDERS returns the handle to a new ALL_SLIDERS or the handle to
%      the existing singleton*.
%
%      ALL_SLIDERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ALL_SLIDERS.M with the given input arguments.
%
%      ALL_SLIDERS('Property','Value',...) creates a new ALL_SLIDERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before all_sliders_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to all_sliders_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help all_sliders

% Last Modified by GUIDE v2.5 06-Apr-2016 16:08:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @all_sliders_OpeningFcn, ...
                   'gui_OutputFcn',  @all_sliders_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before all_sliders is made visible.
function all_sliders_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to all_sliders (see VARARGIN)

% Choose default command line output for all_sliders
    global RunningParameters
    global ApplicationHandles

    handles.output = hObject;
    ApplicationHandles.SliderWindow = hObject;
    
    set(hObject,'units','pixels');
    % In case that the reference window was moved:    
    
%     sendWindowUnder(hObject,...
%                     RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
%                     RunningParameters.WindowOffsetY);
%     % The position changes, so the value must be upgraded
    RunningParameters.SliderWindowPosition  = get(hObject, 'Outerposition');
    
if (length(RunningParameters.displayWindows)>1)
    shiftWindowRight(ApplicationHandles.SliderWindow,RunningParameters.SliderWindowPosition,RunningParameters.WindowOffsetX);    
else
    sendWindowUnder(ApplicationHandles.SliderWindow,...
    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
    RunningParameters.WindowOffsetY);
end
    % Update handles structure
    guidata(hObject, handles);
    
% Set the an appropriate window title.
set(ApplicationHandles.SliderWindow,'Name','Page selection');
% UIWAIT makes all_sliders wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = all_sliders_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function main_slider_Callback(hObject, eventdata, handles)
% hObject    handle to main_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    global val
    global InputImage InputFile
    global RunningParameters
%     global InternalData
    global ApplicationHandles
    
    jButton = findjobj(hObject);
    jButton.MouseWheelMovedCallback = {@myMouseWheelFcn, handles};
    % fieldnames(jButton); % To get a list of the available fields
    
    val = fix(get(hObject,'Value'));
    val = round(val);
    set(handles.main_slider,'Value',val);
    set(handles.main_value_box,'String',num2str(val));
    guidata(hObject, handles);

    if (RunningParameters.SliderSelectionMode == 'T')
        InputImage(RunningParameters.SelectedDisplay).CurrentFrame = val;
    elseif (RunningParameters.SliderSelectionMode == 'Z')               
        InputImage(RunningParameters.SelectedDisplay).CurrentSlice = val;
    elseif (RunningParameters.SliderSelectionMode == 'C')
        InputImage(RunningParameters.SelectedDisplay).CurrentChannel = val;
    end
    
    switch (char(InputImage(RunningParameters.SelectedDisplay).StackOrder))
        case 'XYCZT'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        case 'XYCTZ'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        case 'XYZCT'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        case 'XYZTC'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        case 'XYTCZ'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        case 'XYTZC'
            indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
        otherwise
            disp('Missing stack order info');
    end
    % Select the right file for the corresponding slider
    RunningParameters.SelectedFile = RunningParameters.SelectedDisplay;
    if(RunningParameters.RAM == 1)

        InputImage(RunningParameters.SelectedDisplay).Buffer = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,...
                   InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
                            
        imshow(InputImage(RunningParameters.SelectedDisplay).Buffer,[0 max(InputImage(RunningParameters.SelectedDisplay).Buffer(:))],...
        	'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
    elseif(RunningParameters.RAM == 0)

        try
            InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedDisplay).Path,'\',InputFile(RunningParameters.SelectedFile).Name],...
            'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
        catch %OME TIFFS are different files, but the file name changes%

        	zInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentSlice);
            tInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentFrame);
            cInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentChannel);
            returnName = findOmeTiffFile(InputFile(RunningParameters.SelectedFile).Path, zInd, tInd, cInd);
                                
            InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',returnName]);    
        end
    end


function myMouseWheelFcn(hObject, eventData, handles)

    global maximum minimum val
    global InputImage InputFile
    global RunningParameters    
    global ApplicationHandles
           
    % fielnames(eventData); To get a list of the possible fields
    rotation = get(eventData,'WheelRotation');
    tval = val+rotation;
    if (minimum <= tval && tval <= maximum)
        val = tval;
        upgradeInterface(handles, val);
    
        if (RunningParameters.SliderSelectionMode == 'T')
            InputImage(RunningParameters.SelectedDisplay).CurrentFrame = val;
        elseif (RunningParameters.SliderSelectionMode == 'Z')
            InputImage(RunningParameters.SelectedDisplay).CurrentSlice = val;
        elseif (RunningParameters.SliderSelectionMode == 'C')
            InputImage(RunningParameters.SelectedDisplay).CurrentChannel = val;
        end     

        switch (char(InputImage(RunningParameters.SelectedDisplay).StackOrder))
            case 'XYCZT'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYCTZ'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYZCT'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYZTC'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYTCZ'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYTZC'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            otherwise
                disp('Missing stack order info');
        end        
        if(RunningParameters.RAM == 1)
            InputImage(RunningParameters.SelectedDisplay).Buffer = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
        elseif(RunningParameters.RAM == 0)

            try
               if(InputFile(RunningParameters.SelectedFile).isOMETiff); 
                    disp('OME TIFF file');
                    zInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentSlice);
                    tInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentFrame);
                    cInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentChannel);
                    returnName = findOmeTiffFile(InputFile(RunningParameters.SelectedFile).Path, zInd, tInd, cInd);                
                	InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',returnName]);                   
               else                   
                    InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],...
                    'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
               end
            catch
                if(strfind(InputFile(RunningParameters.SelectedFile).Name, 'nd2'));
                    disp('nd2 file');

                    %InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],...
                    %'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);

                    InputImage(RunningParameters.SelectedDisplay).Buffer = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,...
                    InputImage(RunningParameters.SelectedFile).CurrentIndex);
                                                
                elseif(InputFile(RunningParameters.SelectedFile).isOMETiff);
                    disp('OME TIFF file');
                    zInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentSlice);
                    tInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentFrame);
                    cInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentChannel);
                    returnName = findOmeTiffFile(InputFile(RunningParameters.SelectedFile).Path, zInd, tInd, cInd);                
                	InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',returnName]);
                end
            
           

            end            
        end
        imshow(InputImage(RunningParameters.SelectedFile).Buffer,[0 max(InputImage(RunningParameters.SelectedFile).Buffer(:))],...
               'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
        % Imshow moves the focus to the display window. We need to bring it
        % back to the slider so the mouse wheel still interacts with it.
        uicontrol(ApplicationHandles.MainSlider);        
    else        
        fprintf('Ignoring values out of range.\n');        
    end
        
function upgradeInterface(~, val)

    global ApplicationHandles
    sliderLocalHandler = guidata(ApplicationHandles.SliderWindow);
    set(sliderLocalHandler.main_slider,'Value',val);
    set(sliderLocalHandler.main_value_box,'String',num2str(val));       

% --- Executes during object creation, after setting all properties.
function main_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to main_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

    global maximum minimum
    global ApplicationHandles
    global InputImage
    global RunningParameters    
    
    ApplicationHandles.MainSlider = hObject;
    maximum = InputImage(RunningParameters.SelectedDisplay).PageNumber;
    minimum = 1;
    set(hObject,'Min',minimum);
    set(hObject,'Max',maximum);
    set(hObject,'SliderStep',[1, 1/maximum]);
    set(hObject,'Value',minimum);
    
    guidata(hObject, handles);

function main_value_box_Callback(hObject, eventdata, handles)
% hObject    handle to main_value_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of main_value_box as text
%        str2double(get(hObject,'String')) returns contents of main_value_box as a double

    global maximum minimum val
    global InputImage InputFile
    global RunningParameters
    global ApplicationHandles
    
    val = fix(str2num(get(hObject,'String')));
    % fields(handles) % to see what's available
    if (minimum<= val && val <= maximum)
        set(handles.main_slider,'Value',val);

        if (RunningParameters.SliderSelectionMode == 'T')
            InputImage(RunningParameters.SelectedDisplay).CurrentSlice = val;
        elseif (RunningParameters.SliderSelectionMode == 'Z')
            InputImage(RunningParameters.SelectedDisplay).CurrentSlice = val;
        elseif (RunningParameters.SliderSelectionMode == 'C')
            InputImage(RunningParameters.SelectedDisplay).CurrentChannel = val;
        end     
                
        switch (char(InputImage(RunningParameters.SelectedDisplay).StackOrder))
            case 'XYCZT'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYCTZ'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYZCT'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentFrame];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYZTC'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYTCZ'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentChannel,InputImage(RunningParameters.SelectedDisplay).CurrentSlice];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            case 'XYTZC'
                indices = [InputImage(RunningParameters.SelectedDisplay).CurrentFrame,InputImage(RunningParameters.SelectedDisplay).CurrentSlice,InputImage(RunningParameters.SelectedDisplay).CurrentChannel];
                InputImage(RunningParameters.SelectedDisplay).CurrentIndex = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(indices(1),indices(2),indices(3));
            otherwise
                disp('Missing stack order info');
        end        
    else
        fprintf(1,'Ignoring input out of range.\n');
    end
    if (RunningParameters.RAM == 1)
        InputImage(RunningParameters.SelectedDisplay).Buffer = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,...
            InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
    elseif(RunningParameters.RAM == 0)
        if(InputFile(RunningParameters.SelectedFile).isOMETiff);
        	disp('OME TIFF file');
            zInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentSlice);
            tInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentFrame);
            cInd = num2str(InputImage(RunningParameters.SelectedDisplay).CurrentChannel);
            returnName = findOmeTiffFile(InputFile(RunningParameters.SelectedFile).Path, zInd, tInd, cInd);                
            InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',returnName]);
        else
            % InputFile(RunningParameters.SelectedFile).Name            
            InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile.Path,'\',InputFile.Name],...
            'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
        end                
    end
    imshow(InputImage(RunningParameters.SelectedDisplay).Buffer,[0 max(InputImage(RunningParameters.SelectedDisplay).Buffer(:))],...
        'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));


% --- Executes during object creation, after setting all properties.
function main_value_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to main_value_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    
    global ApplicationHandles
    
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    ApplicationHandles.MainValueBox = hObject;
    guidata(hObject, handles);

% --- Executes on selection change in slider_menu.
function slider_menu_Callback(hObject, eventdata, handles)
% hObject    handle to slider_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns slider_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from slider_menu

global ApplicationHandles
global InputImage
global maximum minimum
global RunningParameters

contents = cellstr(get(hObject,'String'));
selection = contents{get(hObject,'Value')};

if (strcmp(selection,'time frame'))
    RunningParameters.SliderSelectionMode = 'T';
    set(ApplicationHandles.MaxString,'String',InputImage(RunningParameters.SelectedDisplay).FrameNumber);
    minimum = 1;
    maximum = InputImage(RunningParameters.SelectedDisplay).FrameNumber;
        
elseif(strcmp(selection,'Z slice'))
    RunningParameters.SliderSelectionMode = 'Z';
    set(ApplicationHandles.MaxString,'String',InputImage(RunningParameters.SelectedDisplay).SliceNumber);
    minimum = 1;
    maximum = InputImage(RunningParameters.SelectedDisplay).SliceNumber;
elseif(strcmp(selection,'channel'))    
    RunningParameters.SliderSelectionMode = 'C';
    set(ApplicationHandles.MaxString,'String',InputImage(RunningParameters.SelectedDisplay).ChannelNumber);
    minimum = 1;
    maximum = InputImage(RunningParameters.SelectedDisplay).ChannelNumber;
end
set(ApplicationHandles.MainSlider,'Min',minimum);
set(ApplicationHandles.MainSlider,'Max',maximum);
set(ApplicationHandles.MainSlider,'SliderStep',[1/maximum, 1/maximum]);
set(ApplicationHandles.MainSlider,'Value',minimum);

% --- Executes during object creation, after setting all properties.
function slider_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    global ApplicationHandles
    ApplicationHandles.SliderMenu = hObject;


% --- Executes during object creation, after setting all properties.
function minimum_textbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minimum_textbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global ApplicationHandles
    ApplicationHandles.MinString = hObject;
    guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function maximum_textbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maximum_textbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global ApplicationHandles
    ApplicationHandles.MaxString = hObject;
    guidata(hObject, handles);    


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
