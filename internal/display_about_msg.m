function [] = display_about_msg()
%display_about_msg Show copyright information
    
    global ApplicationHandles 
    
    message = {'Bioimaging Facility';'IST Austria' }';
    ApplicationHandles.AboutMessageBox = msgbox(message);

end

