function [ ] = display_collapsed(axe)
%DISPLAY_COLLAPSED show copllapsed views of the current stack and allow
%snapshots

    global current_ind tracks slider_hdlr menu_hdlr show_additional_hdl
    
    
    show_additional_win = show_additional();
    show_additional_position = get(show_additional_win, 'Position'); % Store position to place other windows
    show_additional_hdl  = guidata(show_additional_win);
    upgrade_collapsed_view(current_ind, menu_hdlr, axe);
end

