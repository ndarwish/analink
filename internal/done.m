function varargout = done(varargin)
% DONE MATLAB code for done.fig
%      DONE, by itself, creates a new DONE or raises the existing
%      singleton*.
%
%      H = DONE returns the handle to a new DONE or the handle to
%      the existing singleton*.
%
%      DONE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DONE.M with the given input arguments.
%
%      DONE('Property','Value',...) creates a new DONE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before done_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to done_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help done

% Last Modified by GUIDE v2.5 06-Jun-2017 14:56:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @done_OpeningFcn, ...
                   'gui_OutputFcn',  @done_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before done is made visible.
function done_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to done (see VARARGIN)

% Choose default command line output for done

global ApplicationHandles

    handles.output = hObject;
    handles.Stop = false;
    ApplicationHandles.ConfirmWindow = hObject;

    set(hObject,'units','pixels');

    % Update handles structure    
    guidata(hObject, handles);

% UIWAIT makes done wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = done_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in done_btn.
function done_btn_Callback(hObject, eventdata, handles)
% hObject    handle to done_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global ApplicationHandles RunningParameters

    handles.Stop = true;
    % User Data.. don't forget it:
    userData = get(handles.figure1, 'UserData');


    RunningParameters.DoneButtonPressed = true;
    ApplicationHandles.ConfirmWindow = [];
    % The most important line:
    set(handles.figure1,'UserData',userData);
    
    
    handles.Stop = true;
    
    % Update handles structure    
    guidata(hObject, handles);
    

    % The window is closed outside to have access to the 'Stop' field before
   

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global RunningParameters
    
    handles.Stop = false;
    % Update handles structure    
    guidata(hObject, handles);
    
    RunningParameters.DoneButtonPressed = false;
    RunningParameters.DoneWindowPosition  = get(hObject, 'Outerposition');
