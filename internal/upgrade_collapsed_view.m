function upgrade_collapsed_view(current_ind, axe)
    
    global ApplicationHandles InputImage RunningParameters

    box = zeros(InputImage.SizeY,InputImage.SizeX,InputImage.SliceNumber);
    
    % Indices strart with X and Y, but 'matrix' contains only Z,T and C.

    indZ = strfind(InputImage(RunningParameters.SelectedDisplay).StackOrder,'Z')-2;
    
    if InputImage.PageNumber > 1;
        pages = [];
        for Z = 1:InputImage.SliceNumber
            current_ind(indZ) = Z;        
            page = InputImage(RunningParameters.SelectedDisplay).IndexMatrix(current_ind(1),current_ind(2),current_ind(3));        

            pages = [pages, page];
        end
        box = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,pages);
    else
        box = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,1);
    end

    % For setting the threshold the maximum intensity projection is more
    % useful, as the averages get dimmer across the stack and the
    % threshold applies equally to all slices. So in order to use the
    % projection view to find a good threshold the MIP is preferred.
    
    switch(axe)
        case 'X',
            [ proj ] = YZprojection_MIP(box);
        case 'Y',
            [ proj ] = XZprojection_MIP(box);
        case 'Z',    
            [ proj ] = XYprojection_MIP(box);
    end
    [proj, ~, ~, ~] = threshold(proj);    
    
    imshow(proj,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'Initialmagnification','fit');