function varargout = parameter_display(varargin)
% PARAMETER_DISPLAY MATLAB code for parameter_display.fig
%      PARAMETER_DISPLAY, by itself, creates a new PARAMETER_DISPLAY or raises the existing
%      singleton*.
%
%      H = PARAMETER_DISPLAY returns the handle to a new PARAMETER_DISPLAY or the handle to
%      the existing singleton*.
%
%      PARAMETER_DISPLAY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PARAMETER_DISPLAY.M with the given input arguments.
%
%      PARAMETER_DISPLAY('Property','Value',...) creates a new PARAMETER_DISPLAY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before parameter_display_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to parameter_display_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help parameter_display

% Last Modified by GUIDE v2.5 08-Jun-2017 13:30:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @parameter_display_OpeningFcn, ...
                   'gui_OutputFcn',  @parameter_display_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before parameter_display is made visible.
function parameter_display_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to parameter_display (see VARARGIN)

% Choose default command line output for parameter_display
handles.output = hObject;
    global RunningParameters
    global ApplicationHandles
    
    ApplicationHandles.ParameterWindow = hObject;
    
    set(hObject,'units','pixels');
    % The position changes, so the value must be upgraded
    RunningParameters.InfoWindowPosition  = get(hObject, 'Outerposition');

    
if (length(RunningParameters.displayWindows)>1)
    shiftWindowRight(RunningParameters.displayWindows(end),RunningParameters.ViewerWindowPosition(end-1,:),RunningParameters.WindowOffsetX);
else
    sendWindowRight(ApplicationHandles.ParameterWindow,...
    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
    RunningParameters.WindowOffsetX);
end

RunningParameters.InfoWindowPosition  = get(hObject, 'Outerposition');

    % Update handles structure
    guidata(hObject, handles);

% UIWAIT makes parameter_display wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = parameter_display_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in parameter_menu.
function parameter_menu_Callback(hObject, eventdata, handles)
% hObject    handle to parameter_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parameter_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parameter_menu

    global InputImage    
    global RunningParameters
    global ApplicationHandles
    
    str = get(hObject,'String');
    val = get(hObject,'Value');
    param_val = eval(['InputImage(RunningParameters.SelectedDisplay).Info.',char(str(val))]);
    set(ApplicationHandles.ParameterDisplayBox,'String',param_val);


% --- Executes during object creation, after setting all properties.
function parameter_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parameter_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

    global ApplicationHandles
    ApplicationHandles.ParameterMenu = hObject;


% --- Executes during object creation, after setting all properties.
function parameter_display_textbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parameter_display_textbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global ApplicationHandles

ApplicationHandles.ParameterDisplayBox = hObject;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global RunningParameters
    global ApplicationHandles
    
    RunningParameters.InfoWindowPosition = get(gcf, 'Outerposition');
    