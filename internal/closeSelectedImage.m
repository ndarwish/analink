function [] = closeSelectedImage()
%closeSelectedImage closes the image buffer and cleans the memory

    global ApplicationHandles
    global RunningParameters

    
    % The numbers are updated during the close call for the
    % display_window object (see corresponding file)    
    close(ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay));
    
end
