function varargout = projectionViewWin(varargin)
% PROJECTIONVIEWWIN MATLAB code for projectionViewWin.fig
%      PROJECTIONVIEWWIN, by itself, creates a new PROJECTIONVIEWWIN or raises the existing
%      singleton*.
%
%      H = PROJECTIONVIEWWIN returns the handle to a new PROJECTIONVIEWWIN or the handle to
%      the existing singleton*.
%
%      PROJECTIONVIEWWIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJECTIONVIEWWIN.M with the given input arguments.
%
%      PROJECTIONVIEWWIN('Property','Value',...) creates a new PROJECTIONVIEWWIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before projectionViewWin_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to projectionViewWin_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help projectionViewWin

% Last Modified by GUIDE v2.5 07-Jun-2016 20:19:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @projectionViewWin_OpeningFcn, ...
                   'gui_OutputFcn',  @projectionViewWin_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before projectionViewWin is made visible.
function projectionViewWin_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to projectionViewWin (see VARARGIN)

% Choose default command line output for projectionViewWin
handles.output = hObject;



global RunningParameters
    global ApplicationHandles
    set(hObject,'units','pixels');
    % In case that the reference window was moved:
    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:) = get(ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay), 'Outerposition');
    sendWindowRight(hObject,...
                    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
                    RunningParameters.WindowOffsetX);
    % The position changes, so the value must be upgraded
    
    RunningParameters.ProjectionWindowPosition = get(hObject, 'Outerposition');








% Update handles structure
guidata(hObject, handles);

% UIWAIT makes projectionViewWin wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = projectionViewWin_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    
    global ApplicationHandles
    ApplicationHandles.YZProjectionWin = hObject;


% --- Executes during object creation, after setting all properties.
function YZprojectionAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YZprojectionAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate YZprojectionAxes
    
    global ApplicationHandles
    ApplicationHandles.YZProjectionAxes = hObject;
