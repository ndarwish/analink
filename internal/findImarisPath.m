function [path, found] = findImarisPath(topDirectory, fileName)
    
    path = topDirectory;

    pathContents = dir(topDirectory);
    [sorted_names,~] = sortrows({pathContents.name}');

    if(isempty(find(strcmp([sorted_names],fileName), 1)))
        found = 0;
        path = [];

        innerPathIndices = find([pathContents.isdir] == 1);

        
        blackList = find(strcmp({pathContents(innerPathIndices).name},'.'));
        blackList = cat(1, blackList, find(strcmp({pathContents(innerPathIndices).name},'..')));
        innerPathIndices(blackList) = [];

        
        for innerPathInd = 1:length(innerPathIndices)
            newPath = char(strcat(topDirectory,'\',sorted_names(innerPathIndices(innerPathInd))))
            [path, found] = findImarisPath(newPath, fileName);
            if(found == 1)
                return
            end
        end

    else
        found = 1;
        return;            
    end

end