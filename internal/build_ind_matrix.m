function [ index_matrix ] = build_ind_matrix(sizes)
    %build_ind_matrix: From multipage to indices for multistack tiff images
    %   It builds an array where the value at a given position points to
    %   the right frame on the tiff file.
    


    index_matrix = (ones(sizes(1), sizes(2), sizes(3)));
        
    for i = 1:sizes(1)
        for j = 1:sizes(2)
            for k = 1:sizes(3)
                index_matrix(i,j,k) = (k-1)*sizes(2)*sizes(1)+...
                                      (j-1)*sizes(1)+...
                                      i;
            end
        end
    end
        
end

