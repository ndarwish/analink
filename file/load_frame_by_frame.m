function load_frame_by_frame(reader)

    % If the hyperstack is too big for the RAM it loads the first frame
    % only and sets a flag to use the HD versions of the functions
    
    global InputImage
    global RunningParameters

    % Set the first page as the current view, this always works
    InputImage(RunningParameters.SelectedFile).Hyperstack = [];
    InputImage(RunningParameters.SelectedFile).CurrentIndex  = 1;
    InputImage(RunningParameters.SelectedFile).Buffer = bfGetPlane(reader, ...
    	InputImage(RunningParameters.SelectedFile).CurrentIndex);