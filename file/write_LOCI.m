function [] = write_LOCI(img_path, target_file, plane, imageIndex)

%Write_LOCI: Writes the image pages together with the LOCI relevant info.


global slice_no frame_no channel_no
global pixel_width pixel_height voxel_depth
global HYPERSTACK stack_order

    % Inline functions used later .........................................
    toInt   = @(x) ome.xml.model.primitives.PositiveInteger(java.lang.Integer(x));
    toFloat = @(x) ome.xml.model.primitives.PositiveFloat(java.lang.Double(x));    
    
    % .....................................................................
    
    OmeMeta = loci.formats.MetadataTools.createOMEXMLMetadata();
    OmeMeta.createRoot();
    
    % Images might be cropped, so sizes might not match the originals .....   
    
imageIndex = imageIndex -1;

    OmeMeta.setPixelsSizeX(toInt(size(plane, 2)), imageIndex);             % sizeX
    OmeMeta.setPixelsSizeY(toInt(size(plane, 1)), imageIndex);             % sizeY
    
    OmeMeta.setPixelsSizeZ(toInt(slice_no),   imageIndex);                 % slice_no
    OmeMeta.setPixelsSizeT(toInt(frame_no),   imageIndex);                 % frame_no
    OmeMeta.setPixelsSizeC(toInt(channel_no), imageIndex);                 % channel_no

    OmeMeta.setPixelsPhysicalSizeX(toFloat(pixel_width),  imageIndex);     % pixel_width
    OmeMeta.setPixelsPhysicalSizeY(toFloat(pixel_height), imageIndex);     % pixel_height
    OmeMeta.setPixelsPhysicalSizeZ(toFloat(voxel_depth),  imageIndex);     % voxel_depth

    % Set the pixel depth according to the hyperstack .....................
    
    pixelTypeEnumHandler = ome.xml.model.enums.handlers.PixelTypeEnumHandler();
    pixelsType = pixelTypeEnumHandler.getEnumeration(class(HYPERSTACK));
    OmeMeta.setPixelsType(pixelsType, imageIndex);                         % bitdepth

    % Stack_order..........................................................
    
%     OmeMeta.setPixelsTimeIncrement(java.lang.Double(frame_interval), imageIndex);
    
    
    dimensionOrderEnumHandler = ome.xml.model.enums.handlers.DimensionOrderEnumHandler();
    OmeMeta.setPixelsDimensionOrder(dimensionOrderEnumHandler.getEnumeration(stack_order), imageIndex);
    OmeMeta.setImageID('Image:0', imageIndex);
    OmeMeta.setPixelsID('Pixels:0', imageIndex);
    OmeMeta.setChannelID('Channel:0:0', 0, imageIndex);

    OmeMeta.setChannelSamplesPerPixel(ome.xml.model.primitives.PositiveInteger(java.lang.Integer(1)), 0, imageIndex);
    OmeMeta.setPixelsBinDataBigEndian(java.lang.Boolean.FALSE, 0, imageIndex);      % Intel is a typical little endian architecture
    % Save ................................................................


    writer = loci.formats.ImageWriter()
    
    writer.setMetadataRetrieve(OmeMeta);
    writer.setInterleaved(false);    
%     writer.setValidBitsPerPixel(12);
    writer.setValidBitsPerPixel(16);
    writer.setCompression('Uncompressed');
    writer.getWriter([img_path,'\',target_file]).setBigTiff(true);
    writer.setWriteSequentially(true);
    writer.setId([img_path,'\',target_file]);

%     bfsave(plane, [img_path,'\',target_file], 'metadata', OmeMeta);


writer.close();
    
    
    
% % This Beanshell can write a proper OME-TIFF:
% % (JAVA code)
% import java.util.Arrays;
% import loci.common.DataTools;
% import loci.common.services.ServiceFactory;
% import loci.formats.FormatTools;
% import loci.formats.ImageWriter;
% import loci.formats.MetadataTools;
% import loci.formats.meta.IMetadata;
% import loci.formats.services.OMEXMLService;
% import ome.xml.model.enums.DimensionOrder;
% import ome.xml.model.enums.PixelType;
% import ome.xml.model.primitives.PositiveFloat;
% import ome.xml.model.primitives.PositiveInteger;
% 
% path = "/tmp/test.ome.tif";
% width = height = 512;
% depth = 10;
% 
% metadata = new ServiceFactory().getInstance(OMEXMLService.class).createOMEXMLMetadata(null);

% metadata.createRoot();
% 
% metadata.setImageID(MetadataTools.createLSID("Image", new int[] { 0 }), 0);                               
% metadata.setPixelsID(MetadataTools.createLSID("Pixels", new int[] { 0 }), 0);
% metadata.setPixelsDimensionOrder(DimensionOrder.fromString("XYCZT"), 0);
% metadata.setChannelID(MetadataTools.createLSID("Channel", new int[] { 0, 0 }), 0, 0);
% metadata.setChannelSamplesPerPixel(new PositiveInteger(1), 0, 0);
% metadata.setPixelsBinDataBigEndian(Boolean.FALSE, 0, 0);
% metadata.setPixelsType(PixelType.fromString(FormatTools.getPixelTypeString(FormatTools.UINT16)), 0);

% metadata.setPixelsSizeX(new PositiveInteger(width), 0);
% metadata.setPixelsSizeY(new PositiveInteger(height), 0);
% metadata.setPixelsSizeZ(new PositiveInteger(depth), 0);
% metadata.setPixelsSizeC(new PositiveInteger(1), 0);
% metadata.setPixelsSizeT(new PositiveInteger(1), 0);

% metadata.setPixelsPhysicalSizeX(new PositiveFloat(1d), 0);
% metadata.setPixelsPhysicalSizeY(new PositiveFloat(1d), 0);
% metadata.setPixelsPhysicalSizeZ(new PositiveFloat(1d), 0);
% metadata.setPixelsTimeIncrement(new Double(1), 0);
 
% new File(path).delete();
% writer = new ImageWriter().getWriter(path);
% writer.setWriteSequentially(true);
% writer.setMetadataRetrieve(metadata);
% writer.setInterleaved(false);
% writer.setBigTiff(true);
% writer.setValidBitsPerPixel(12);
% writer.setCompression("Uncompressed");
% 
% for (int i = 0; i < depth; i++) {
%     short[] pixels = new short[width * height];
%     Arrays.fill(pixels, (short)(i * 128));
% 
%     byte[] bytes = DataTools.shortsToBytes(pixels, true);
%     writer.setId(path);
%     writer.saveBytes(i, bytes);
% }
% 
% writer.close();    
