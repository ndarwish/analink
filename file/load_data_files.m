function [nameList, commonPath] = load_data_files(filter)
%load_data_files() loads text files to process their columns

    global InputFile
    
    if(~isempty(InputFile))
        NewFile = InputFile(end);
    else
        NewFile.Name = [];
        NewFile.Path = [];
    end    
    
    if (~isempty(NewFile.Path))
        % Start in the previous path if available:
        [nameList, commonPath] = uigetfile([NewFile.Path,'\',filter], 'Select the image','MultiSelect','on');
    else            
        % Without previous selections start in the current path
        [nameList, commonPath] = uigetfile([pwd,'\',filter], 'Select the image','MultiSelect','on');
    end
    
end

