function load_to_RAM(reader)

    % If the RAM is sufficcient it loads the whole stack into it.
    
    global InputImage
    global RunningParameters    
    
    % Load the hyperstack into RAM:
    InputImage(RunningParameters.SelectedFile).Hyperstack = zeros( InputImage(RunningParameters.SelectedDisplay).SizeY, InputImage(RunningParameters.SelectedDisplay).SizeX, InputImage(RunningParameters.SelectedFile).PageNumber);
    wbar = waitbar(0,'Loading file');

    for page = 1:InputImage(RunningParameters.SelectedFile).PageNumber
        waitbar(page/InputImage(RunningParameters.SelectedFile).PageNumber, wbar, ...
            ['page ',num2str(page),' of ',num2str(InputImage(RunningParameters.SelectedFile).PageNumber)]);
        InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,page) = bfGetPlane(reader, page);
    end
    
    % Set the first page as the current view
    InputImage(RunningParameters.SelectedFile).CurrentChannel = 1;
    InputImage(RunningParameters.SelectedFile).CurrentSlice   = 1;
    InputImage(RunningParameters.SelectedFile).CurrentSlice   = 1;
    
    InputImage(RunningParameters.SelectedFile).CurrentIndex   = 1;
    
    InputImage(RunningParameters.SelectedFile).Buffer         = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,...
                                        InputImage(RunningParameters.SelectedFile).CurrentIndex);
    close(wbar);