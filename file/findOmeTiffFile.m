function [returnName] = findOmeTiffFile(path, zInd, tInd, cInd)

    global regExpX regExpY regExpZ
    

                dirData = dir([path,'*.ome.*']);
                fileNames = {dirData.name};

                if(~isempty(find(~cellfun(@isempty,fileNames), 1)))

                    zNameString = ['Z', zInd];
                    tNameString = ['T', tInd];
                    cNameString = ['C', cInd];                

                    zFilteredFiles = regexp(fileNames, zNameString);
                    tFilteredFiles = regexp(fileNames, tNameString);
                    cFilteredFiles = regexp(fileNames, cNameString);
                    
                    selectedIndex = [];
                    
                    zInd = find(~cellfun(@isempty,zFilteredFiles), 1);
                    tInd = find(~cellfun(@isempty,tFilteredFiles), 1);               
                    cInd = find(~cellfun(@isempty,cFilteredFiles), 1);
                    
                    if(~isempty(zInd))
                        selectedIndex = zInd;
                    end
                    if(~isempty(tInd))
                        selectedIndex = find(selectedIndex == tInd);
                    end
                    if(~isempty(cInd))
                        selectedIndex = find(selectedIndex == cInd);
                    end
                    
                    returnName = fileNames{selectedIndex};
                                        
                else
                    disp('nothing found :(')
                    returnName = [];
                end