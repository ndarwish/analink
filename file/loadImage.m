function [success] = loadImage(NewFile)
    
    global InputImage InputFile
    global ApplicationHandles
    global RunningParameters
    
    % There will be an additional opened image:
    % Whenever loading a new file, new settings are recorded with respect
    % to it. Whenever a new file is loaded, the file count increases. For
    % closing files the opposite will be done.

    if(~isempty(ApplicationHandles.OpenedImages))
        ApplicationHandles.OpenedImages   = ApplicationHandles.OpenedImages + 1;
        RunningParameters.SelectedDisplay = ApplicationHandles.OpenedImages;
        RunningParameters.SelectedFile    = ApplicationHandles.OpenedImages;
    else
        ApplicationHandles.OpenedImages   = 1;
        RunningParameters.SelectedDisplay = 1;
        RunningParameters.SelectedFile    = 1;
    end


    success = 1;
    % Open the file: Load image pages and metadata. Everything goes to
    % global structures:

    % The function open_LOCI accepts lists of files
    if isempty(InputFile(end).Name)
        InputFile = NewFile;
    else
        InputFile = [InputFile; NewFile];
    end

    open_LOCI();

    fprintf(1,'File loaded correctly:\n');    
    fprintf(1,'%s%s\n',char(InputFile(RunningParameters.SelectedFile).Path),char(InputFile(RunningParameters.SelectedFile).Name));

    % Open the display, control and information windows:        
    %This makes the display handles available
    % The first one creates a pile of handlers, so many instances of
    % viewer windows can be opened at the same time.

    RunningParameters.displayWindows = [RunningParameters.displayWindows; display_window];
    RunningParameters.displayWindowData = [RunningParameters.displayWindowData; guidata(RunningParameters.displayWindows(end))];

    ApplicationHandles.ViewerWindow = [ApplicationHandles.ViewerWindow; ...
                                       RunningParameters.displayWindowData(end).figure1];

    ApplicationHandles.ViewerAxes = [ApplicationHandles.ViewerAxes; ...
                                       RunningParameters.displayWindowData(end).image_axes];  

    RunningParameters.ViewerWindowPosition = [RunningParameters.ViewerWindowPosition; ...    
                                   RunningParameters.displayWindowData(end).ViewerWindowPosition];

    % The other two will interact with the selected image, so one
    % instance only in each case:

    all_sliders;               % This allows page selection
    parameter_display;         % This displays metadata

    if (length(RunningParameters.displayWindows)>1)
        shiftWindowRight(RunningParameters.displayWindows(end),RunningParameters.ViewerWindowPosition(end-1,:),RunningParameters.WindowOffsetX);
        shiftWindowRight(ApplicationHandles.SliderWindow,RunningParameters.SliderWindowPosition,RunningParameters.WindowOffsetX);
        shiftWindowRight(ApplicationHandles.ParameterWindow,RunningParameters.InfoWindowPosition,RunningParameters.WindowOffsetX);
    else
        sendWindowUnder(ApplicationHandles.SliderWindow,...
            RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
            RunningParameters.WindowOffsetY);
        sendWindowRight(ApplicationHandles.ParameterWindow,...
            RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
            RunningParameters.WindowOffsetX);
    end    

    % Set the display window title.
    set(ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay),'Name',InputFile(RunningParameters.SelectedFile).Name);

    imshow(InputImage(RunningParameters.SelectedDisplay).Buffer,[0 max(InputImage(RunningParameters.SelectedDisplay).Buffer(:))],...
           'Parent',ApplicationHandles.ViewerAxes(end));
       
    try
        set(ApplicationHandles.ParameterMenu,'String', fieldnames(InputImage(RunningParameters.SelectedDisplay).Info(1)));
    catch
        fprintf('No info available!\n');
    end
    
    % Apply some safe display settings:
    InputImage(RunningParameters.SelectedDisplay).CurrentChannel = 1;
    InputImage(RunningParameters.SelectedDisplay).CurrentFrame   = 1;
    InputImage(RunningParameters.SelectedDisplay).CurrentSlice   = 1; 
end