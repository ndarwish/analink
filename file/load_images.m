function [ success ] = load_images()
%load_images load the specified image hyperstack
%   This function uses the LOCI formats library and should be able to open
%   any kind of digital image.

    global InputFile InputImage
    
    if (size(InputImage) > 0)
            [~, listedimages] = size(InputImage);
    else
        listedimages = [];
    end
    
    if (listedimages > 0)
        NewImage = InputImage(end);
        InputImage = [InputImage, NewImage];
    end
    % Get the path and file name    
    NewFile = InputFile(end);
            
    if (~isempty(NewFile.Path))
    % Start in the previous path if available:
        [nameList, commonPath] = uigetfile([NewFile.Path,'\','*.*'], 'Select the image','MultiSelect','on');
    else            
        % Without previous selections start in the current path
        [nameList, commonPath] = uigetfile([pwd,'\','*.*'], 'Select the image','MultiSelect','on');
    end

    % Report the result of the operation: 1 for success, 0 for fail.
    success = 0;

    % Open all setected files sequentially:
    fileCount = length(nameList);
    if (ischar(nameList))
        NewFile.Name = nameList;
        NewFile.Path = commonPath;
        [success] = loadImage(NewFile);
    elseif(fileCount > 1)
        for fileInd = 1:fileCount
            NewFile.Name = char(nameList(fileInd));
            NewFile.Path = char(commonPath);
            if (~isempty(NewFile.Name))
                [success] = loadImage(NewFile);
            end
        end
    end
end