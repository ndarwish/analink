function [] = open_LOCI()

    global InputImage
    global InputFile
    global RunningParameters
       
    reader = bfGetReader([char(InputFile(RunningParameters.SelectedFile).Path),char(InputFile(RunningParameters.SelectedFile).Name)]);
    
    fprintf('Loading image');
    while(1)
        if exist('InputFile', 'var')
            if(~isempty(InputFile))
                break;
            end
            fprintf('.');
        end
    end

try
    InputImage(RunningParameters.SelectedFile).Info   = imfinfo([char(InputFile(RunningParameters.SelectedFile).Path),char(InputFile(RunningParameters.SelectedFile).Name)]);
catch

    disp('No image info available!');
end
    FileInfo = dir([char(InputFile(RunningParameters.SelectedFile).Path),char(InputFile(RunningParameters.SelectedFile).Name)]);    
    InputFile(RunningParameters.SelectedFile).Size = FileInfo.bytes;
    
    % Get relevant image information ......................................
    % Retrieve the OME metadata from the file using the OME bioformats
    % importer:
    
    omeMeta = reader.getMetadataStore();
    
    % .....................................................................

    % Use this line if you ever need to find which methds are available for
    % the metadata:
    % methodsview(omeMeta)

    % This is the way it's supposed to work, but sometimes this information
    % is wrong and has to be recalculated:
%     InputImage(RunningParameters.SelectedFile).PageNumber = length(InputImage(RunningParameters.SelectedDisplay).Info(:,1));

    InputImage(RunningParameters.SelectedFile).SizeX = omeMeta.getPixelsSizeX(0).getValue();        % sizeX
    InputImage(RunningParameters.SelectedFile).SizeY = omeMeta.getPixelsSizeY(0).getValue();        % sizeY

    InputImage(RunningParameters.SelectedFile).PixelWidth  = omeMeta.getPixelsPhysicalSizeX(0).value; % in �m
    InputImage(RunningParameters.SelectedFile).PixelHeight = omeMeta.getPixelsPhysicalSizeY(0).value; % in �m    

    InputImage(RunningParameters.SelectedFile).SliceNumber = omeMeta.getPixelsSizeZ(0).getValue();         % Z slices

    if (InputImage(RunningParameters.SelectedFile).SliceNumber > 1)
        InputImage(RunningParameters.SelectedFile).VoxelDepth = omeMeta.getPixelsPhysicalSizeZ(0).value;        % in �m

    else
        InputImage(RunningParameters.SelectedFile).VoxelDepth = 0;
    end

    InputImage(RunningParameters.SelectedFile).FrameNumber = omeMeta.getPixelsSizeT(0).getValue();         % Number of time frames
    if (InputImage(RunningParameters.SelectedFile).FrameNumber > 1)
        InputImage(RunningParameters.SelectedFile).FrameInterval = omeMeta.getPixelsTimeIncrement(0);                % Time between frames
    else
        InputImage(RunningParameters.SelectedFile).FrameInterval = 0;
    end

    InputImage(RunningParameters.SelectedFile).ChannelNumber = omeMeta.getPixelsSizeC(0).getValue();       % Number of channels

    % We calculate the page number instead of reading it.
    InputImage(RunningParameters.SelectedFile).PageNumber = ...
        InputImage(RunningParameters.SelectedFile).SliceNumber *...
        InputImage(RunningParameters.SelectedFile).FrameNumber *...
        InputImage(RunningParameters.SelectedFile).ChannelNumber;
    
    InputImage(RunningParameters.SelectedFile).Bitdepth = char(omeMeta.getPixelsType(0).getValue());       % Bit depth

    if(~strcmp(omeMeta.getImageName(0), InputFile(RunningParameters.SelectedFile).Name))  % filename including extension
        disp('File name and its corresponding tag did not match.');
        omeMeta.setImageName(InputFile(RunningParameters.SelectedFile).Name, 0);
        disp('Solved');
    else
        disp(omeMeta.getImageName(0));        
    end

    InputImage(RunningParameters.SelectedFile).StackOrder = omeMeta.getPixelsDimensionOrder(0).getValue(); % e.g. XYCZT

    % .....................................................................

    if(isempty(InputImage(RunningParameters.SelectedDisplay).ChannelNumber))
        InputImage(RunningParameters.SelectedDisplay).ChannelNumber = 1;
    end
    if(isempty(InputImage(RunningParameters.SelectedDisplay).SliceNumber))
        InputImage(RunningParameters.SelectedDisplay).SliceNumber = 1;
    end
    if(isempty(InputImage(RunningParameters.SelectedDisplay).FrameNumber))
        InputImage(RunningParameters.SelectedDisplay).FrameNumber = 1;
    end
        
    switch (char(InputImage(RunningParameters.SelectedFile).StackOrder))
    case 'XYCZT'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).ChannelNumber,InputImage(RunningParameters.SelectedFile).SliceNumber,InputImage(RunningParameters.SelectedFile).FrameNumber];        
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    case 'XYCTZ'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).ChannelNumber,InputImage(RunningParameters.SelectedFile).FrameNumber,InputImage(RunningParameters.SelectedFile).SliceNumber];
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    case 'XYZCT'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).SliceNumber,InputImage(RunningParameters.SelectedFile).ChannelNumber,InputImage(RunningParameters.SelectedFile).FrameNumber];
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    case 'XYZTC'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).SliceNumber,InputImage(RunningParameters.SelectedFile).FrameNumber,InputImage(RunningParameters.SelectedFile).ChannelNumber];
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    case 'XYTCZ'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).FrameNumber,InputImage(RunningParameters.SelectedFile).ChannelNumber,InputImage(RunningParameters.SelectedFile).SliceNumber];
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    case 'XYTZC'
        sizes = [InputImage(RunningParameters.SelectedFile).SizeX, InputImage(RunningParameters.SelectedFile).SizeY, ...
            InputImage(RunningParameters.SelectedFile).FrameNumber,InputImage(RunningParameters.SelectedFile).SliceNumber,InputImage(RunningParameters.SelectedFile).ChannelNumber];
        InputImage(RunningParameters.SelectedFile).IndexMatrix = build_ind_matrix(sizes(3:5));
    otherwise
        disp('Missing stack order info');
    end
    
    % Evaluate if the computer can load the full series into its RAM:
    [userview, ~] = memory;
    file_mem_usage = 100 * InputFile(RunningParameters.SelectedFile).Size / ( userview.MemAvailableAllArrays - userview.MemUsedMATLAB);
    InputFile(RunningParameters.SelectedFile).isOMETiff = (strfind(InputFile(RunningParameters.SelectedFile).Name, 'ome.tif') | strfind(InputFile(RunningParameters.SelectedFile).Name, 'ome.tif'));
    
    % Don't load the whole file into RAM if it's an OME Tiff or if it's too
    % large:
    if (file_mem_usage < 0.10 & ~InputFile(RunningParameters.SelectedFile).isOMETiff)    % We observed problems already at 14% (with 128Gb of RAM)
        fprintf('The series occupies a %f%% of the available RAM.\n',file_mem_usage);
        load_to_RAM(reader);
        RunningParameters.RAM = 1; 
    else
        fprintf('The series occupies a %f%% of the available RAM.\nThe file will be loaded frame by frame.\n',file_mem_usage);
        load_frame_by_frame(reader);
        RunningParameters.RAM = 0;
    end
    check_indices(); %fix wrong indices, if any

function check_indices()
    global InputImage
    global RunningParameters
    % HANDLE BAD INDICES ..................................................

    if (~any([InputImage(RunningParameters.SelectedFile).FrameNumber, InputImage(RunningParameters.SelectedFile).ChannelNumber, InputImage(RunningParameters.SelectedFile).SliceNumber]))
        InputImage(RunningParameters.SelectedFile).FrameNumber = 1;
        InputImage(RunningParameters.SelectedFile).ChannelNumber = 1;
        InputImage(RunningParameters.SelectedFile).SliceNumber = InputImage(RunningParameters.SelectedFile).PageNumber;
    end