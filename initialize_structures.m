function [] = initialize_structures()
%initialize_structures Initializes all the global variables
%   All the global variables are organized now and initialized at the same
%   time, so they are available while needed afterwards.

    global ApplicationHandles 
    global InputImage OutputImage
    global InputFile OutputFile
    global OutputStatisticsFile
    global InstructionStack
    global InternalData
    global LogFile
    global RunningParameters
    global SystemFeatures

    % Main window controls:
    ApplicationHandles.MainWindow = [];
    
    % Wiewer window controls:
    ApplicationHandles.ViewerWindow = [];          % this might be an array
    ApplicationHandles.YZProjectionWin = [];
    ApplicationHandles.ViewerAxes = [];
    ApplicationHandles.YZProjectionAxes = [];
    ApplicationHandles.AuxiliaryViewWindow = [];
    
    ApplicationHandles.OpenedImages = 0;     % Initially no images are open
    % 'About' message box:
    ApplicationHandles.AboutMessageBox = [];    
    ApplicationHandles.DoneWindow = []; 
    ApplicationHandles.DoneButton = [];
    
    % Slider window controls:
    ApplicationHandles.SliderWindow = [];

    ApplicationHandles.ParameterWindow = [];    
    ApplicationHandles.MainSlider = [];
    ApplicationHandles.MainValueBox = [];
    
    ApplicationHandles.ParameterMenu = [];
    ApplicationHandles.SliderMenu = [];
    ApplicationHandles.MinString = [];
    ApplicationHandles.MaxString = [];
    
       
    
    % Parameter display window controls:
    ApplicationHandles.ParameterWindow = [];
    
    InputImage.Info          = [];      
    InputImage.PageNumber    = [];
    InputImage.SliceNumber   = [];
    InputImage.FrameNumber   = [];
    InputImage.ChannelNumber = [];
    InputImage.SizeX         = [];
    InputImage.SizeY         = [];
    InputImage.Bitdepth      = [];
    InputImage.FrameInterval = [];
    InputImage.VoxelDepth    = [];
    InputImage.PixelWidth    = [];
    InputImage.PixelHeight   = [];
    InputImage.SizeUnits     = [];
    InputImage.StackOrder    = [];
    InputImage.IndexMatrix   = [];

%   Execution parameters associated with a concrete image
    InputImage.Threshold      = [];
    InputImage.AreaThreshold  = [];
    InputImage.ProjectionAxe  = [];
    InputImage.MaskX          = [];
    InputImage.MaskY          = [];
    InputImage.CurrentIndex   = [];
    InputImage.Buffer         = [];    
    
    InputImage.CurrentChannel = [];
    InputImage.CurrentFrame   = [];
    InputImage.CurrentSlice   = [];

    InputImage.Hyperstack     = [];
    
    OutputImage = InputImage;
        
    InputFile.Path            = [];
    InputFile.Name            = [];
    InputFile.Extension       = [];
    InputFile.Size            = [];
    InputFile.SortedFileNames = [];
    InputFile.FileNumber      = [];
    InputFile.Multiple        = [];
    InputFile.isOMETiff       = 0;
    
    OutputFile = InputFile;
    
    OutputStatisticsFile.FilePath = [];
    OutputStatisticsFile.FileName = [];
    
%   Internal execution parameters
    
    InstructionStack.last         = [];
    InstructionStack.Parameters   = [];

    InternalData.ThresholdedView  = [];

    LogFile.Path = [];
    LogFile.Name = [];
    
    RunningParameters.ScriptPath = [];
    RunningParameters.ScriptName = [];
    RunningParameters.BatchRun   = [];
    RunningParameters.RAM = 0;    % 1 if the stack is loaded into RAM, otherwise 0
    
    % these two are for loading multiple visualization windows (stacked handles)
    RunningParameters.displayWindows = [];
    RunningParameters.displayWindowData = [];
    
    RunningParameters.SliderSelectionMode = [];
    
    RunningParameters.ImarisPath = [];
    
    % Window positions:
    % The system returs 4 numbers: [ X,Y,Width,Height ]
    % X is relative to the left, but Y (unless negative) is relative to the 
    % bottom
    RunningParameters.MainWindowPosition = [];
    RunningParameters.ViewerWindowPosition = [];
    RunningParameters.InfoWindowPosition = [];
    RunningParameters.SliderWindowPosition = [];
    RunningParameters.ParameterWindowPosition = [];
    RunningParameters.DoneWindowPosition = [];
    RunningParameters.ProjectionWindowPosition = [];
    RunningParameters.SegmentWindowPosition = [];
    
    % Selected display window, so the operations apply to the corresponding
    % buffer:
    RunningParameters.SelectedDisplay = 1; % So it points to the firs window unless another selected
    RunningParameters.SelectedFile    = 1; % Similarly for files
    
    RunningParameters.SegmentationMethod = '';
    
    RunningParameters.DoneButtonPressed = [];  % It's set to false when the dialog opens
    
    % Offsets between windows on the screen
    RunningParameters.WindowOffsetX = 50;
    RunningParameters.WindowOffsetY = 50;
    
    Screensize = get(0, 'Screensize'); % Screen size in pixels
    SystemFeatures.ScreensizeX = Screensize(3);
    SystemFeatures.ScreensizeY = Screensize(4);
end

