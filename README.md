ANALINK mini-toolbox for image analysis.

Opens microscopy files using the Matlab port of the Bio-formats library, 
incorporates a series of structures for handling events with the graphical 
interface and to navigate through mutliple hiperstacks (for more details 
plpease see "initialize_structures.m"). 

Some sample projects developed and loaded in the "scripts" directory. 

The software starts with "startup.m". Tested under Matlab 2017 and later 
versions.

Developed by Nasser Darwish, at IST Austria. The functions under the 
"fileExchange" folder were developed by other authors and their corresponding 
license files are kept with them. 