function [folder_names] = add_all_paths(current_path)
% ADD_ALL_PATHS add all the subfolders to the search path
%   The purpose is to make all the functions available for the main script,
%   keeping them classified by categories in different folders.
    
    file_list_struct = dir(current_path);
    folder_indices = find([file_list_struct.isdir]);

    
    for ind = 3:length(folder_indices) %supress the current and the upper directories
        
        folderName = file_list_struct(folder_indices(ind)).name;
        nestedPath = [current_path,'\',folderName];
        disp(['Adding path: ',nestedPath]);
        
        add_all_paths(nestedPath);
        addpath(nestedPath);
    end

end

