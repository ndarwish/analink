function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 05-Jul-2018 18:38:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
    global ApplicationHandles
    global RunningParameters
    set(hObject,'units','pixels');
    handles.output = hObject;
    ApplicationHandles.MainWindow = hObject;                               % Add window handler to the overall structure    
    movegui(hObject,'northwest'); 
    RunningParameters.MainWindowPosition  = get(hObject, 'Outerposition');
    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes main wait for user response (see UIRESUME)
    % uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function File_top_menu_Callback(hObject, eventdata, handles)
% hObject    handle to File_top_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Help_top_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Help_top_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function file_open_menu_Callback(hObject, eventdata, handles)
% hObject    handle to file_open_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

success = load_images();

if (success == 0)
    disp('Error loading image');
end

% --------------------------------------------------------------------
function file_exit_menu_Callback(hObject, eventdata, handles)
% hObject    handle to file_exit_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ApplicationHandles RunningParameters

    if(ApplicationHandles.OpenedImages > 1)
        for(ind = ApplicationHandles.OpenedImages:1)
            RunningParameters.SelectedDisplay = ind;
            closeSelectedImage();
        end
    elseif(ApplicationHandles.OpenedImages == 1)
        closeSelectedImage();        
    else
        fprintf('Nothing to close!\n');
    end

end_program();


% --------------------------------------------------------------------
function help_about_menu_Callback(hObject, eventdata, handles)
% hObject    handle to help_about_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

display_about_msg();


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function edit_basicSegmentation_menu_Callback(hObject, eventdata, handles)
% hObject    handle to edit_basicSegmentation_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    clc;
    segment_dialog('Wait(interruptible)');


% --------------------------------------------------------------------
function edit_threshold_menu_Callback(hObject, eventdata, handles)
% hObject    handle to edit_threshold_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    clc;
    threshold_slider_dialog();

% --------------------------------------------------------------------
function surface_render_menu_Callback(hObject, eventdata, handles)
% hObject    handle to surface_render_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    QuickAndDirty();


% --------------------------------------------------------------------
function file_close_menu_Callback(hObject, eventdata, handles)
% hObject    handle to file_close_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    closeSelectedImage();


% --------------------------------------------------------------------
function surface_render_batch_menu_Callback(hObject, eventdata, handles)
% hObject    handle to surface_render_batch_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    BuildMultipleImarisSurfaces();


% --------------------------------------------------------------------
function skeleton_menu_Callback(hObject, eventdata, handles)
% hObject    handle to skeleton_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    objectInd = 1; % to see how to use it properly, check the buildSurface function in the Franks directory;
    skeletonizer3D(objectInd);


% --------------------------------------------------------------------
function interactive_3D_crop_menu_Callback(hObject, eventdata, handles)
% hObject    handle to interactive_3D_crop_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    InteractiveCrop();

% --------------------------------------------------------------------
function clusterify_menu_Callback(hObject, eventdata, handles)
% hObject    handle to clusterify_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    clc;
    clusterify();

% --------------------------------------------------------------------
function edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function InteractiveSegmentation_menu_Callback(hObject, eventdata, handles)
% hObject    handle to InteractiveSegmentation_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    masker();


% --------------------------------------------------------------------
function Rebrain_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Rebrain_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    clc;
    rebrain();


% --------------------------------------------------------------------
function connMap_menu_Callback(hObject, eventdata, handles)
% hObject    handle to connMap_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


    clc;        
    connMap();
