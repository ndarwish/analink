function [ currentHandle ] = shiftWindowRight( currentHandle, refPosition, hoffset )
%shiftWindowUnder Move the specified window to the right with respect to the left border of another
    
    buffer = get(currentHandle, 'Outerposition');

    set(currentHandle,'Outerposition', [refPosition(1) + hoffset,...
                                  refPosition(2) + refPosition(4) - buffer(4),...
                                  buffer(3),buffer(4)]);    


end

