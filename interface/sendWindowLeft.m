function [ currentHandle ] = sendWindowLeft( currentHandle, refPosition, hoffset )
%sendWindowUnder Move the specified window below another
    
    buffer = get(currentHandle, 'Outerposition');

    set(currentHandle,'Outerposition', [refPosition(1) - buffer(3) - hoffset,...
                                  refPosition(2) + refPosition(4) - buffer(4),...
                                  buffer(3),buffer(4)]);    


end

