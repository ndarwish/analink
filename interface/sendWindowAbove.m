function [ currentHandle ] = sendWindowAbove( currentHandle, refPosition, voffset )
%sendWindowUnder Move the specified window below another
    
    buffer = get(currentHandle, 'Outerposition');

    set(currentHandle,'Outerposition',[refPosition(1),...
                                  refPosition(2) + refPosition(4) + voffset,...
                                  buffer(3),buffer(4)]);
    clear('buffer');

end

