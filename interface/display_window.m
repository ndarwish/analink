function varargout = display_window(varargin)
% DISPLAY_WINDOW MATLAB code for display_window.fig
%      DISPLAY_WINDOW, by itself, creates a new DISPLAY_WINDOW or raises the existing
%      singleton*.
%
%      H = DISPLAY_WINDOW returns the handle to a new DISPLAY_WINDOW or the handle to
%      the existing singleton*.
%
%      DISPLAY_WINDOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAY_WINDOW.M with the given input arguments.
%
%      DISPLAY_WINDOW('Property','Value',...) creates a new DISPLAY_WINDOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before display_window_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to display_window_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help display_window

% Last Modified by GUIDE v2.5 13-Apr-2016 10:10:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @display_window_OpeningFcn, ...
                   'gui_OutputFcn',  @display_window_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before display_window is made visible.
function display_window_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to display_window (see VARARGIN)


% Choose default command line output for display_window
    handles.output = hObject;

    global RunningParameters
    global ApplicationHandles
    set(hObject,'units','pixels');
    % In case that the reference window was moved:
    RunningParameters.MainWindowPosition = get(ApplicationHandles.MainWindow, 'Outerposition');
    sendWindowUnder(hObject,...
                    RunningParameters.MainWindowPosition,...
                    RunningParameters.WindowOffsetY);
    % The position changes, so the value must be upgraded
    
    handles.ViewerWindowPosition = get(hObject, 'Outerposition');
    
    % Update handles structure
    guidata(hObject, handles);

% UIWAIT makes display_window wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = display_window_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    
    handles.ViewerWindow = hObject;
  


% --- Executes during object creation, after setting all properties.
function image_axes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to image_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate image_axes

    handles.ViewerAxes = hObject;

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, ~, ~)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global RunningParameters
global ApplicationHandles
global InputImage

RunningParameters.SelectedDisplay = find(ApplicationHandles.ViewerWindow == gcf);
RunningParameters.SelectedFile = RunningParameters.SelectedDisplay;

if (isempty(InputImage(RunningParameters.SelectedDisplay).CurrentChannel))
    InputImage(RunningParameters.SelectedDisplay).CurrentChannel = 1;
end
if (isempty(InputImage(RunningParameters.SelectedDisplay).CurrentSlice))
    InputImage(RunningParameters.SelectedDisplay).CurrentSlice = 1;
end
if (isempty(InputImage(RunningParameters.SelectedDisplay).CurrentFrame))
    InputImage(RunningParameters.SelectedDisplay).CurrentFrame = 1;
end

RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:)  = get(hObject, 'Outerposition');

% Move the interaction windows relative to the selected display

    sendWindowUnder(ApplicationHandles.SliderWindow,...
                    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
                    RunningParameters.WindowOffsetY);
    sendWindowRight(ApplicationHandles.ParameterWindow,...
                    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
                    RunningParameters.WindowOffsetX);
                
    RunningParameters.SliderWindowPosition  = get(ApplicationHandles.SliderWindow, 'Outerposition');
    RunningParameters.InfoWindowPosition = get(ApplicationHandles.ParameterWindow, 'Outerposition');

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

    global ApplicationHandles
    global RunningParameters

    if(ApplicationHandles.OpenedImages > 1)

        RunningParameters.SelectedFile = RunningParameters.SelectedFile - 1;
        ApplicationHandles.OpenedImages = ApplicationHandles.OpenedImages - 1;
        RunningParameters.SelectedDisplay = RunningParameters.SelectedDisplay - 1;
        ApplicationHandles.ViewerWindow(end) = [];

    elseif(ApplicationHandles.OpenedImages == 1)
        fprintf('All images were closed.\n');
        close(ApplicationHandles.SliderWindow);
        close(ApplicationHandles.ParameterWindow);
        ApplicationHandles.ViewerWindow = [];
        ApplicationHandles.OpenedImages = [];  
        RunningParameters.SelectedDisplay = [];
    else
        fprintf('Nothing to close!\n');
    end


    delete(hObject);

% --- Executes on mouse press over figure background.
function image_axes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on mouse press over figure background.

function figure1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
