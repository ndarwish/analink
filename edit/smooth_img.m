function varargout = smooth_img(varargin)
% SMOOTH_IMG MATLAB code for smooth_img.fig
%      SMOOTH_IMG, by itself, creates a new SMOOTH_IMG or raises the existing
%      singleton*.
%
%      H = SMOOTH_IMG returns the handle to a new SMOOTH_IMG or the handle to
%      the existing singleton*.
%
%      SMOOTH_IMG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SMOOTH_IMG.M with the given input arguments.
%
%      SMOOTH_IMG('Property','Value',...) creates a new SMOOTH_IMG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before smooth_img_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to smooth_img_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help smooth_img

% Last Modified by GUIDE v2.5 11-Dec-2014 12:59:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @smooth_img_OpeningFcn, ...
                   'gui_OutputFcn',  @smooth_img_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before smooth_img is made visible.
function smooth_img_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to smooth_img (see VARARGIN)

% Choose default command line output for smooth_img
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes smooth_img wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = smooth_img_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function ksize_box_Callback(hObject, eventdata, handles)
% hObject    handle to ksize_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ksize_box as text
%        str2double(get(hObject,'String')) returns contents of ksize_box as a double


% --- Executes during object creation, after setting all properties.
function ksize_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ksize_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in apply_btn.
function apply_btn_Callback(hObject, eventdata, handles)
% hObject    handle to apply_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global menu_hdlr current_ind img_path img_name
global img_data frame_no slice_no channel_no HYPERSTACK
global log_file_name
    
    kern_size = fix(str2num(get(handles.ksize_box,'String')));
    kernel = fspecial('gaussian',[kern_size, kern_size],2);

    indX = find(strcmp('indX',  menu_hdlr.indices));
    indY = find(strcmp('indY',  menu_hdlr.indices));
    indZ = find(strcmp('ind_z', menu_hdlr.indices));
    indT = find(strcmp('ind_t', menu_hdlr.indices));
    indC = find(strcmp('ind_c', menu_hdlr.indices));

	smoothed_img_name = [img_name(1:strfind(img_name,'.')-1),...
    	' - smoothed by gaussian kernel of ',num2str(kern_size),'pix width.tif'];    

    if(~isempty(current_ind))
        
        % current_ind has 3 the indices for Z, T and C. The matrix contains also
        % the indices for X and Y. That's the reason for this '-2':
        indices(indZ) = current_ind(indZ -2);
        indices(indT) = current_ind(indT -2);
        indices(indC) = current_ind(indC -2);

        for T = 1:frame_no
            for Z = 1:slice_no
                current_ind(indT -2) = T;
                current_ind(indZ -2) = Z;
                page(Z,T) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
                % HD version
    %             block(:,:,Z,T) = imread([img_path,'\',img_name],'Index',page(Z,T));
                % RAM version
                block(:,:,Z,T) = HYPERSTACK(:,:,page(Z,T));
            end
        end
        disp('Hiperstack loaded correctly');

        write_metadata( img_path, smoothed_img_name, img_data);

        for T = 1:frame_no
            for Z = 1:slice_no
                page(Z,T) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
                TMP = imfilter(block(:,:,Z,T),kernel,'same');
                smoothed_block(:,:,Z,T) = TMP;
                imwrite(smoothed_block(:,:,Z,T),[img_path,'/',smoothed_img_name],'WriteMode','append', 'Compression','none');
            end
            fprintf(1, 'Frame %d of %d. \n',T,frame_no);
        end                
    else
        block = HYPERSTACK(:,:,1);
        smoothed_block = imfilter(block,kernel,'same');
        imwrite(smoothed_block,[img_path,'/',smoothed_img_name],'WriteMode','append', 'Compression','none'); 
    end
    msgbox('smoothed image saved');
    
    log_hdl = fopen([img_path,'\',log_file_name],'a+');
    fprintf(log_hdl,'SMOOTH - KNL: %d',kern_size);
    fprintf(log_hdl,'\n');
    fclose(log_hdl);
    
    disp('smoothed image saved');
    close(handles.figure1);
    
