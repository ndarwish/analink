function [segmented] = watershed2D( block )
%watershed2D binarizes and segments an image


global RunningParameters
global ApplicationHandles
global thr

    binary_block = zeros(size(block));            % Allocate the binary image
    
    [ ~, maskX, maskY, positives ] = threshold( block, thr );
%     imshow(thr_block,[0 max(max(thr_block))],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');
    
    for ind = 1:positives
        binary_block(maskX(ind),maskY(ind)) = 1;
    end
    
    imshow(binary_block,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),...
           'InitialMagnification', 'fit');   
    
    distances = bwdist(binary_block);             % Distance transformation
    distances = -distances;
%     imshow(distances,[],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');
    distances(~binary_block) = - Inf;             % Pixels under threshold are not close to anything
% 
    segmented = watershed(distances,8);
    rgb = label2rgb(segmented,'jet',[.5 .5 .5]);
    imshow(rgb,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay), 'InitialMagnification', 'fit');
end

