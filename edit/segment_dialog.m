function varargout = segment_dialog(varargin)
% SEGMENT_DIALOG MATLAB code for segment_dialog.fig
%      SEGMENT_DIALOG, by itself, creates a new SEGMENT_DIALOG or raises the existing
%      singleton*.
%
%      H = SEGMENT_DIALOG returns the handle to a new SEGMENT_DIALOG or the handle to
%      the existing singleton*.
%
%      SEGMENT_DIALOG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEGMENT_DIALOG.M with the given input arguments.
%
%      SEGMENT_DIALOG('Property','Value',...) creates a new SEGMENT_DIALOG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before segment_dialog_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to segment_dialog_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help segment_dialog

% Last Modified by GUIDE v2.5 08-Jun-2017 16:56:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @segment_dialog_OpeningFcn, ...
                   'gui_OutputFcn',  @segment_dialog_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before segment_dialog is made visible.
function segment_dialog_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to segment_dialog (see VARARGIN)

% Choose default command line output for segment_dialog
    global RunningParameters

    handles.output = hObject;

movegui(gcf,'east');
% RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:)
% RunningParameters.WindowOffsetY
%     sendWindowUnder(hObject,...
%             RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
%             RunningParameters.WindowOffsetY);
        
        
    RunningParameters.SegmentWindowPosition = get(hObject, 'Outerposition');
    
    % Update handles structure
    guidata(hObject, handles);    

% UIWAIT makes segment_dialog wait for user response (see UIRESUME)
% uiwait(handles.segmentDialogFigure);


% --- Outputs from this function are returned to the command line.
function varargout = segment_dialog_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in method_menu.
function method_menu_Callback(hObject, eventdata, handles)
% hObject    handle to method_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method_menu

    global seg_method

    %str = get(hObject,'String');
    val = get(hObject,'Value');

    switch(val)
        case{1}
            seg_method = char('Watershed');
        case{2}
            seg_method = char('Connected neighbors');
        case{3}
            seg_method = char('Local maxima');
    end


% --- Executes during object creation, after setting all properties.
function method_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

    global seg_method

    
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    default = 1;
    set(hObject,'Value',default);
    list = get(hObject,'String');
    seg_method = char(list(default));
    
    

% --- Executes on button press in run_btn.
function run_btn_Callback(hObject, eventdata, handles)
% hObject    handle to run_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global seg_method seg_menu 
    global menu_hdlr current_ind
    global img_data smallest_obj largest_obj
    global HYPERSTACK thr

    global RunningParameters
    %global ApplicationHandles
    global InputImage InputFile


    order = InputImage(RunningParameters.SelectedFile).StackOrder;

    indX =  strfind(order,'X');
    indY =  strfind(order,'Y');
    indZ =  strfind(order,'Z');
    indT =  strfind(order,'T');
    indC =  strfind(order,'C');


    smallest_obj = str2num(get(handles.smallest_box,'String'));
    largest_obj  = str2num(get(handles.biggest_box,'String'));

% By now the operations are allpied to the current channel only:

    switch(seg_menu)
        case{'xy'} % 2D segmentation, once only ...............................
            switch(seg_method)
                case{'Watershed'}
                    block = zeros(...
                        InputImage(RunningParameters.SelectedFile).SizeX,...
                        InputImage(RunningParameters.SelectedFile).SizeY...
                    );
synthetic_block = block;
                    
page = InputImage(RunningParameters.SelectedFile).CurrentIndex;
                    if(RunningParameters.RAM == 0)
                        block(:,:) = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],'Index',page);
                    elseif(RunningParameters.RAM == 1)
                        % RAM version                        
                        block(:,:) = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,page);
                    end
        
                    disp('Image loaded correctly');

                    watershed2D(block);
                    disp('Segmentation completed');
                case{'Connected neighbors'}
                    [xVect, yVect] = interactiveConnectedNeighbors();
                    
%                    [ x_group, y_group, x_vect, y_vect, neighb ] =
%                    conn_neigh( x0, y0, x_vect, y_vect, dist, neighb );
                case{'Local maxima'}
                    % TO DO%
            end
    case{'xyz'} % 3D segmentation, once only ..............................
        block = zeros(menu_hdlr.sizes(indY),menu_hdlr.sizes(indX),menu_hdlr.sizes(indZ));

synthetic_block = block;
        
        for Z = 1:InputImage.SliceNumber
        	current_ind(indZ -2) = Z;
            page(Z) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
            if(RunningParameters.RAM == 0)
                % HD version
                block(:,:,Z) = imread([InputImage.Path,'\',InputImage.Name],'Index',page(Z));
            elseif(RunningParameters.RAM == 1)
                % RAM version
                block(:,:,Z) = HYPERSTACK(:,:,page(Z));
            end
            
        end
        disp('Stack loaded correctly');
        
        switch(seg_method)        
            case{'Watershed'}        
                watershed3D(block);
                disp('Segmentation completed');
            case{'Connected neighbors'}
                
            case{'Local maxima'}
                XYproj = XYprojection_MIP(block);
                XZproj = XZprojection_MIP(block);
                YZproj = YZprojection_MIP(block);

                extremes = [min([min(XYproj(:)),min(XZproj(:)),min(YZproj(:))]),...
                            max([max(XYproj(:)),max(XZproj(:)),max(YZproj(:))])];
                
                thrbk = thr; % backup the threshold setting
                % provisional threshold to supress the pixels with the 20%
                % lowest values. This worked really well supressing false
                % positives on confocal images.

                thr = extremes(1)+(extremes(2)-extremes(1))/5;

                [XYproj, ~, ~] = threshold(XYproj, thr);
                [XZproj, ~, ~] = threshold(XZproj, thr);
                [YZproj, ~, ~] = threshold(YZproj, thr);
                
                thr = thrbk; % recover the threshold
                
                ortho_slices_win = ortho_slices();
                ortho_slices_hdl  = guidata(ortho_slices_win);

                    last_set = [];
                    imshow(XYproj,[],'Parent',ortho_slices_hdl.axesXY,'Initialmagnification','fit');
                    for size_ = largest_obj:-1:smallest_obj
                        delta = fix(size_/2);         
                        XYmaxima = localmaxima2D(XYproj,size_);                                        
                        overlaps = find_overlaps(XYmaxima,last_set,delta);
                        XYmaxima(overlaps,:) = [];

                        ll = length(XYmaxima);
                        if (ll>0)
                            disp([num2str(ll),' clusters of ',num2str(size_),'pix.']);
                        end
                        hold(ortho_slices_hdl.axesXY,'on');                        
                        scatter(XYmaxima(:,2),XYmaxima(:,1),size_,'Parent',ortho_slices_hdl.axesXY,'filled');
                        pause(0.05);
                        hold(ortho_slices_hdl.axesXY,'off');
                        
                        last_set = [last_set; XYmaxima];
                    end
                    disp('...')
                    last_set = [];
                    imshow(XZproj',[],'Parent',ortho_slices_hdl.axesXZ,'Initialmagnification','fit');
                    for size_ = largest_obj:-1:smallest_obj
                        delta = fix(size_/2);         
                        XZmaxima = localmaxima2D(XZproj,size_);                                        
                        overlaps = find_overlaps(XZmaxima,last_set,delta);
                        XZmaxima(overlaps,:) = [];

                        ll = length(XZmaxima);
                        if(ll>0)
                            disp([num2str(ll),' clusters of ',num2str(size_),'pix.']);                        
                        end
                        hold(ortho_slices_hdl.axesXZ,'on');                     
                        scatter(XZmaxima(:,1),XZmaxima(:,2),size_,'Parent',ortho_slices_hdl.axesXZ,'filled');
                        pause(0.05);
                        hold(ortho_slices_hdl.axesXZ,'off');
                        
                        last_set = [last_set; XZmaxima];
                    end
                    disp('...')
                    last_set = [];
                    imshow(YZproj',[],'Parent',ortho_slices_hdl.axesYZ,'Initialmagnification','fit');
                    for size_ = largest_obj:-1:smallest_obj
                        delta = fix(size_/2);         
                        YZmaxima = localmaxima2D(YZproj,size_);                                        
                        overlaps = find_overlaps(YZmaxima,last_set,delta);
                        YZmaxima(overlaps,:) = [];

                        ll = length(YZmaxima);
                        if(ll>0)
                            disp([num2str(ll),' clusters of ',num2str(size_),'pix.']);
                        end
                        hold(ortho_slices_hdl.axesYZ,'on');                     
                        scatter(YZmaxima(:,1),YZmaxima(:,2),size_,'Parent',ortho_slices_hdl.axesYZ,'filled');
                        pause(0.05);
                        hold(ortho_slices_hdl.axesYZ,'off');
                        
                        last_set = [last_set; YZmaxima];
                    end
                    disp('...')
        end
    case{'xyt'} % 2D segmentation, every time frame .......................
        
        block = zeros(menu_hdlr.sizes(indY),menu_hdlr.sizes(indX),menu_hdlr.sizes(indT));
        synthetic_block = block;
        
        for T = 1:InputImage.FrameNumber
            current_ind(indT -2) = T;
            page(T) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
            if(RunningParameters.RAM == 0)            
                % HD version
                block(:,:,T) = imread([InputImage.Path,'\',InputImage.Name],'Index',page(T));
            elseif(RunningParameters.RAM == 1)            
                % RAM version
                block(:,:,T) = HYPERSTACK(:,:,page(T));
            end
%             watershed2D(block(:,:,T));
        end
        disp('Movie loaded correctly');
        for T = 1:InputImage.FrameNumber
            watershed2D(block(:,:,T));            
        end
        disp('Segmentation completed');
        
    case{'xyzt'} % 3D segmentation, every time frame ......................
                
        synthetic_img_name = [InputImage.Name(1:strfind(InputImage.Name,'.')-1),' - synthetic.tif'];
        write_metadata( InputImage.Path, synthetic_img_name, img_data);    
        
        block = zeros(menu_hdlr.sizes(indY),menu_hdlr.sizes(indX),menu_hdlr.sizes(indZ),menu_hdlr.sizes(indT));
        synthetic_block = block;
        
        for T = 1:InputImage.FrameNumber
            for Z = 1:InputImage.SliceNumber
                current_ind(indT -2) = T;
                current_ind(indZ -2) = Z;
                page(Z,T) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
                if(RunningParameters.RAM == 0)                
                    % HD version
                    block(:,:,Z,T) = imread([InputImage.Path,'\',InputImage.Name],'Index',page(Z,T));
                elseif(RunningParameters.RAM == 1)
                    % RAM version
                    block(:,:,Z,T) = HYPERSTACK(:,:,page(Z,T));
                end
            end
        end
        disp('Hiperstack loaded correctly');
        for T = 1:InputImage.FrameNumber

            TMP = watershed3D(block(:,:,:,T));
            synthetic_block(:,:,:,T) = TMP;
            for Z = 1:InputImage.SliceNumber
                page(Z,T) = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
                imwrite(synthetic_block(:,:,Z,T),[InputImage.Path,'/',synthetic_img_name],'WriteMode','append', 'Compression','none');
            end
            fprintf(1, 'Frame %d of %d. \n',T,InputImage.FrameNumber);
        end
        
        disp('Segmentation completed');
    end


% --- Executes when selected object is changed in apply_option.
function apply_option_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in apply_option 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

    global seg_menu

    switch(hObject)
        case{handles.seg_snapshot}
            % Segment the current snapshot only
            disp('Segment the current snapshot');
            seg_menu = 'xy';
        case{handles.seg_stack}
            % Segment the curtrent stack
            disp('Segment the curtrent Z stack');
            seg_menu = 'xyz';
        case{handles.seg_movie}
            % Segment the time series for the current slice
            disp('Segment the time series for the current slice');
            seg_menu = 'xyt';
        case{handles.seg_all}
            % Segmment the entire series
            disp('Segmment the 4D series')
            seg_menu = 'xyzt';
    end


% --- Executes during object creation, after setting all properties.
function apply_option_CreateFcn(hObject, eventdata, handles)
% hObject    handle to apply_option (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called




% --- Executes during object creation, after setting all properties.
function seg_stack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seg_stack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global InputImage
    global RunningParameters

    if (InputImage(RunningParameters.SelectedDisplay).SliceNumber < 2)
        set(hObject,'Enable','off');
    end


% --- Executes during object creation, after setting all properties.
function seg_movie_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seg_movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global InputImage
    global RunningParameters

    if (InputImage(RunningParameters.SelectedDisplay).FrameNumber < 2)
        set(hObject,'Enable','off');
    end

% --- Executes during object creation, after setting all properties.
function seg_all_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seg_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global InputImage

    if (InputImage(1).SliceNumber < 2) && (InputImage(1).FrameNumber < 2)
        set(hObject,'Enable','off');
    end



function smallest_box_Callback(hObject, eventdata, handles)
% hObject    handle to smallest_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of smallest_box as text
%        str2double(get(hObject,'String')) returns contents of smallest_box as a double

    global smallest_obj
        smallest_obj = str2num(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function smallest_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to smallest_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function biggest_box_Callback(hObject, eventdata, handles)
% hObject    handle to biggest_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of biggest_box as text
%        str2double(get(hObject,'String')) returns contents of biggest_box as a double

    global largest_obj
        largest_obj = str2num(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function biggest_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to biggest_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes during object creation, after setting all properties.
function segmentDialogFigure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to segmentDialogFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    global seg_menu
    

    seg_menu = 'xy';    



function allowedSeparationBox_Callback(hObject, eventdata, handles)
% hObject    handle to allowedSeparationBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of allowedSeparationBox as text
%        str2double(get(hObject,'String')) returns contents of allowedSeparationBox as a double

    global allowedSeparation
    allowedSeparation = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function allowedSeparationBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to allowedSeparationBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
