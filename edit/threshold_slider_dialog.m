function varargout = threshold_slider_dialog(varargin)
% THRESHOLD_SLIDER_DIALOG MATLAB code for threshold_slider_dialog.fig
%      THRESHOLD_SLIDER_DIALOG, by itself, creates a new THRESHOLD_SLIDER_DIALOG or raises the existing
%      singleton*.
%
%      H = THRESHOLD_SLIDER_DIALOG returns the handle to a new THRESHOLD_SLIDER_DIALOG or the handle to
%      the existing singleton*.
%
%      THRESHOLD_SLIDER_DIALOG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THRESHOLD_SLIDER_DIALOG.M with the given input arguments.
%
%      THRESHOLD_SLIDER_DIALOG('Property','Value',...) creates a new THRESHOLD_SLIDER_DIALOG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before threshold_slider_dialog_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to threshold_slider_dialog_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help threshold_slider_dialog

% Last Modified by GUIDE v2.5 17-May-2017 17:07:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @threshold_slider_dialog_OpeningFcn, ...
                   'gui_OutputFcn',  @threshold_slider_dialog_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before threshold_slider_dialog is made visible.
function threshold_slider_dialog_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to threshold_slider_dialog (see VARARGIN)

% UIWAIT makes threshold_slider_dialog wait for user response (see UIRESUME)
% uiwait(handles.figure1);
    
    global RunningParameters

    sendWindowUnder(hObject,...
    RunningParameters.InfoWindowPosition,...
    RunningParameters.WindowOffsetY);

    drawnow;

%     sendWindowRight(gcf,...
%     RunningParameters.InfoWindowPosition,...
%     RunningParameters.WindowOffsetX);

%     drawnow;

    % Choose default command line output for threshold_slider_dialog
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = threshold_slider_dialog_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function thr_slider_Callback(hObject, eventdata, handles)
% hObject    handle to thr_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global thr preview sizeX sizeY thr_img prev_win_hdl menu_hdlr
global x_vect y_vect current_ind HYPERSTACK page_no


global ApplicationHandles
global RunningParameters
global InputImage InputFile


    %Read slider position and convert it to a threshold value:
    thr = get(handles.thr_slider,'Value');
    set(handles.thr_txtbox,'String',num2str(thr));
    fprintf('%d threshold selected \n',thr);
        
    % Apply threshold:
    
    %page = InputImage(RunningParameters.SelectedFile).CurrentIndex;
    if(RunningParameters.RAM == 0)    
        % HD version
        preview = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],...
        'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);  
    elseif(RunningParameters.RAM == 1)
        % RAM version    
        preview = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
    end
    

    % {x_vect, y_vect} are the members of a mask above the threshold
    [ thr_img, x_vect, y_vect, ~ ] = threshold( preview, thr );
    
    % Display result:

    imshow(thr_img,[min(thr_img(:)) max(thr_img(:))],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay), 'InitialMagnification', 'fit');
        
    % Calculate histogram:
    s_buff = reshape(preview,1,sizeX*sizeY);
    s_ind = s_buff > 100;
histogram(s_buff, double((max(s_buff)-min(s_buff))/100),'Parent', handles.histogram_axes);
hst = hist(double(s_buff(s_ind)),double(50));
    
    %Plot histogram. Set a mark at the threshold point
%     hist(double(s_buff(s_ind)),double(50),'Parent', handles.histogram_axes);       
    line([thr,thr],[0,max(hst(:))],'Parent', handles.histogram_axes,'Color','r');

imshow(preview,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));

% --- Executes during object creation, after setting all properties.
function thr_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thr_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
global thr preview img_max img_min current_ind menu_hdlr HYPERSTACK page_no
global RunningParameters InputImage InputFile


    CurrentIndex = InputImage(RunningParameters.SelectedDisplay).CurrentIndex;

    if (CurrentIndex > 1)
        % page = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
        page = CurrentIndex;
    else
        page = 1;
    end
    
    if(RunningParameters.RAM == 0)
        % HD version
        disp('HD')
        preview = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedDisplay).Name],...
        'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);    
        
    elseif(RunningParameters.RAM == 1)
        % RAM version
        disp('RAM')
        preview = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
    end
    
    
    if(RunningParameters.RAM == 1)
    	InputImage(RunningParameters.SelectedDisplay).Buffer = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
	elseif(RunningParameters.RAM == 0)
        InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],...
        'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
    end
    
    
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    thr = 0;                                           % Set threshold to 0

    img_min = min(preview(:))
    img_max = max(preview(:))

    set(hObject,'Value',thr);
    set(hObject,'Max',fix(img_max));

    set(hObject,'SliderStep',[1, 1/(double(img_max)-double(img_min))]);
    set(hObject,'Value',0);     

function thr_txtbox_Callback(hObject, eventdata, handles)
% hObject    handle to thr_txtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of thr_txtbox as text
%        str2double(get(hObject,'String')) returns contents of thr_txtbox as a double
global preview thr sizeX sizeY prev_win_hdl menu_hdlr
global x_vect y_vect thr_img current_ind HYPERSTACK
global RunningParameters    

    page = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
    if(RunningParameters.RAM == 0)
        % HD version
        preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
    elseif(RunningParameters.RAM == 1)
        % RAM version
        preview = HYPERSTACK(:,:,page);
    end
    
    % Find dynamic ranges .................................................
    MX = max(max(preview));
    mx = min(min(preview));
    
    % Display images ......................................................
    imshow(preview,[min(preview(:)) max(preview(:))],...
           'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');
    hist(double(reshape(preview,1,sizeX*sizeY)),double(30),...
           'Parent', handles.histogram_axes);
    
    
    % Ask user for a threshold value ......................................
    % (The callback is executed after pressing [enter] so a value should be
    % typed in the box already)
    input = str2double(get(handles.thr_txtbox,'String'));
    
    % And... do it:
    if (input < mx) || (input > MX)
        % Threshold out of range ..........................................
        fprintf(1,'Threshold out of range \n');
        set(handles.thr_txtbox,'String','Out of range');        
        thr = mx;
        set(handles.thr_slider,'Value',thr);
    else
        % Threshold fine ..................................................
        thr = input;                           % Upgrade threshold globally
        set(handles.thr_slider,'Value',thr);   % Outputs: text & slider
        fprintf(1,'%d threshold selected \n',thr);

        % Apply threshold:
        thr_img = preview;                % Apply changes to a buffer image            
        [ thr_img, x_vect, y_vect, ~ ] = threshold( thr_img, thr );
        
        length(x_vect)    % How many points there exist above the threshold
        
        % Display the thresholded (buffer) image ..........................
        imshow(thr_img,[min(thr_img(:)) max(thr_img(:))],'Parent',handles.image_axes, 'InitialMagnification', 'fit');
        
        % Upgrade image histogram .........................................
        s_buff = reshape(preview,1,sizeX*sizeY);
        s_ind = s_buff > 100;    
        hst = hist(double(s_buff(s_ind)),double(50));
        %Plot histogram. Set a mark at the threshold point
        hist(double(s_buff(s_ind)),double(50),'Parent', handles.histogram_axes);
        line([thr,thr],[0,max(max(hst))],'Parent', handles.histogram_axes,'Color','r');
    end    
    
imshow(preview,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));



function thr_txtbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thr_txtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes during object creation, after setting all properties.
function histogram_axes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to histogram_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate histogram_axes

global menu_hdlr current_ind sizeX sizeY HYPERSTACK
global RunningParameters

    page = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
    if(RunningParameters.RAM == 0)
        % HD version
        preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
    elseif(RunningParameters.RAM == 1)
        % RAM version
        preview = HYPERSTACK(:,:,page);
    end
       
    % Calculate histogram:
    s_buff = reshape(preview,1,sizeX*sizeY);
    s_ind = s_buff > 100;
    hist(double(s_buff(s_ind)),double(50),'Parent', hObject);

    handles.histogram_axes = hObject;
    guidata(gcf,handles);


% --- Executes on button press in close_btn.
function close_btn_Callback(hObject, eventdata, handles)
% hObject    handle to close_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.figure1)
% other objects associated have to be deleted also


% --- Executes on button press in apply_thr_btn.
function apply_thr_btn_Callback(hObject, eventdata, handles)
% hObject    handle to apply_thr_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global page_no img_path img_name img_data sizeX sizeY 
global prev_win_hdl HYPERSTACK
global log_file_name
global RunningParameters

thresholded_img_name = [img_name,' - thresholded','.tif'];
binary_img_name      = [img_name,' - binary','.tif'];

output_file = Tiff([img_path,'/',thresholded_img_name],'w');

% write headers
header_success = write_metadata( img_path, thresholded_img_name, img_data);
header_success = write_metadata( img_path, binary_img_name, img_data);

wbar = waitbar(page_no,['page 1 of',num2str(page_no)]); % Progress bar
movegui(wbar, [-50,-550]);


for page = 1:page_no
    if(RunningParameters.RAM == 0)
        % HD version
        preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
    elseif(RunningParameters.RAM == 1)
        % RAM version
        preview = HYPERSTACK(:,:,page);
    end
    [ thr_img, maskX, maskY, positives ] = threshold( preview, thr );
    bin_img = zeros(sizeY, sizeX);
    for ind = 1:positives
        bin_img(maskX(ind),maskY(ind)) = 1;
    end
%     imshow(bin_img,[0 1],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');

    imwrite(thr_img,[img_path,'/',thresholded_img_name],'WriteMode','append', 'Compression','none');
    imwrite(bin_img,[img_path,'/',binary_img_name],'WriteMode','append', 'Compression','none');

    waitbar((page)/(page_no),wbar,...                    % Progress bar
            ['page',num2str(page),' of ',num2str(page_no),'.']);
end
close(wbar);
imshow(thr_img,[min(thr_img(:)) max(thr_img(:))],'Parent',prev_win_hdl.image_axes, 'InitialMagnification', 'fit');

    log_hdl = fopen([img_path,'\',log_file_name],'a+');
    fprintf(log_hdl,'THRESHOLD THR:%d',thr);
    fprintf(log_hdl,'\n');
    fclose(log_hdl);


% --- Executes on key press with focus on apply_thr_btn and none of its controls.
function apply_thr_btn_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to apply_thr_btn (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
