function [synthetic_block] = watershed3D( block )
%watershed3D Watershed appied to a stack

global prev_win_hdl slice_no smallest_obj largest_obj thr
    
    % A 5x5x5 matrix containing a sphe of 5 pix of diameter
    msize = 20;
    diameter = 20;
    range = (-1)*fix(msize/2):fix(msize/2);
    delta = max(range);
    ball = sphere(length(range),diameter); 
    
    [sbX, sbY] = size(block(:,:,1));

    synthetic_block = zeros(size(block));       % Initialize the output
    binary_block = zeros(size(block));        % Initialize the binary image

    for Z = 1:slice_no
        [ thr_block(:,:,Z), maskX, maskY, positives ] = threshold( block(:,:,Z), thr );
        for ind = 1:positives
            binary_block(maskX(ind),maskY(ind),Z) = 1;
        end
    end
    
    distances = bwdist(binary_block);             % Distance transformation
    distances = -distances;    
	distances(~binary_block) = - Inf;             % Pixels under threshold are not close to anything        
    segmented = watershed(distances);

    % Latest label, or number of selected objects
    object_no = max(segmented(:));

    for object = 1:object_no
        Lpositions = segmented == object;
        pX = [];
        pY = [];
        pZ = [];
        for X = 1:sbX
            for Y = 1:sbY
                for Z = 1:slice_no
                    if (segmented(X,Y,Z) == object)
                        pX = [pX, X];
                        pY = [pY, Y];
                        pZ = [pZ, Z];
                    end
                end
            end
        end
        position_no = length(pX);

        if (position_no > largest_obj)
            disp('big object');
            segmented(Lpositions) = 0;        
        elseif(position_no < smallest_obj)
            disp('too small object');
            segmented(Lpositions) = 0;              
        else
            % Calculate the center of mass
%             coordinates = segmented(positions)
            weigths = ones(position_no,1);
            centroid = centroid3D( [pX;pY;pZ], weigths );
            centroid = fix(centroid);
            
            rangeX = range;
            rX = rangeX - min(range)+1;
            rangeX((centroid(1)+range <1)|(centroid(1)+range > sbX))= [];
            rX((centroid(1)+range <1)|(centroid(1)+range > sbX))= [];               

            rangeY = range;
            rY = rangeY - min(range)+1;
            rangeY((centroid(2)+range <1)|(centroid(2)+range > sbY))= [];
            rY((centroid(2)+range <1)|(centroid(2)+range > sbY))= [];

            rangeZ = range;
            rZ = rangeZ - min(range)+1;
            rangeZ((centroid(3)+range <1)|(centroid(3)+range > slice_no))= [];
            rZ((centroid(3)+range <1)|(centroid(3)+range > slice_no))= [];

            synthetic_block(centroid(1)+rangeX,...
                            centroid(2)+rangeY,...
                            centroid(3)+rangeZ) = ...
                            ball(rX,rY,rZ);
 
        end    
    end
    
    
    
    radius = 0.5;
    [sy,sx,sz]= size(block);
    [x,y,z] = meshgrid(1:sx,1:sy,1:sz);
    figure(88)
    isosurface(x,y,z,segmented,radius/2), axis equal
    
end

