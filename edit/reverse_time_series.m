function [] = reverse_time_series()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

global slice_no frame_no channel_no img_data
global img_path img_name menu_hdlr HYPERSTACK



    [~,frame_index  ] = find(strcmp(menu_hdlr.indices,'ind_t'));
    [~,slice_index  ] = find(strcmp(menu_hdlr.indices,'ind_z'));
    [~,channel_index] = find(strcmp(menu_hdlr.indices,'ind_c'));

    % The first 2 positions are for indX and indY
    
    frame_index   = frame_index -2;
    slice_index   = slice_index -2;
    channel_index = channel_index -2;
    
    sizes = menu_hdlr.sizes;

    
    
    reversed_img_name = [img_name(1:strfind(img_name,'.')-1),' - reversed','.tif'];
    header_success = write_metadata( img_path, reversed_img_name, img_data);
    
    wbar = waitbar(slice_no); % Progress bar
    movegui(wbar, [-50,-550]);    
    for slice = 1:slice_no
        current_ind(slice_index) = slice;
        for channel = 1:channel_no
            current_ind(channel_index) = channel;
            for frame = 1:frame_no
                current_ind(frame_index) = frame_no - frame + 1; % sequence reversed
                page = menu_hdlr.matrix(current_ind(1),current_ind(2),current_ind(3));
                % HD version
%                 preview = imread([menu_hdlr.img_path,'\',menu_hdlr.img_name],'Index',page);
                % RAM version
                preview = HYPERSTACK(:,:,page);
                imwrite(preview,[img_path,'/',reversed_img_name],'WriteMode','append', 'Compression','none');
                pause(0.01);
            end
        end
        waitbar((slice)/(slice_no),wbar);
    end
    close(wbar);

