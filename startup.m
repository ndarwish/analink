function [  ] = startup()
%STARTUP Run this function to start the suite
%   This first will load the paths so the rest of the sw can use them for
%   accessin the functions

    % Start propperly:    
    clear all;
    clc;
    
    
    % Add the subfolders to the search path, to make all the functions
    % available:
    add_all_paths(pwd);

    % Initialize logging. This avoids problems with the Bioformats Jar.
    %   Function call needed to avoid the 'no appenders found for logger' 
    %   Java error... don't ask me:
    
    bfInitLogging();    
       
    initialize_structures();
    
    % Then, all the rest:    
    warning off all;
    main();

end

