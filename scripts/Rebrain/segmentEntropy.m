    function [segmentedImage, indexedImage] = segmentEntropy(inputImage, thresholdVal, thresholdPixArea)
      
        entropyThreshold = thresholdVal;
           
        segmentedImage = segmentByTexture(inputImage, entropyThreshold,'entropy');
          
        indexedImage = bwconncomp(segmentedImage);                
        segmentedImage = uint8(filterIslandsBySize(segmentedImage, thresholdPixArea)).*uint8(segmentedImage);
            



