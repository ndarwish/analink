    function [segmentedImage] = segmentInteractively(inputImage, thresholdVal, thresholdPixArea, method)
      
        switch method
            
            case 'std'
            STDThreshold = thresholdVal;                                   % STDThreshold = 0.2;
            segmentedImage = segmentByTexture(inputImage, STDThreshold, 'std');                

            case 'entropy'
            entropyThreshold = thresholdVal;
           
            segmentedImage = segmentByTexture(inputImage, entropyThreshold,'entropy');
          
            indexedImage = bwconncomp(segmentedImage);
            cellCount = indexedImage.NumObjects;
            centroids = regionprops(indexedImage,'Centroid');
            segmentedImage = uint8(filterIslandsBySize(segmentedImage, thresholdPixArea)).*uint8(segmentedImage);
            
            case 'connectedComponents'
                % Apply some threshod initially:
                [ ~, maskX, maskY, positives ] = threshold( inputImage, thresholdVal );
                
                Xv = [];
                Yv = [];
                while(1)
                    
                    MM = max(inputImage(:));
                    if (MM == 0) 
                        break;
                    end
                    % Find seed points for the objects
                    [seedX, seedY] = find(inputImage == MM);
                    % Segment from the seed:
                    recursionLevelAvail = 450; %Matlab's recursion limit.
                    [ ~, ~, x_vect, y_vect, ~ ] = connNeighLimitedRecursion( seedX, seedY, maskX, maskY, 1, 1 , recursionLevelAvail)
                    
                    % Remove the segmented pixels. Useful if the recursion
                    % limit is surpassed and the function needs to return.
                    inputImage(x_vect, y_vect) = 0;
                    % Append the new elements to the tables
                    Xv = [Xv, x_vect];
                    Yv = [Yv, y_vect];
                end
                segmentedImage = uint8(filterIslandsBySize(segmentedImage, thresholdPixArea)).*uint8(segmentedImage);
            case 'watershed'                
                indexedImage = watershedSegment( inputImage, thresholdVal );
                segmentedImage = uint8(filterIslandsBySize(indexedImage, thresholdPixArea)).*uint8(indexedImage);
            otherwise
                fprintf(1, 'No method defined. This case should be unreachable');
                indexedImage = zeros(size(inputImage));
        end


