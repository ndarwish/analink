function varargout = segmentOptions(varargin)
% SEGMENTOPTIONS MATLAB code for segmentOptions.fig
%      SEGMENTOPTIONS, by itself, creates a new SEGMENTOPTIONS or raises the existing
%      singleton*.
%
%      H = SEGMENTOPTIONS returns the handle to a new SEGMENTOPTIONS or the handle to
%      the existing singleton*.
%
%      SEGMENTOPTIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEGMENTOPTIONS.M with the given input arguments.
%
%      SEGMENTOPTIONS('Property','Value',...) creates a new SEGMENTOPTIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before segmentOptions_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to segmentOptions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help segmentOptions

% Last Modified by GUIDE v2.5 07-Aug-2018 18:47:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @segmentOptions_OpeningFcn, ...
                   'gui_OutputFcn',  @segmentOptions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before segmentOptions is made visible.
function segmentOptions_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to segmentOptions (see VARARGIN)

% Choose default command line output for segmentOptions
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes segmentOptions wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = segmentOptions_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function areaThresholdSlider_Callback(hObject, eventdata, handles)
% hObject    handle to areaThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    global InputImage RunningParameters
    
    LogAreaPixVal = get(hObject,'Value');
    areaPixVal = ceil(exp(double(LogAreaPixVal)));
    InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = areaPixVal;
	set(handles.areaThresholdBox,'String',num2str(...
        InputImage(RunningParameters.SelectedDisplay).AreaThreshold));  

    % Update handles structure
    guidata(hObject, handles);
    



% --- Executes during object creation, after setting all properties.
function areaThresholdSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.

    global InputImage RunningParameters

    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

    minPixArea = 1;
    maxPixArea = InputImage(RunningParameters.SelectedDisplay).SizeX * ...
                 InputImage(RunningParameters.SelectedDisplay).SizeY;

    lMin = log(double(minPixArea));
    lMax = log(double(maxPixArea));
    
	set(hObject,'Min',lMin);
    set(hObject,'Max',lMax);
    
    set(hObject,'SliderStep',[1/(lMax-lMin), 0.1]);


    set(hObject,'Value',lMax);
    InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = maxPixArea;
    
	% Update handles structure    
    guidata(hObject, handles);    


function areaThresholdBox_Callback(hObject, eventdata, handles)
% hObject    handle to areaThresholdBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of areaThresholdBox as text
%        str2double(get(hObject,'String')) returns contents of areaThresholdBox as a double

    global InputImage RunningParameters
    
    contextVal = str2double(get(hObject,'String'));
    set(handles.areaThresholdSlider,'Value',log10(contextVal));
    
    InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = contextVal;
    
	% Update handles structure    
    guidata(hObject, handles);        

% --- Executes during object creation, after setting all properties.
function areaThresholdBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaThresholdBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.

    global InputImage RunningParameters

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    maxPixArea = InputImage(RunningParameters.SelectedDisplay).SizeX * ...
                 InputImage(RunningParameters.SelectedDisplay).SizeY;             
    set(hObject,'String',num2str(log10(maxPixArea)));
	
    InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = maxPixArea;
    
    % Update handles structure    
    guidata(hObject, handles);        

function contextParamBox_Callback(hObject, eventdata, handles)
% hObject    handle to contextParamBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of contextParamBox as text
%        str2double(get(hObject,'String')) returns contents of contextParamBox as a double

global InputImage RunningParameters ApplicationHandles

    contextVal = str2double(get(hObject,'String'));
    set(handles.areaThresholdSlider,'Value',contextVal);

    switch(handles.segmentationMethodGroup.SelectedObject)
        case{handles.STDSegCheckbox}
            % Standard deviation method
            disp('Standard deviation chosen');
             
        case{handles.entropySegCheckbox}
            % Entropy method
            disp('Entropy chosen');

        case{handles.connectedCompSegCheckbox}
            % Connected components
            disp('Connected components chosen');
            set(handles.contextParamBox,'String',num2str(contextVal));
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;
            % Display the effect of the selection
            [ th_img, ~, ~, ~ ] = threshold( InputImage(RunningParameters.SelectedDisplay).Buffer, InputImage(RunningParameters.SelectedDisplay).Threshold );
            imshow(th_img,[0, max(th_img(:))],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
        case{handles.watershedSegCheckbox}            
            % Watershed:
            set(handles.contextParamBox,'String',num2str(contextVal));
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;
            % Display the effect of the selection
            [ th_img, ~, ~, ~ ] = threshold( InputImage(RunningParameters.SelectedDisplay).Buffer, InputImage(RunningParameters.SelectedDisplay).Threshold );
            
            [bitDepth, maxRange] = findPixDepth(InputImage(RunningParameters.SelectedDisplay));
            [indexedImg, saturationMap] = generateSaturationMap(th_img, maxRange);
            hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');
            imgObject = imshow(indexedImg,saturationMap,'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
            alphaData = ~indexedImg;
            set(imgObject, 'AlphaData', alphaData);
            hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');
    end            
    
	% Update handles structure    
    guidata(hObject, handles);    



% --- Executes during object creation, after setting all properties.
function contextParamBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to contextParamBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function contextParameterSlider_Callback(hObject, eventdata, handles)
% hObject    handle to areaThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global InputImage RunningParameters ApplicationHandles

    contextVal = get(hObject,'Value');
    set(handles.contextParamBox,'String',num2str(double(contextVal)));
    
%     InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedDisplay).Path,...
%         '\',InputFile(RunningParameters.SelectedFile).Name],...
%         'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);
        
    maxR = max(InputImage(RunningParameters.SelectedDisplay).Buffer(:));

    switch(handles.segmentationMethodGroup.SelectedObject)
        case{handles.STDSegCheckbox}
            % Standard deviation method
            disp('Standard deviation chosen');
             
        case{handles.entropySegCheckbox}
            % Entropy method
            disp('Entropy chosen');

            thresholdPixArea = InputImage(RunningParameters.SelectedDisplay).AreaThreshold;
            entropyThresholdVal = contextVal;   % In this context it is the entropy threshold
            segmentedImage = segmentInteractively(InputImage(RunningParameters.SelectedDisplay).Buffer, entropyThresholdVal, thresholdPixArea,RunningParameters.SegmentationMethod);
            [~,vertexSubset] = findBorders(segmentedImage);

            imshow(InputImage(RunningParameters.SelectedDisplay).Buffer,[0 max(InputImage(RunningParameters.SelectedFile).Buffer(:))],...
                'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));            
            
            try
                hold(ApplicationHandles.ViewerAxes(end-1),'on');
                scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(end-1),'r');
                hold(ApplicationHandles.ViewerAxes(end-1),'off');
            catch
                disp('empty result');
            end

        case{handles.connectedCompSegCheckbox}
            % Connected components
            disp('Connected components chosen');
            set(handles.contextParamBox,'String',num2str(contextVal));
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;
            % Display the effect of the selection
            [ th_img, ~, ~, ~ ] = threshold( InputImage(RunningParameters.SelectedDisplay).Buffer, InputImage(RunningParameters.SelectedDisplay).Threshold );            
            imshow(th_img,[0, max(th_img(:))],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
            
        case{handles.watershedSegCheckbox}
            set(handles.contextParamBox,'String',num2str(contextVal));
            [bitDepth, maxRange] = findPixDepth(InputImage(RunningParameters.SelectedDisplay));
            selectedThreshold = contextVal*maxR;
            InputImage(RunningParameters.SelectedDisplay).Threshold = selectedThreshold;

            % Display the effect of the selection
            
            [ th_img, ~, ~, ~ ] = threshold( InputImage(RunningParameters.SelectedDisplay).Buffer, selectedThreshold);
                       
            [indexedImg, saturationMap] = generateSaturationMap(th_img, maxRange);
                   
            imshow(InputImage(RunningParameters.SelectedDisplay).Buffer,[0 max(InputImage(RunningParameters.SelectedFile).Buffer(:))],...
               'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
           
%             imgObject = imshow(indexedImg,saturationMap,'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
%             alphaData = ~indexedImg;
%             set(imgObject, 'AlphaData', alphaData);
  
        
            
thresholdPixArea = InputImage(RunningParameters.SelectedDisplay).AreaThreshold;
ThresholdVal = contextVal;              % Now the parameter is the level threshold
segmentedImage = segmentInteractively(th_img, ThresholdVal, thresholdPixArea,RunningParameters.SegmentationMethod);
[~,vertexSubset] = findBorders(segmentedImage);

hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');
scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'r');
hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');

% try
%     if (legth(ApplicationHandles.ViewerAxes) > 1)
%         hold(ApplicationHandles.ViewerAxes(end-1),'on');
%         scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(end-1),'r');
%         hold(ApplicationHandles.ViewerAxes(end-1),'off');
%     else
%         hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');
%         scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'r');        
%         hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');
%     end
% catch
%     disp('empty result');
% end            
            
    end
    
	% Update handles structure    
    guidata(hObject, handles);
    


% --- Executes during object creation, after setting all properties.
function contextParameterSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes when selected object is changed in segmentationMethodGroup.
function segmentationMethodGroup_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in segmentationMethodGroup 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global RunningParameters InputImage
    
    
    maxLevel = max(InputImage(RunningParameters.SelectedDisplay).Buffer(:));
    minLevel = min(InputImage(RunningParameters.SelectedDisplay).Buffer(:));
            
    switch(hObject)
        case{handles.STDSegCheckbox}
            % Standard deviation method
            disp('Standard deviation chosen');
            set(handles.contextParamTitle,'String','STD');           
            RunningParameters.SegmentationMethod = 'std';
            
            contextVal = get(handles.contextParameterSlider,'Value');              
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;
            
        case{handles.entropySegCheckbox}
            % Entropy method
            disp('Entropy chosen');
            set(handles.contextParamTitle,'String','Entropy');
            RunningParameters.SegmentationMethod = 'entropy';
            
            contextVal = 0;
            set(handles.contextParameterSlider,'Value',contextVal);
            set(handles.contextParamBox,'String',num2str(double(contextVal)));
            set(handles.areaThresholdSlider,'Value',1);
            InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = 1;
                        
            contextVal = get(handles.contextParameterSlider,'Value');              
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;            
            
        case{handles.connectedCompSegCheckbox}
            % Connected components
            disp('Connected components chosen');
            set(handles.contextParamTitle,'String','Level');
            RunningParameters.SegmentationMethod = 'connectedComponents';
        case{handles.watershedSegCheckbox}
            % Watershed
            disp('Watershed chosen');

            set(handles.contextParamTitle,'String','Threshold');
            contextVal = 0;
            set(handles.contextParameterSlider,'Value',contextVal);
            set(handles.contextParamBox,'String',num2str(double(contextVal)));
            set(handles.areaThresholdSlider,'Value',1);
            InputImage(RunningParameters.SelectedDisplay).AreaThreshold  = 1;
            
            
%             contextVal = get(handles.contextParameterSlider,'Value');      
            set(handles.contextParamBox,'String',num2str(contextVal));
            fprintf('%d default threshold.\n',contextVal);
            
            RunningParameters.SegmentationMethod = 'watershed';
            InputImage(RunningParameters.SelectedDisplay).Threshold = contextVal;
    end
    
    % Update handles structure    
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function segmentationMethodGroup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to segmentationMethodGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global RunningParameters

RunningParameters.SegmentationMethod = 'watershed';


% --- Executes during object deletion, before destroying properties.
function areaThresholdSlider_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to areaThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in watershedSegCheckbox.
function watershedSegCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to watershedSegCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of watershedSegCheckbox
