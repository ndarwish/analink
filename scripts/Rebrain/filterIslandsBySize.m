function [returnBinImage] = filterIslandsBySize(inputBinImage, thresholdPixArea)
    
    returnBinImage = bwareaopen(inputBinImage, thresholdPixArea);