function [segmentedImage] = segmentByTexture(inputImage, Threshold, method)

    switch method
        case 'std'
            buff = stdfilt(inputImage);
        case 'entropy'
            buff = entropyfilt(inputImage);

    end
          
    buff = im2bw(double(buff/max(buff(:))), Threshold);
    segmentedImage = imfill(buff,'holes');       