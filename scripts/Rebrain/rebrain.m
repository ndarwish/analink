function [] = rebrain()

    % This function will let users interactively select a series of
    % elements ('islands') from different slices to generate an aligned stack. The
    % number of islands might be arbitrary.

    global RunningParameters
    global ApplicationHandles
    global InputImage InputFile
    
% Check if there's a Z-stack available:

while(ApplicationHandles.OpenedImages < 1)
    success = false;   
    while(~success)
        disp('No open stacks');
        success = load_images();
        if(success)
            break;
        end
    end

    if(InputImage(RunningParameters.SelectedDisplay).SliceNumber < 2)
        disp('Not a Z- stack. Closing it...');
        closeSelectedImage();
    else
        break;
    end    
end

fprintf(1,'Your stack contains %d slices. \n',...
    InputImage(RunningParameters.SelectedDisplay).SliceNumber);

% Open a second instance of the same image.................................

% First, we add an additional display window:

RunningParameters.displayWindows = [RunningParameters.displayWindows; display_window];
RunningParameters.displayWindowData = [RunningParameters.displayWindowData; guidata(RunningParameters.displayWindows(end))];
ApplicationHandles.ViewerWindow = [ApplicationHandles.ViewerWindow; ...
                                   RunningParameters.displayWindowData(end).figure1];
ApplicationHandles.ViewerAxes = [ApplicationHandles.ViewerAxes; ...
                                 RunningParameters.displayWindowData(end).image_axes];
RunningParameters.ViewerWindowPosition = [RunningParameters.ViewerWindowPosition; ...    
                                   RunningParameters.displayWindowData(end).ViewerWindowPosition];

refPos = RunningParameters.displayWindowData(end-1).ViewerWindowPosition;
                               
if (length(RunningParameters.displayWindows)>1)
    
% Place windows side by side:
    shiftWindowRight(RunningParameters.displayWindows(end),RunningParameters.ViewerWindowPosition(end-1,:),RunningParameters.WindowOffsetX+ refPos(3));
    
    shiftWindowRight(ApplicationHandles.SliderWindow,RunningParameters.SliderWindowPosition,RunningParameters.WindowOffsetX);
    shiftWindowRight(ApplicationHandles.ParameterWindow,RunningParameters.InfoWindowPosition,RunningParameters.WindowOffsetX);
else
    sendWindowUnder(ApplicationHandles.SliderWindow,...
        RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
        RunningParameters.WindowOffsetY);
    sendWindowRight(ApplicationHandles.ParameterWindow,...
        RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
        RunningParameters.WindowOffsetX);
end

% Move interacion windows back:

sendWindowUnder(ApplicationHandles.SliderWindow,...
    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
    RunningParameters.WindowOffsetY);
sendWindowRight(ApplicationHandles.ParameterWindow,...
    RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
    RunningParameters.WindowOffsetX);


% set(ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay),'Name','Auxiliary view: next slice');
% 
% % The Buffer2 has to be assigned to the next slice. Not done yet:
Buffer2 = imread([InputFile(RunningParameters.SelectedFile).Path,'\',...
                  InputFile(RunningParameters.SelectedFile).Name]);
imshow(Buffer2,[0, max(Buffer2(:))],...
       'Parent',ApplicationHandles.ViewerAxes(end));

% The value is always there, but it would be nice to have a short name for
% it:
sliceCount = InputImage(RunningParameters.SelectedDisplay).SliceNumber;

% Retrieve the slider window
sliderData = guidata(ApplicationHandles.SliderWindow);

% Retrieve the callbacks from the controls
menu_callback = get(sliderData.slider_menu, 'Callback');
slider_callback = get(sliderData.main_slider, 'Callback');
box_callback = get(sliderData.main_value_box, 'Callback');

set(sliderData.slider_menu,'Value',3);                    % 3 for 'Z slice'
set(sliderData.main_slider,'Value',1);
set(sliderData.main_value_box,'String','1');

menu_callback(sliderData.slider_menu,[]);
slider_callback(sliderData.main_slider,[]);
box_callback(sliderData.main_value_box,[]);


RunningParameters.SegmentationMethod = 'watershed';


    % This block was originally called by the segmentOptsWin function:
    InputImage(RunningParameters.SelectedDisplay).Buffer = imread([InputFile(RunningParameters.SelectedDisplay).Path,...
        '\',InputFile(RunningParameters.SelectedFile).Name],...
        'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);  


% Open a segmentation mode selection dialog and wait for confirmation:
segmentOptsWin = segmentOptions;
done_wdw = done();
        % Confirmation box waiting loop:
        while(RunningParameters.DoneButtonPressed == 0)        
            try
                % The pause is absolutely necessary, otherwise the user never 
                % gets access to the window.
                pause(0.1);                
            catch
                break;
            end
        end
        close(done_wdw);
        

for slice = 1:sliceCount
   
    % For the window on the left (lower Z):
    strL = ['slice: ', num2str(slice)];
    set(RunningParameters.displayWindows(end-1),'Name',strL,'NumberTitle','off');
    set(sliderData.main_slider,'Value',slice);
    set(sliderData.main_value_box,'String',num2str(slice));
    slider_callback(sliderData.main_slider,[]);
    box_callback(sliderData.main_value_box,[]);        
    
    % For the window on the right (higher Z):
    strR = ['slice: ', num2str(slice - 1)];
    set(RunningParameters.displayWindows(end),'Name',strR,'NumberTitle','off');
    if (slice > 1)
        imshow(formerBuffer,[0, max(formerBuffer(:))],...
       'Parent',ApplicationHandles.ViewerAxes(end));
    end
    formerBuffer = InputImage(RunningParameters.SelectedDisplay).Buffer;

    thresholdVal = InputImage(RunningParameters.SelectedDisplay).Threshold;
    thresholdPixArea = InputImage(RunningParameters.SelectedDisplay).AreaThreshold;
    segmentedImage = segmentInteractively(formerBuffer, thresholdVal, thresholdPixArea,...
                     RunningParameters.SegmentationMethod);

    [layerShell,vertexSubset] = findBorders(segmentedImage);
    [highCurvatureVertices, largeCurvatures, objectList] = curveFeatures(vertexSubset);


    % Build the mask structure for the current layer:
    objectCount = max(segmentedImage(:));
    [sX, sY] = size(segmentedImage);
    mask = zeros(sX, sY, objectCount);
    for objectInd = 1:objectCount    
        mask(:,:,objectInd) = (segmentedImage == objectInd);
    end
    
% currentMaskSubset = zeros(size(buffer),length(objectList));

    
    if(slice > 1) %We want interaction only when 2 different slices 
        % (here the first two) are available.                
        % Confirmation box before proceeding to the next slice:
        done_wdw = done();
        % Send the confirmation window to a useful location:
        sliderWinPos = get(ApplicationHandles.SliderWindow, 'Outerposition');
        sendWindowRight(ApplicationHandles.ConfirmWindow,sliderWinPos,...
        RunningParameters.WindowOffsetX);
  
        matchingPointLocations = [];
        
%         for oldInd = 1:length(pervlargeCurvatures);
%             PossibleMatchingPointLocations = [matchingPointLocations, ...
%                 find((largeCurvatures == pervlargeCurvatures(oldInd)))];
%                 
%         end
        
% PossibleMatchingPointLocations
        
        % Loop comparing all objects between adjacent slices:
        for currentObject = 1:objectCount
            for prevObject = 1:prevObjectCount;
                selectedObject = mask(:,:,currentObject) == currentObject;
                targetObject   = mask(:,:,prevObject) == prevObject;

% fprintf('slice %d, object %d vs slice %d, object %d \n', slice-1, prevObject, slice, currentObject);


% https://de.mathworks.com/help/images/examples/find-image-rotation-and-scale-using-automated-feature-matching.html
                findAffineTransformation(selectedObject, targetObject);

            end
        end


        hold(ApplicationHandles.ViewerAxes(end-1),'on');
        scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(end-1));
        hold(ApplicationHandles.ViewerAxes(end-1),'off');
        
        hold(ApplicationHandles.ViewerAxes(end),'on');        
        scatter(vertexSubset(:,2),vertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(end));        
        hold(ApplicationHandles.ViewerAxes(end),'off');

        % Confirmation box waiting loop:
        while(RunningParameters.DoneButtonPressed == 0)        
            try
                % The pause is absolutely necessary, otherwise the user never 
                % gets access to the window.
                pause(0.1);
                
            catch
                break;
            end
        end
        close(done_wdw);
    end 
%     previousVertexSubset = vertexSubset;
%     prevhighCurvatureVertices = highCurvatureVertices;
    pervlargeCurvatures = largeCurvatures;
%     prevobjectList = objectList;
    
    prevObjectCount = objectCount;
    prevMask = mask;
end
                               

% move through the stack:

%   Display the current slice on a window and the next one on another
%   Align:
%       Still don't know how exactly, BUT:
%       ***** Let the user click on the corresponding features *****
%       Obtain the Dx, Dy & Dtheta values for aligning the new with respect
%       to the old
%   Display the overview on a third window and ask for confirmation
%       If confirmed, apply the transformation to the new, put it on the
%       compartment for the old one, and load the next slice. Otherwise
%       let the user do it manually.