function [] = masker()

    global RunningParameters    
    global ApplicationHandles
    global InputImage
    
    nestedObjects = get(ApplicationHandles.SliderWindow,'Children')
    sliderMenu = nestedObjects(3);
    mainValueBox = nestedObjects(4);
        
    set(sliderMenu,'String','Z slice');
    sliderLocalHandler = guidata(ApplicationHandles.SliderWindow)

sMenu = get(sliderMenu)
sValueBox = get(mainValueBox)

% sMenu.Callback()
sValueBox.Callback

% hObject: sliderLocalHandler.main_slider
% eventdata: []
% handles: sliderLocalHandler

sliderLocalHandler.main_slider
sliderLocalHandler.slider_menu

all_sliders('slider_menu_Callback',sliderLocalHandler.main_slider,[],sliderLocalHandler.main_slider)



    for zInd = 1:InputImage(RunningParameters.SelectedFile).SliceNumber
        
zInd


        set(mainValueBox,'Value',zInd);
%         sliderLocalHandler.main_value_box.Callback
        all_sliders('main_value_box_Callback',sliderLocalHandler.main_value_box_Callback,[],sliderLocalHandler)

        pause(1);
    end
    
    disp('stack processed')