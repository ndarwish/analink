function [] = connMap()

    % This function will find connectivity maps between cells

    global RunningParameters
    global ApplicationHandles
    global InputImage
    global preview
    global indexedImage
    global InputFile
    global siz
    global strelSize
    
    distanceAround = 2; % Pixels around the given point
    mediumThreshold = 30; % Value below which we say there's no medium
    siz = 2;              % Kernel size for smoothening    
        
    % Check if there's a Z-stack available:

    while(ApplicationHandles.OpenedImages < 1)
        success = false;   
        while(~success)
            disp('No open stacks');
            success = load_images();
            if(success)
                break;
            end
        end
    end

    % Move interacion windows back:

    sendWindowUnder(ApplicationHandles.SliderWindow,...
        RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
        RunningParameters.WindowOffsetY);
    sendWindowRight(ApplicationHandles.ParameterWindow,...
        RunningParameters.ViewerWindowPosition(RunningParameters.SelectedDisplay,:),...
        RunningParameters.WindowOffsetX);

    % Shorter names: ......................................................
    
    frameCount = InputImage(RunningParameters.SelectedDisplay).FrameNumber;
    sliceCount = InputImage(RunningParameters.SelectedDisplay).SliceNumber;
    
    fprintf(1,'Your stack contains %d slices. \n',sliceCount);    
    
    
    % Prepare the robot ...................................................
    
    % Retrieve the slider window
    sliderData = guidata(ApplicationHandles.SliderWindow);

    % Retrieve the callbacks from the controls, to use them later in our robot
    menu_callback = get(sliderData.slider_menu, 'Callback');
    slider_callback = get(sliderData.main_slider, 'Callback');
    box_callback = get(sliderData.main_value_box, 'Callback');
    
    sliderSelectionForFrames   = 2;
    sliderSelectionForSlices   = 3;
    sliderSelectionForChannels = 4;
    % .....................................................................

    % ROBOT works here:
    % First time frame is always the safe choice:
    
    if(~(sliderData.slider_menu > 1))

        set(sliderData.slider_menu,'Value',sliderSelectionForFrames);
        set(sliderData.main_slider,'Value',1);
        set(sliderData.main_value_box,'String','1');

        menu_callback(sliderData.slider_menu,sliderSelectionForFrames);
        slider_callback(sliderData.main_slider,num2str(1));
        box_callback(sliderData.main_value_box,[]);
    end

    % .....................................................................    
        
    % Let the user tell which channel doew what:

    answ = (inputdlg('DAPI Channel index'));
    dapiCh = str2num(answ{1});
    answ = (inputdlg('Membrane Channel index'));
    membraneCh = str2num(answ{1});
    answ = (inputdlg('Medium Channel index'));
    mediumCh = str2num(answ{1});
    % The ratio of the distance between nuclei to the nucleus diameter, to be considered for a contact
%     answ = (inputdlg('Set the ratio tolerance (e.g. 2.3)'));
%     ratioTolerance = str2num(answ{1});
%     
    ratioTolerance = 2; % Seems to work always
    
    answ = (inputdlg('Set the imclose structuring element size (e.g. 6)'));
    strelSize = str2num(answ{1});
    
    RunningParameters.SegmentationMethod = 'entropy';    
    % .....................................................................    
    
    
    % Loop over time here
    
    fileName = InputFile(RunningParameters.SelectedFile).Name;
    fileId = fopen([InputFile(RunningParameters.SelectedFile).Path,'\',fileName([1:strfind(fileName,'.')-1]),'.csv'],'a+');
	for frame =1:frameCount
                
        % ROBOTS:
                        
        set(sliderData.slider_menu,'Value',sliderSelectionForFrames);
        menu_callback(sliderData.slider_menu,sliderSelectionForFrames);
                
        set(sliderData.main_slider,'Value',frame);
        slider_callback(sliderData.main_slider,[]);
        set(sliderData.main_value_box,'String',num2str(frame));
        box_callback(sliderData.main_value_box,[]); 
              
        % Change to the DAPI channel for a better segmentation:
        set(sliderData.slider_menu,'Value',sliderSelectionForChannels);
        menu_callback(sliderData.slider_menu,sliderSelectionForChannels);
                
        set(sliderData.main_slider,'Value',dapiCh);
        slider_callback(sliderData.main_slider,[]);
        set(sliderData.main_value_box,'String',num2str(dapiCh));
        box_callback(sliderData.main_value_box,[]);
                
        preview = InputImage(RunningParameters.SelectedDisplay).Buffer;
        dapiTopView = preview;
        entropySlidersWdw = entropySegmentSliders();

        % Prepare the robot ...................................................
    
        % Retrieve the slider window
        % Retrieve the callbacks from the controls, to use them later in our robot

        entropySlidersData = guidata(entropySlidersWdw);
        thresholdSlider_callback = get(entropySlidersData.thresholdSlider, 'Callback');
        areaSlider_callback      = get(entropySlidersData.areaSlider, 'Callback');        
        % .....................................................................        
               
        % Open a segmentation mode selection dialog and wait for confirmation:
        % segmentOptsWin = segmentOptions;
        done_wdw = done();
                
        % Confirmation box waiting loop:
        while(RunningParameters.DoneButtonPressed == 0)
            try
                % The pause is absolutely necessary, otherwise the user never 
                % gets access to the window.        
                pause(0.1);
            catch
                break;
            end    
        end
        close(done_wdw);

        % The user chooses the typical link length by clicking on the image

        
        msgbox('Please click on two connected cells to find set an average distance','Action needed');
        
        
        targetWin = ApplicationHandles.ViewerWindow(RunningParameters.SelectedDisplay);        
        [y1,x1] = myginput(1,'crosshair',targetWin);
        text(y1,x1,'+','Color','y');
        [y2,x2] = myginput(1,'crosshair',targetWin );
        text(y1,x1,'+','Color','y');
        R12 = [x2-x1,y2-y1];
        refDiameter = sqrt(R12(1)^2+R12(2)^2);
        
        % .................................................................
        
        % retrieve the inputs to apply them later to the other slices:
        segmentationThreshold = get(entropySlidersData.thresholdSlider,'Value');
        areaThreshold         = get(entropySlidersData.areaSlider,'Value');

% sliceCount = 5;
        [sx, sy] = size(preview);
        buffer = zeros(sx, sy, sliceCount);
               
        for currentSlice = 1:sliceCount
                    
            % Move through the Z stack:
            
            set(sliderData.slider_menu,'Value',sliderSelectionForSlices);
            menu_callback(sliderData.slider_menu,sliderSelectionForSlices);
            
            set(sliderData.main_slider,'Value',currentSlice);
            slider_callback(sliderData.main_slider,[]);
            set(sliderData.main_value_box,'String',num2str(currentSlice));
            box_callback(sliderData.main_value_box,[]); 
                        
            % Change to the DAPI channel for a better segmentation:
            set(sliderData.slider_menu,'Value',sliderSelectionForChannels);
            menu_callback(sliderData.slider_menu,sliderSelectionForChannels);
                
            set(sliderData.main_slider,'Value',dapiCh);
            slider_callback(sliderData.main_slider,[]);
            set(sliderData.main_value_box,'String',num2str(dapiCh));
            box_callback(sliderData.main_value_box,[]);                        
            
            preview = imread([InputFile(RunningParameters.SelectedDisplay).Path,...
                '\',InputFile(RunningParameters.SelectedFile).Name],...
                'Index',InputImage(RunningParameters.SelectedDisplay).CurrentIndex);  
            origView = preview;
            % .............................................................
            
            imshow(origView,[],'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
            if(currentSlice > 1)
                if(~isempty(oldVertexSubset))
                hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');
                scatter(oldVertexSubset(:,2),oldVertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'b');
                hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');
                pause(0.01);
                end
            end
            
            % Confirmation box waiting loop:
            % Enable a confirmation window in case the parameters have to be
            % changed
%             done_wdw = done();                    
%             while(RunningParameters.DoneButtonPressed == 0)
%                 try
%                     % The pause is absolutely necessary, otherwise the user never 
%                     % gets access to the window.        
%                     pause(0.1);                
%                 catch
%                     break;
%                 end    
%                         
                try
                    % Run the callback for entropySegmentSliders() at every slice
                    thresholdSlider_callback(entropySlidersData.thresholdSlider,[]);
                    areaSlider_callback(entropySlidersData.areaSlider,[]);
                    [ segmentedImage, oldVertexSubset] = commitEntropySegmentation( );
                    buffer(:,:, currentSlice) = segmentedImage;
                    if(~isempty(oldVertexSubset))
                        hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'on');
                        scatter(oldVertexSubset(:,2),oldVertexSubset(:,1),'.','Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'r');
                        hold(ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay),'off');
                    end                                              
                catch                
                    fprintf('Wrong limits.\n');         
                end
%             end
%             close(done_wdw);

        
        end  % End slice loop
% moved into the commitEntropySegmentation( ) function:       
% se6 = strel('cube',6); 
% erodedBuffer = imclose(buffer, se6);

erodedBuffer = buffer;


% se6 = strel('cube',6);
% se3 = strel('cube',3);
% erodedBuffer = imerode(imclose(buffer, se6),se6);
if(sliceCount > 1)
        [x, y, z] = ind2sub(size(erodedBuffer), find(erodedBuffer));
end   
        
        segmentedImage = bwconncomp(erodedBuffer);
                
        indexedImage = segmentedImage;
        cellCount = indexedImage.NumObjects;
        fprintf('%d cells found.\n',cellCount);
                
        centStats = regionprops(indexedImage,'Centroid');
        if(sliceCount >1)
            centers = zeros(cellCount,3);
        else
            centers = zeros(cellCount,2);
        end
        
        % .................................................................        
%         % Find the diameter of the largest nucleus to use it as a scaling
%         % factor (done in 2D for speed)
% %         if (currentSlice == 1)
%             maxSz = 0;
%             for cell = 1:cellCount
%                 [szX, szY, ~] = size(indexedImage.PixelIdxList{cell});
%                 sz = szX*szY;
%                 if (sz > maxSz)
%                     maxSz = sz;
%                 end
%             end
% %             refDiameter = 2*sqrt(maxSz/pi);
% %         end        
        % .................................................................
        % Locate the centers:
        for cell = 1:cellCount
            centers(cell,:) = centStats(cell).Centroid;
        end

        % .................................................................
        % Build the 3D blocks to analyze the profiles
        
        membraneBuff = zeros(sx, sy, sliceCount);
        mediumBuff   = zeros(sx, sy, sliceCount);
        
        for currentSlice = 1:sliceCount
            % Change to the medium channel for testing spaces:

            set(sliderData.slider_menu,'Value',sliderSelectionForChannels);
            menu_callback(sliderData.slider_menu,sliderSelectionForChannels);

            set(sliderData.main_slider,'Value',mediumCh);
            slider_callback(sliderData.main_slider,[]);
            set(sliderData.main_value_box,'String',num2str(mediumCh));
            box_callback(sliderData.main_value_box,[]);
            preview = InputImage(RunningParameters.SelectedDisplay).Buffer;
            mediumBuff(:,:,currentSlice) = preview;

            % Change to the membrane channel for testing contacts:

            set(sliderData.slider_menu,'Value',sliderSelectionForChannels);
            menu_callback(sliderData.slider_menu,sliderSelectionForChannels);

            set(sliderData.main_slider,'Value',membraneCh);
            slider_callback(sliderData.main_slider,[]);
            set(sliderData.main_value_box,'String',num2str(membraneCh));
            box_callback(sliderData.main_value_box,[]);
            preview = InputImage(RunningParameters.SelectedDisplay).Buffer;
            membraneBuff(:,:,currentSlice) = preview;
        end

        
wX = double(InputImage(RunningParameters.SelectedFile).PixelWidth);    
wY = double(InputImage(RunningParameters.SelectedFile).PixelHeight);
wZ = double(InputImage(RunningParameters.SelectedFile).VoxelDepth);
        
mxbuff = max(membraneBuff(:))/4;
scalingZ = wZ/wX;
if(sliceCount > 1)
[xb, yb, zb] = ind2sub(size(membraneBuff), find(membraneBuff > mxbuff));
figure(99)
    hold on;
    plot3(x,y,z*scalingZ,'.b');
%     plot3(xb,yb,zb*scalingZ,'.g');
    axis equal      
else
          
end

        
        hold(ApplicationHandles.ViewerAxes(end),'off');
            imshow(preview,[],'Parent',ApplicationHandles.ViewerAxes(end));
        hold(ApplicationHandles.ViewerAxes(end),'on');

        % .................................................................
        % Pairs are identified here:

        if(cellCount > 1)            
            pairs = ones(cellCount);
            distanceRatios = NaN * ones(cellCount);
            d12 = NaN * ones(cellCount);
 
            % mark the far connections
            disp('first phase: distant cells');
            for cell1 =1:cellCount
                % Cells are not self-connected
                pairs(cell1,cell1) = 0;
                for cell2 = cell1+1:cellCount

                    % Exclude distant cells:
                    
                    v12 = centers(cell2,:) - centers(cell1,:);

                    if(sliceCount > 1)
                        d12(cell1, cell2) = sqrt(v12(1)^2+v12(2)^2+v12(3)^2);
                    else
                        d12(cell1, cell2) = sqrt(v12(1)^2+v12(2)^2);
                    end
                    d12(cell2, cell1) = d12(cell1, cell2);
                    
                    distanceRatios(cell1, cell2) = d12(cell1, cell2) / refDiameter;
                    distanceRatios(cell2, cell1) = distanceRatios(cell1, cell2);

                    
                    % Here the points that are too close have to be
                    % merged...........................................................................................
                    
                    % ****************************
                    

                end
            end

            disp('second phase: testing connections');
            
% fInd =0;
            for cell1 =1:cellCount
                for cell2 = cell1+1:cellCount

                    % If the cells are too far from each other or if the
                    % distance is smoler than the radius of a nucleus
                    % discard the connection:
                    if(distanceRatios(cell1, cell2) > ratioTolerance ||...
                       distanceRatios(cell1, cell2) < 0.5)
                        pairs(cell1, cell2) = 0;
                        pairs(cell2, cell1) = 0;
                    else
                        pairs(cell1, cell2) = 1;
                        pairs(cell2, cell1) = pairs(cell1, cell2);                        
                        
                        % Take a pair of points and consider all the pixels around them
                        
                        v1 = centers(cell1,:);
                        v2 = centers(cell2,:);

                        
                        % X and Y coordinates are swapped:
bf = v1(1);
v1(1) = v1(2);
v1(2) = bf;

bf = v2(1);
v2(1) = v2(2);
v2(2) = bf;

                        
                    
if(sliceCount > 1)
figure(99)
    hold on;
    scatter3(centers(:,2),centers(:,1),centers(:,3)*scalingZ,'.r');
figure(100)
    hold on;
    scatter3(centers(:,2),centers(:,1),centers(:,3)*scalingZ,'ob');
    axis equal   
else
figure(99)
    hold on;
    scatter(centers(:,2),centers(:,1),'.r');
figure(100)
    hold on;
    scatter(centers(:,2),centers(:,1),'.r');    
end

    
                        if v1(1)>v2(1)
                            v1(1) = min(sx,v1(1));
                            v2(1) = max(1,v2(1));
                            
                            vX1 = v2(1);
                            vX2 = v1(1);                            
                        else
                            v2(1) = min(sx,v2(1));
                            v1(1) = max(1,v1(1));
                            
                            vX1 = v1(1);
                            vX2 = v2(1);                                                        
                        end
                        
                        if v1(2)>v2(2)
                            v1(2) = min(sy,v1(2));
                            v2(2) = max(1,v2(2));
                            
                            vY1 = v2(2);
                            vY2 = v1(2);                            
                        else
                            v2(2) = min(sy,v2(2));
                            v1(2) = max(1,v1(2));
                            
                            vY1 = v1(2);
                            vY2 = v2(2);                            
                        end
if(sliceCount>1)
                        if v1(3)>v2(3)
                            v1(3) = min(sliceCount,v1(3));
                            v2(3) = max(1,v2(3));

                            vZ1 = v2(3);
                            vZ2 = v1(3);
                        else
                            v2(3) = min(sliceCount,v2(3));
                            v1(3) = max(1,v1(3));

                            vZ1= v1(3);
                            vZ2= v2(3);
                        end
end
                        deltaX = vX2 - vX1;
                        deltaY = vY2 - vY1;
if(sliceCount>1)                        
                        deltaZ = vZ2 - vZ1;
%                         delta = ceil(max([deltaX, deltaY, deltaZ]));

else
%                         delta = ceil(max([deltaX, deltaY]));
end
delta = ceil(d12(cell1,cell2));
                        

                        points = 0:1/delta:1;

                        vX = ceil(vX1+ deltaX* points);
                        vY = ceil(vY1+ deltaY* points);
if(sliceCount>1)                        
                        vZ = ceil(vZ1+ deltaZ* points);
end                        
                        % Create the profiles in 3D averaging around ......
                        
                        mediumProfile   = zeros(1,delta);
                        membraneProfile = zeros(1,delta);

                        for pathInd = 1:delta

                            [xMinus, xPlus, yMinus, yPlus] = surrounding(vX(pathInd),vY(pathInd), distanceAround);

                            xMinus = -1* xMinus;
                            yMinus = -1* yMinus;

                            avgPointCount = (xPlus - xMinus) * (yPlus - yMinus);
                            
                            mediumProfile(pathInd)   = 0;
                            membraneProfile(pathInd) = 0;
                            for xInd = xMinus:xPlus
                                for yInd = yMinus:yPlus
                                    if(sliceCount>1)
                                        mediumProfile(pathInd)   = mediumProfile(pathInd)   + mediumBuff(vX(pathInd)+xInd,  vY(pathInd)+yInd,vZ(pathInd));
                                        membraneProfile(pathInd) = membraneProfile(pathInd) + membraneBuff(vX(pathInd)+xInd,vY(pathInd)+yInd,vZ(pathInd));
                                    else
                                        mediumProfile(pathInd)   = mediumProfile(pathInd)   + mediumBuff(vX(pathInd)+xInd,  vY(pathInd)+yInd);
                                        membraneProfile(pathInd) = membraneProfile(pathInd) + membraneBuff(vX(pathInd)+xInd,vY(pathInd)+yInd);                                        
                                    end
                                end
                            end
                        end
                        

                        mediumProfile   = double(mediumProfile) / avgPointCount;    
                        membraneProfile = double(membraneProfile) / avgPointCount;                                                

                        
                        mediumProfile = mediumProfile - min(mediumProfile);                        
                        membraneProfile = membraneProfile - min(membraneProfile);

                        % .................................................
                                                
line([v2(2),v1(2)],[v2(1),v1(1)],...
'Parent',ApplicationHandles.ViewerAxes(end),'Color','w');

                        xVect = 1:length(membraneProfile);
                        
                        if(max(mediumProfile) > mediumThreshold)
                            pairs(cell1, cell2) = 0;
                            pairs(cell2, cell1) = 0;                            
                        else

                            try
                                % The model used: a1*exp(-((x-b1)/c1)^2)
                                % The amplitude should not be higher that the profile's
                                % maximum: a1 <= max(profile)*1.2
                                % The peak position should about the middle of the segment:
                                % abs(b1 - length(profile)) < length(profile)/2

                                [curve, gof] = fit(xVect',membraneProfile','gauss1');

                                if (pairs(cell1, cell2) ~= 0 &&...
                                    (max(mediumProfile)< 0.5*max(membraneProfile)) &&...
                                    abs(curve.a1) <= max(membraneProfile)*2 && ...
                                    length(membraneProfile)/4 <= abs(curve.b1) &&...
                                    abs(curve.b1) <= length(membraneProfile)*(3/4) &&...
                                    gof.adjrsquare < 1 &&...
                                    gof.rmse < max(curve(xVect))/2)
fInd = fInd+1;
figure(fInd)
    hold on;
	plot(mediumProfile, 'b');
	plot(membraneProfile,'g');
    hold off
                                    % Discard additional peaks
                                    
                                    

                                    difference = abs(membraneProfile'-curve(xVect));

% figure(fInd)
%     hold on;
% 	plot(difference, 'k');
%     hold off

%                                         if(trapz(difference) > 0.8* trapz(curve(xVect))||...
%                                            max(difference) > max(curve(xVect))/3)
                                        if(trapz(difference) > 1.0* trapz(curve(xVect)))

                                            pairs(cell1, cell2) = 0;
                                            pairs(cell2, cell1) = 0;
                                        else
                                            pairs(cell1,cell2) = 1;
                                            pairs(cell2,cell1) = pairs(cell1,cell2);
% figure(fInd)
%     hold on;
%     plot(curve(xVect),'r');
%     hold off


if(sliceCount > 1)
figure(99)
    hold on
    line([v2(1),v1(1)],[v2(2),v1(2)],[v2(3)*scalingZ,v1(3)*scalingZ],'Color','r');
    pause(0.001);
    hold off
figure(100)
    hold on
    line([v2(1),v1(1)],[v2(2),v1(2)],[v2(3)*scalingZ,v1(3)*scalingZ],'Color','r');
    pause(0.001);
    hold off
else
figure(99)
    hold on
    line([v2(2),v1(2)],[v2(1),v1(1)],'Color','r');
    pause(0.001);
    hold off
figure(100)
    hold on
    line([v2(2),v1(2)],[v2(1),v1(1)],'Color','r');
    pause(0.001);
    hold off    
end


                                        
    hold(ApplicationHandles.ViewerAxes(end),'on');
    scatter(centers(:,1),centers(:,2),...
            'Parent',ApplicationHandles.ViewerAxes(end),'.r');    
    line([v2(2),v1(2)],[v2(1),v1(1)],...
        'Parent',ApplicationHandles.ViewerAxes(end),'Color','y');
    pause(0.001);
    hold(ApplicationHandles.ViewerAxes(end),'off');                                                                             
                                        end
                                end                                        
                            catch
                                pairs(cell1, cell2) = 0;
                                pairs(cell2, cell1) = 0;
                            end
                        end
                    end
                end    
            end
        end
                
        % Change to the medium channel for testing spaces:
        valueForChannels = 4;
        set(sliderData.slider_menu,'Value',valueForChannels);
        menu_callback(sliderData.slider_menu,valueForChannels);
                
        set(sliderData.main_slider,'Value',membraneCh);
        slider_callback(sliderData.main_slider,[]);
        set(sliderData.main_value_box,'String',num2str(membraneCh));
        box_callback(sliderData.main_value_box,[]);
        
        preview = InputImage(RunningParameters.SelectedDisplay).Buffer;        

        imshow(cat(3, dapiTopView, membraneBuff(:,:,1), mediumBuff(:,:,1)),...
                            'Parent',ApplicationHandles.ViewerAxes(end));              
        
        wX = double(InputImage(RunningParameters.SelectedFile).PixelWidth);    
        wY = double(InputImage(RunningParameters.SelectedFile).PixelHeight);
        wZ = double(InputImage(RunningParameters.SelectedFile).VoxelDepth);
                        
        if(sliceCount > 1)
            fprintf(1,     'Cell #, \t X, \t Y, \t Z, frame,\t connection #,\t connections   \n');
            fprintf(fileId,'Cell #, \t X, \t Y, \t Z, frame,\t connection #,\t connections   \n');
        else
            fprintf(1,     'Cell #, \t X, \t Y, frame,\t connection #,\t connections   \n');
            fprintf(fileId,'Cell #, \t X, \t Y, frame,\t connection #,\t connections   \n');
        end
        
        for cell1 =1:cellCount
            if(exist('v1','var'))
                
                if(sliceCount > 1)
                    
                    x = wX*v1(1);
                    y = wY*v1(2);
                    z = wZ*v1(3);
                    
                    fprintf(1,     '%d, \t %d, \t %d, \t %d, \t %d,',cell1, x, y, z, frame);
                    fprintf(fileId,'%d, \t %d, \t %d, \t %d, \t %d,',cell1, x, y, z, frame);                    
                else
                    x = wX*v1(1);
                    y = wY*v1(2);
                    
                    fprintf(1,     '%d, \t %d, \t %d, \t %d,',cell1, x, y, frame);
                    fprintf(fileId,'%d, \t %d, \t %d, \t %d,',cell1, x, y, frame);
                end
            end
            positives = [];
                
            for cell2 = 1:cellCount                
                if(pairs(cell1,cell2) == 1)
                        
                        positives = [positives , cell2];
                              
                       
                        v1 = centers(cell1,:);
                        v2 = centers(cell2,:);
                        
                        hold(ApplicationHandles.ViewerAxes(end),'on');
                        line([v2(1),v1(1)],[v2(2),v1(2)],...
                            'Parent',ApplicationHandles.ViewerAxes(end),'Color','y');
                        pause(0.001);
                        hold(ApplicationHandles.ViewerAxes(end),'off');                        
                end                    
            end

            if ~isempty(positives)
            	fprintf(1,     '\t%d,',length(positives));
                fprintf(fileId,'\t%d,',length(positives));
                for connection = 1:length(positives)
                        fprintf(1,     '\t%d,',positives(connection));
                        fprintf(fileId,'\t%d,',positives(connection));                
                end
            else
                fprintf(1,     '\t No connections');
                fprintf(fileId,'\t No connections');
            end                        
            fprintf(1,     '\n');
            fprintf(fileId,'\n');          
        end
        
        snapshot = getframe(ApplicationHandles.ViewerAxes(end));
        imwrite(snapshot.cdata, ...
            [InputFile(RunningParameters.SelectedFile).Path,'\',fileName([1:strfind(fileName,'.')-1]),'_time_frame',num2str(frame),'_snapshot.tif']...
            );
    end
    
    fprintf(1,'\n ');
    fprintf(fileId,'\n ');
        
    fprintf('end\n');        
    fclose(fileId);    




end

