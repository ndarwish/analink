function [ profileArround ] = buildProfileArround( view, p1, p2, distanceAround)

[ x1Minus, x1Plus, y1Minus, y1Plus ] = surrounding(p1(1), p1(2), distanceAround);
[ x2Minus, x2Plus, y2Minus, y2Plus ] = surrounding(p1(1), p1(2), distanceAround);

xMinus = max(x1Minus, x2Minus);
yMinus = max(y1Minus, y2Minus);
xPlus  = min(x1Plus, x2Plus);
yPlus  = min(y1Plus, y2Plus);

LX = 1+xPlus+xMinus;
LY = 1+yPlus+yMinus;

profileArround = [];

for xInd = 1:LX
    xIncrement = xInd - xMinus;
    for yInd = 1:LY
        yIncrement = yInd - yMinus;
        if(xInd == 1 && yInd == 1)
            profileArround = improfile(view,...
                            [p2(1)+xIncrement, p1(1)+xIncrement],...
                            [p2(2)+yIncrement, p1(2)+yIncrement ]);
        else
            profileArround = profileArround +...
                            improfile(view,...
                            [p2(1)+xIncrement, p1(1)+xIncrement],...
                            [p2(2)+yIncrement, p1(2)+yIncrement ]);            
        end
    end      
end
                        
profileArround = profileArround /(LX * LY);

end

