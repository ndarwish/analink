function [] = skeletonizer3D(objectInd)


    
    global InputImage
    global InputFile
    global RunningParameters    
    
    
    % These two lines set the focus to the current image:
    RunningParameters.SelectedFile    = objectInd;
    RunningParameters.SelectedDisplay = objectInd;    
        
    
    try
        fprintf('Please note that this function will only work if the file \nsizes do not exceed the 143Gb limit. Otherwise you will \nget the array size limit error\n');
        reader = bfGetReader([char(InputFile(RunningParameters.SelectedFile).Path),char(InputFile(RunningParameters.SelectedFile).Name)]);
        load_to_RAM(reader);    
        skel = Skeleton3D(InputImage(RunningParameters.SelectedFile).Hyperstack);
    catch
        fprintf('And it did not work, eh!\n');
        skel = skeletonizeLargeFiles3D(objectInd);
    end
    
    figure(66);    
    col=[.7 .7 .8];
    hiso = patch(isosurface(testvol,0),'FaceColor',col,'EdgeColor','none');
    hiso2 = patch(isocaps(testvol,0),'FaceColor',col,'EdgeColor','none');
    axis equal;axis off;
    lighting phong;
    isonormals(testvol,hiso);
    alpha(0.5);
    set(gca,'DataAspectRatio',[1 1 1])
    camlight;
    hold on;
    w=size(skel,1);
    l=size(skel,2);
    h=size(skel,3);
    [x,y,z]=ind2sub([w,l,h],find(skel(:)));
    plot3(y,x,z,'square','Markersize',4,'MarkerFaceColor','r','Color','r');            
    set(gcf,'Color','white');
    view(140,80);
    
    
function [skel] = skeletonizeLargeFiles3D(objectInd)
    
    global InputImage
    global InputFile
    global RunningParameters   
    
    order = InputImage(objectInd).StackOrder;

    indZ =  strfind(order,'Z');
    indT =  strfind(order,'T');
    indC =  strfind(order,'C');
    
    frameNumber = InputImage(RunningParameters.SelectedFile).FrameNumber;
    channelNumber = InputImage(RunningParameters.SelectedFile).ChannelNumber;
    sliceNumber = InputImage(RunningParameters.SelectedFile).SliceNumber;

    
    for frame = 1:frameNumber
        cIndex(indT-2) = frame;
        for channel = 1: channelNumber
            cIndex(indC-2) = channel;
            pages = zeros(10,1);
            
            startSlice = 1;
            blockHeight = 10; % Analyze blocks of 10 slices;

            

            while(1)
                block = zeros(InputImage(RunningParameters.SelectedFile).SizeY, ...
                              InputImage(RunningParameters.SelectedFile).SizeX, ...
                              blockHeight); % some memory allocation
                          
                for slice = startSlice:min(sliceNumber, startSlice+blockHeight)
                    cIndex(indZ-2) = slice;
                    currentSlice = slice-startSlice+1;
                    pages(currentSlice) = InputImage(objectInd).IndexMatrix(cIndex(1),cIndex(2),cIndex(3));

                    block(:,:,currentSlice) = imread([InputFile(objectInd).Path,'\',InputFile(objectInd).Name],'Index',pages(currentSlice));                  
                end

                % Too optimistic approach:.................................
%                 skel(:,:,startSlice:min(sliceNumber, startSlice+blockHeight-1)) = ...
%                     Skeleton3D(block(:,:,1:10));
                % .........................................................
                
                



% Load sections of 20 slices: (slices are 70nm away from each other)
% The upper and lowermost 






                skel(:,:,startSlice:min(sliceNumber, startSlice+blockHeight-1)) = ...
                     bwdist(~block(:,:,1:10));
                figure(88)
                	imshow(skel(:,:,startSlice));
                                    
                if(startSlice < sliceNumber)
                    startSlice = currentSlice + 1;
                else
                    break;
                end
            end
            
            
        end
        
    end