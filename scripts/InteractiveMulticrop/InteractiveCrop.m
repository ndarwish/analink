function [ ] = InteractiveCrop( )

%InteractiveCrop allow the user to crop areas interactively, as it name says.
% With two mouse clicks the boundary corners are selected, and the magic
% happens. As many times as needed.

multicrop();
fprintf('multicrop finished\n');

end

