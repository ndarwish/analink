function multicrop()

global HYPERSTACK page_no slice_no img_path img_name img_data show_additional_hdl current_cell 
global shot % required for naming the snapshots
global ortho_slices_hdl
global tmpXYproj tmpXZproj tmpYZproj  
global XYproj XZproj YZproj

	msg = msgbox('Enclose the ROIs');
    drawnow;
    waitfor(msg);

    % Manage the confirmation box

    done_wdw = done();            %This function has beed defined elsewhere
    movegui(done_wdw,'west');
    
    done_hdl  = guidata(done_wdw);
    doneData = get(done_hdl.figure1, 'UserData');
        
    Xm = [];
    XM = [];
    Ym = [];
    YM = [];

    display_collapsed('Z');

    while(~doneData.stop)
    	doneData = get(done_hdl.figure1, 'UserData');
        try
        	[y1,x1] = myginput(1,'crosshair');
            [y2,x2] = myginput(1,'crosshair');
                
            % The enclosed rectangle:

            Xm = [Xm, fix(min(x1,x2))];
            XM = [XM, fix(max(x1,x2))];
            Ym = [Ym, fix(min(y1,y2))];
            YM = [YM, fix(max(y1,y2))];
            
            text((Ym(end)+YM(end))/2, (Xm(end)+XM(end))/2,...
                  num2str(length(Xm)),'Color',[1,1,0],'FontSize',12);
        catch % otherwise stopping the loop generates an error
                break;
        end            
    end        


% Generate the snapshots and save them as *jpg files.......................

sight = getframe(show_additional_hdl.axes);
shot = frame2im(sight);
% imwrite(shot,[img_path,'\',img_name(1:strfind(img_name,'.')-1),' - labeled.jpg']);
% [img_name(1:length(img_name)-strfind(fliplr(img_name),'.')),' - labeled.jpg'];
imwrite(shot,[img_name(1:length(img_name)-strfind(fliplr(img_name),'.')),' - labeled.jpg']);

% The output path name coincides with the name of the analyzed image
% output_path = [img_path,img_name(1:strfind(img_name,'.')-1)];
output_path = img_name(1:strfind(img_name,'.')-1);

mkdir(output_path);

% Before these 2 lines were executed in the local_maxima2 function:
ortho_slices_win = ortho_slices();
movegui(ortho_slices_win,'east');
ortho_slices_hdl  = guidata(ortho_slices_win);
continueData = get(ortho_slices_hdl.figure1, 'UserData');

for shot = 1:length(Xm)
        
    current_cell = shot;
    
    output_name = [num2str(shot),'.tif'];             % '.ome.tif not used'
    img_data.Width  = XM(shot) - Xm(shot);
    img_data.Height = YM(shot) - Ym(shot);
    % .....................................................................
%     write_metadata( output_path, output_name, img_data);

substack = zeros(img_data.Width+1,img_data.Height+1,slice_no);
    % .....................................................................
    for page = 1:slice_no
        imageIndex = page - 1;
        img = uint16(HYPERSTACK(Xm(shot):XM(shot),Ym(shot):YM(shot),page));
%          write_LOCI( output_path, output_name, img, imageIndex);
%          imwrite(img,[output_path,'\',output_name],'WriteMode','append', 'Compression','none');
% substack(Xm(shot):XM(shot),Ym(shot):YM(shot),page) = img;
% size(img)
% size(substack)
substack(:,:,page) = img;
    end
      
    
    XYproj = XYprojection_MIP(substack);
    XZproj = XZprojection_MIP(substack);
    YZproj = YZprojection_MIP(substack);    
    
%     % these values guarantee that the threshold always works on the
%     % original images:
%     tmpXYproj = XYproj;
%     tmpXZproj = XZproj;
%     tmpYZproj = YZproj;    
    
    imshow(XYproj, [],'Parent',ortho_slices_hdl.axesXY, 'InitialMagnification', 'fit');
    imshow(XZproj,[],'Parent',ortho_slices_hdl.axesXZ, 'InitialMagnification', 'fit');
    imshow(YZproj', [],'Parent',ortho_slices_hdl.axesYZ, 'InitialMagnification', 'fit');
    
    while (continueData.stop == 0)
        pause(0.1);
        continueData = get(ortho_slices_hdl.figure1, 'UserData');
    end

    local_maxima2( substack );
    clear('substack');
    
    close(ortho_slices_win);
    ortho_slices_win = ortho_slices();
    movegui(ortho_slices_win,'east');
    ortho_slices_hdl  = guidata(ortho_slices_win);
    continueData = get(ortho_slices_hdl.figure1, 'UserData');
    
end