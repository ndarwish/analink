
function [] = clusterify()

    global thresholdDistance

    clear nameList 
    clear commonPath
    
    [nameList, commonPath] = load_data_files('*.txt');
    textBuffer = inputdlg('Desired threshold distance');
    thresholdDistance = str2double(textBuffer{:});
    
    overallGroupList = struct(...
        'singlets',   0,...
        'doublets',   0,...
        'triplets',   0,...
        'multiplets', 0);   
    
    if (iscell(nameList))    
        for fileIndex = 1:numel(nameList)
            assignedPoints = clusterGrouping(nameList{fileIndex}, commonPath);

            % Plots:
            figure(fileIndex)
            hold on
            gscatter(assignedPoints(end,1),assignedPoints(end,2),assignedPoints(end,3));
            % Summaries:        
            numberOfGroups = assignedPoints(end,3);    
            groupList = zeros(numberOfGroups, 2);

            testIndex = 1;
            count = 0;        
            for assignedPointIndex = 1:length(assignedPoints)
                if(assignedPoints(assignedPointIndex,3) == testIndex)
                    count = count+1;
                    groupList(testIndex,1) = testIndex;
                    groupList(testIndex,2) = count;
                elseif(assignedPoints(assignedPointIndex,3) == testIndex+1)
                    testIndex = testIndex + 1;
                    count = 1;
                    groupList(testIndex,1) = testIndex;
                    groupList(testIndex,2) = count;
                end
            end
            
            figure(fileIndex)
            hold off
            gscatter(assignedPoints(:,1),assignedPoints(:,2),assignedPoints(:,3));
            
            overallGroupList.singlets   = overallGroupList.singlets   + numel(find(groupList(:,2)== 1));
            overallGroupList.doublets   = overallGroupList.doublets   + numel(find(groupList(:,2)== 2));
            overallGroupList.triplets   = overallGroupList.triplets   + numel(find(groupList(:,2)== 3));
            overallGroupList.multiplets = overallGroupList.multiplets + numel(find(groupList(:,2) > 3));
            
            % Outputs:
            csvwrite([commonPath,'\results_using_distance_of_',num2str(thresholdDistance),'-',nameList{fileIndex}],assignedPoints);
            csvwrite([commonPath,'\summary_for_distance_of_',num2str(thresholdDistance),'-',nameList{fileIndex}],groupList);

            % Save the figures to files
            buffer = nameList{fileIndex};
            figurename = [commonPath,'\',buffer(1:max(strfind(buffer,'.txt'))-1)];                        
            saveas(figure(fileIndex), figurename, 'fig');            
        end
        
        outputFileHandler = fopen([commonPath,'\OVERALL_RESULTS(threshold of ',num2str(thresholdDistance),').txt'],'w');
        fprintf(outputFileHandler,...
            '# singlets: %d, \n# doublets: %d, \n# triplets: %d, \n# multiplets: %d\n',...
            overallGroupList.singlets, overallGroupList.doublets, ...
            overallGroupList.triplets, overallGroupList.multiplets);        
        fprintf(outputFileHandler, '%d files processed in total.\n', numel(nameList));
        fclose(outputFileHandler);
        
    elseif(~isempty(nameList))

        assignedPoints = clusterGrouping(nameList, commonPath);

        % Plots:
        figure(1)
        hold on
        gscatter(assignedPoints(end,1),assignedPoints(end,2),assignedPoints(end,3));                
        
        % Summaries:        
        numberOfGroups = assignedPoints(end,3);    
        groupList = zeros(numberOfGroups, 2);            

        testIndex = 1;
        count = 0;
                        
        for assignedPointIndex = 1:length(assignedPoints)
            if(assignedPoints(assignedPointIndex,3) == testIndex)
                count = count+1;
                groupList(testIndex,1) = testIndex;
                groupList(testIndex,2) = count;
            elseif(assignedPoints(assignedPointIndex,3) == testIndex+1)                
                testIndex = testIndex + 1;
                count = 1;
                groupList(testIndex,1) = testIndex;
                groupList(testIndex,2) = count;
            end            
        end

        figure(1)
        hold off
        gscatter(assignedPoints(:,1),assignedPoints(:,2),assignedPoints(:,3));
        
        overallGroupList.singlets   = overallGroupList.singlets   + numel(find(groupList(:,2)== 1));
        overallGroupList.doublets   = overallGroupList.doublets   + numel(find(groupList(:,2)== 2));
        overallGroupList.triplets   = overallGroupList.triplets   + numel(find(groupList(:,2)== 3));
        overallGroupList.multiplets = overallGroupList.multiplets + numel(find(groupList(:,2) > 3));        
                
        % Outputs:
        csvwrite([commonPath,'\results-using_distance_of',num2str(thresholdDistance),'-',nameList],assignedPoints);
        csvwrite([commonPath,'\summary-using_distance_of',num2str(thresholdDistance),'-',nameList],groupList);
        
        % Save the figures to files
        buffer = nameList;
        figurename = [commonPath,'\',buffer(1:max(strfind(buffer,'.txt'))-1)];                        
        saveas(figure(1), figurename, 'fig');        
        
        outputFileHandler = fopen([commonPath,'\OVERALL_RESULTS(threshold of ',num2str(thresholdDistance),').txt'],'w');
        fprintf(outputFileHandler,...
            '# singlets: %d, \n# doublets: %d, \n# triplets: %d, \n# multiplets: %d\n',...
            overallGroupList.singlets, overallGroupList.doublets, ...
            overallGroupList.triplets, overallGroupList.multiplets);        
        fprintf(outputFileHandler, 'Only one file processed.\n', numel(nameList));
        fclose(outputFileHandler);
        
    end