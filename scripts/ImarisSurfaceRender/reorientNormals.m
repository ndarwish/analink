function [faceNormals] = reorientNormals(faceNormals, connectivityList, vertices)

    vertexCount = length(vertices);

    for vertexInd = 1:vertexCount
        [assocTriangles1,~] = find(connectivityList(:,1) == vertexInd);
        [assocTriangles2,~] = find(connectivityList(:,2) == vertexInd);
        [assocTriangles3,~] = find(connectivityList(:,3) == vertexInd);
assocTriangles = cat(1, assocTriangles1, assocTriangles2, assocTriangles3);
%         [assocTriangles,positions] = find((connectivityList(:,1) == vertexInd) | ...
%                                   (connectivityList(:,2) == vertexInd) | ...
%                                   (connectivityList(:,3) == vertexInd));

        assocConunt = length(assocTriangles);

        avgnormal = [sum(faceNormals(assocTriangles,1)),...
                     sum(faceNormals(assocTriangles,2)),...
                     sum(faceNormals(assocTriangles,3))];
        
        % assign averaged normal to associated triangles
        for triangInd = 1:assocConunt
            
            sign = faceNormals(triangInd,1)*avgnormal(1)*...
                   faceNormals(triangInd,2)*avgnormal(2)*...
                   faceNormals(triangInd,3)*avgnormal(3);
            
            if(sign<0)
                faceNormals(triangInd,:) = -faceNormals(triangInd,:);
            end         
        end    
    end

end
