function [vSurpassScene] = buildImarisScene(vImarisApplication,rangeX,rangeY,rangeZ)

    %%% Create a new dataset and visualize it by Imaris
    vDataSet = vImarisApplication.GetFactory.CreateDataSet;
    vDataSet.Create(Imaris.tType.eTypeUInt8,rangeX,rangeY,rangeZ,1,1);
    vImarisApplication.SetDataSet(vDataSet);
    
    vSurpassScene = vImarisApplication.GetFactory.CreateDataContainer;
    vImarisApplication.SetSurpassScene(vSurpassScene);    
        
    vSurpassScene.SetName('Scene');
    % Add a light source
    vLightSource = vImarisApplication.GetFactory.CreateLightSource;
    vLightSource.SetName('Light source');
    % Add a frame (otherwise no 3D rendering)
    vFrame = vImarisApplication.GetFactory.CreateFrame;
    vFrame.SetName(strcat('Frame'));
    % Add a Volume (otherwise no 3D rendering)
    vVolume = vImarisApplication.GetFactory.CreateVolume;
    vVolume.SetName(strcat('Volume'));

    % Set up the surpass scene
    vSurpassScene.AddChild(vLightSource, -1);
    vSurpassScene.AddChild(vFrame, -1);

end