function [] = buildSurface(objectInd, vImarisApplication, vSurpassScene)

    global RunningParameters
    global ApplicationHandles
    global InputImage InputFile
    

    % These two lines set the focus to the current image:
    RunningParameters.SelectedFile    = objectInd;
    RunningParameters.SelectedDisplay = objectInd;

    % These three lines are for bringing the focus to the 
    % corresponding display window:
%     set(ApplicationHandles.ViewerWindow(objectInd),...
%         'Selected', 'off');
%     drawnow;
    set(ApplicationHandles.ViewerWindow(objectInd)...
        , 'Selected', 'on');

    % The EM images have incomplete metadata:
    InputImage(objectInd).PixelWidth  = 7;
    InputImage(objectInd).PixelHeight = 7;
    InputImage(objectInd).VoxelDepth  = 70;

    % units are nm, although Imaris handles them as microns. Life is not
    % perfect...

    pageCount = InputImage(objectInd).PageNumber;
    order = InputImage(objectInd).StackOrder;

    indZ =  strfind(order,'Z');
    indT =  strfind(order,'T');
    indC =  strfind(order,'C');

    vProgressDisplay = waitbar(0, 'Processing stack');                     % create waitbar

    channelCount = InputImage(objectInd).ChannelNumber;
    frameCount   = InputImage(objectInd).FrameNumber;
    sliceCount   = InputImage(objectInd).SliceNumber;


    for frame = 1:frameCount
        cIndex(indT-2) = frame;
            timeIndex = frame-1;                                           % Imaris starts counting from 0
        for channel = 1:channelCount
            cIndex(indC-2) = channel;
            vertexSet = [];
            for slice = 1:sliceCount
                cIndex(indZ-2) = slice;
                page = InputImage(objectInd).IndexMatrix(cIndex(1),cIndex(2),cIndex(3));
                if(RunningParameters.RAM == 1)
                    block(:,:) = InputImage(objectInd).Hyperstack(:,:,page);
                elseif(RunningParameters.RAM == 0) 
                    block(:,:) = imread([InputFile(objectInd).Path,'\',InputFile(objectInd).Name],'Index',page);
                end
                [~,vertexSubset] = findBorders(block);
                if (~isempty(vertexSubset))

                    [sortedVertices, mengerCurvature, objectList] = curveFeatures(vertexSubset);

                    [appendLength, ~] = size(sortedVertices);
                    vertexSubsetApender = ones(appendLength,1);

                    if(appendLength > 0)
                        % add the new components as fields after
                        appendix = cat(2,sortedVertices(:,1),sortedVertices(:,2),...
                                       slice*vertexSubsetApender,...
                                       frame*vertexSubsetApender,...
                                       channel*vertexSubsetApender);
                        % add the new elements as rows below
                        vertexSet = cat(1,vertexSet, appendix);
                    end
                end                                
                
                waitbar(page/pageCount,vProgressDisplay,sprintf('File page %d of %d',page,pageCount));
                
            end
            
            % Current stack processed; now show the last slice and send the
            % surface to Imaris
            
            imshow(block,'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
                        
            if(~isempty(vertexSet))
                
                % Set the appropriate sizes:

                vertexSet(:,1) = InputImage(objectInd).PixelWidth * vertexSet(:,1);
                vertexSet(:,2) = InputImage(objectInd).PixelHeight * vertexSet(:,2);
                vertexSet(:,3) = InputImage(objectInd).VoxelDepth * vertexSet(:,3);

                [triangles, faceNormals] = buildTriangles(vertexSet(:,:,:,frame,channel));
                vertices = vertexSet(:,1:3);

                addSurfacesToImaris(vImarisApplication, vSurpassScene, triangles, vertices, faceNormals, timeIndex);

            end
        end
    end

    close(vProgressDisplay);                                               % Close the progress bar        

end