function [vImarisApplication, vSurpassScene, ImarisPath] = prepareImaris(rangeX, rangeY, rangeZ)

%     global RunningParameters

    % Now we find Imaris automatically:    
    [ImarisPath, ~] = findPath('C:\Program Files\Bitplane', 'Imaris.exe');

    % ************************** START IMARIS *****************************

    aObjectId = 101;                                                       % Set an Id

    syscall = ['! "',ImarisPath,'\Imaris.exe" ',' id',num2str(aObjectId), ' &'];
    eval(syscall);
    % *********************************************************************

    % Java library ........................................................
    
    % before Matlab 2015 the Javaaddpath command deletes the global workspace 
    javaaddpath ImarisLib\ImarisLib.jar;
    vImarisLib = ImarisLib;                                                

    % Stablish the connection (function below)
    timeout = 10;

    vImarisApplication = connectToImaris(vImarisLib, aObjectId, timeout);

    % Then we create the scene elements needed to accomodate the new
    % surfaces (again, see function below)

    vSurpassScene = buildImarisScene(vImarisApplication,rangeX,rangeY,rangeZ);

end