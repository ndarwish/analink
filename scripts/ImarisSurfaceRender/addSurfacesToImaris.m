function [] = addSurfacesToImaris(vImarisApplication, vSurpassScene, triangles, vertices, vertexNormals, timeIndex)
    vResult = vImarisApplication.GetFactory.CreateSurfaces;
    vResult.SetVisible(1);
    
    % The triangles have to be RESORTED (clockwise to counterclockwise (!) and
    % the indices start at 0:
    ImarisTriangles = triangles(:, [1, 3, 2]) - 1;

    % Somehow removing vertices brings triangles with coinciding vertices.
    % Removing them doesn't harm.
    voids12 = ImarisTriangles(:,1)==ImarisTriangles(:,2);
    ImarisTriangles(voids12, :) = [];
    voids13 = ImarisTriangles(:,1)==ImarisTriangles(:,3);
    ImarisTriangles(voids13, :) = [];
    voids23 = ImarisTriangles(:,2)==ImarisTriangles(:,3);
    ImarisTriangles(voids23, :) = [];

    vResult.AddSurface(vertices, ImarisTriangles, vertexNormals, timeIndex);
    vResult.SetColorRGBA(1*255*1);

    vSurpassScene.AddChild(vResult, -1);

end
