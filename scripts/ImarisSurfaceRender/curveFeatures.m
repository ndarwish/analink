
function [highCurvatureVertices, largeCurvatures, objectList] = curveFeatures(vertexSubset)

    vertexCount = length(vertexSubset(:,1));
    mengerCurvature = zeros(vertexCount, 1);
    distanceToPrevious = zeros(vertexCount, 1);
    
    
    % number of points used at each side to calculate the curvature:
    delta = 4;
    
    if(vertexCount > (2*delta+1))
        %STEP 1: ASSOCIATION AND SORTING
        
        objectList = zeros(vertexCount,1);
        ObjectInd = 1;
        objectList(1) = ObjectInd;
        sortedVertices = zeros(size(vertexSubset));
        sortedVertices(1,:) = vertexSubset(1,:);
        vertexSubset(1,:) = [];
        for vertexInd = 1:vertexCount-1
            % distances between current and next elements:
            distances = bsxfun(@minus, sortedVertices(vertexInd,1:2),...
                                 vertexSubset(:,1:2));
            sqrDistances = sum(distances .* distances,2);
            closest = min(sqrDistances);
            neigborLocations = find(sqrDistances == closest);

            if(closest > 2)
                ObjectInd = ObjectInd + 1;
            end
            objectList(vertexInd+1) = ObjectInd;
            sortedVertices(vertexInd+1,:) = vertexSubset(neigborLocations(1),:);
            vertexSubset(neigborLocations(1),:) = [];
        end

        % STEP 1: REMOVE DUPLICATES
        blackList = [];
        for vertexInd = 1:vertexCount
            currentX = sortedVertices(vertexInd,1);
            currentY = sortedVertices(vertexInd,2);

            duplicates = find((sortedVertices(:,1) == currentX) & (sortedVertices(:,2) == currentY));            
            blackList = cat(1, blackList, duplicates);
        end
        sortedVertices(duplicates,:) = [];
        objectList(duplicates) = [];
        mengerCurvature(duplicates) = [];
        distanceToPrevious(duplicates) = [];
        %STEP 3: CURVATURE
        vertexCount = length(sortedVertices(:,1));

        % first points:
        for vertexInd = 1:delta
            V1 = [sortedVertices(vertexInd+vertexCount-delta,1), sortedVertices(vertexInd+vertexCount-delta,2)];
            V2 = [sortedVertices(vertexInd,      1), sortedVertices(vertexInd,      2)];
            V3 = [sortedVertices(vertexInd+delta,1), sortedVertices(vertexInd+delta,2)];

            X = [V1(1),V2(1),V3(1)];
            Y = [V1(2),V2(2),V3(2)];

            mengerCurvature(vertexInd) = polyarea(X,Y)/(norm(V2-V1)*norm(V3-V2)*norm(V3-V1));
            distanceToPrevious(vertexInd) = norm(V2-V1);
        end
        % middle points:
        for vertexInd = delta+1:vertexCount-delta

            V1 = [sortedVertices(vertexInd-delta,1), sortedVertices(vertexInd-delta,2)];
            V2 = [sortedVertices(vertexInd,      1), sortedVertices(vertexInd,      2)];
            V3 = [sortedVertices(vertexInd+delta,1), sortedVertices(vertexInd+delta,2)];

            X = [V1(1), V2(1),V3(1)];
            Y = [V1(2), V2(2),V3(2)];            

            mengerCurvature(vertexInd) = polyarea(X,Y)/(norm(V2-V1)*norm(V3-V2)*norm(V3-V1));
            distanceToPrevious(vertexInd) = norm(V2-V1);            
        end        
        % last points:
        for vertexInd = vertexCount-delta+1:vertexCount
            V1 = [sortedVertices(vertexInd-delta,1), sortedVertices(vertexInd-delta,2)];
            V2 = [sortedVertices(vertexInd,      1), sortedVertices(vertexInd,      2)];
            V3 = [sortedVertices(vertexInd - vertexCount+delta ,1), sortedVertices(vertexInd - vertexCount+delta,2)];
            
            X = [V1(1),V2(1),V3(1)];
            Y = [V1(2),V2(2),V3(2)];

            mengerCurvature(vertexInd) = polyarea(X,Y)/(norm(V2-V1)*norm(V3-V2)*norm(V3-V1));
            distanceToPrevious(vertexInd) = norm(V2-V1);            
        end   

        % Remove unwanted vertices based on curvature:
        highCurvatureVertices = sortedVertices;
        largeCurvatures = mengerCurvature;
        shorterDistances = distanceToPrevious;
        lowCurvatureLocations = find(mengerCurvature < 0.01 );             % This number is empirical. Before 0.02
        blackList = lowCurvatureLocations;
        
        highCurvatureVertices(blackList,:) = [];
        largeCurvatures(blackList) = [];
        shorterDistances(blackList) = [];

        % Remove unwanted vertices based on distance:

        maximumAllowedDistance = 6;                                        % This number is also empirical before 12

        ld = length(shorterDistances);
        
        blackList = [];
       
        for vertexInd = 1:ld-1
            if(shorterDistances(vertexInd)<maximumAllowedDistance)
                shorterDistances(vertexInd+1) = shorterDistances(vertexInd) + shorterDistances(vertexInd+1);
                blackList = cat(1, blackList, vertexInd);
            end
        end
        if(shorterDistances(end)<maximumAllowedDistance)
            blackList = cat(1, blackList, vertexInd+1);
        end
        
        highCurvatureVertices(blackList,:) = [];
        largeCurvatures(blackList) = [];
        shorterDistances(blackList) = [];

% figure(88)
% scatter(highCurvatureVertices(:,1),highCurvatureVertices(:,2),[],shorterDistances)
% 
% figure(89)
% scatter(highCurvatureVertices(:,1),highCurvatureVertices(:,2),[],largeCurvatures)

    else
        % too small group, rejected
        highCurvatureVertices = [];
        largeCurvatures = [];
        objectList = [];
    end

end