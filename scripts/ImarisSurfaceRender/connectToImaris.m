function [vImarisApplication] = connectToImaris(vImarisLib, aObjectId, timeout)
    
    global RunningParameters
    
    fprintf(1,'connecting...');
    for counter = timeout:-1:0
        
        fprintf(1,'%d',counter);
        
        aImarisApplicationID = vImarisLib.GetApplication(aObjectId);       % Opened Imaris instance
        if (~isempty(aImarisApplicationID))
            disp(char(8)); % backspace character
            fprintf('\nConnection established correctly.\n');
            break
        end
        pause(0.1);
        disp(char(8)); % backspace character
    end
    if (isempty(aImarisApplicationID))
        fprintf('\nConnection to Imaris timed out.\n');        
        return;
    end
        
    % connect to Imaris interface
    if ~isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper');
      javaaddpath ImarisLib.jar
      vImarisLib = ImarisLib;
      if ischar(aImarisApplicationID)
        aImarisApplicationID = round(str2double(aImarisApplicationID));
      end
      vImarisApplication = vImarisLib.GetApplication(aImarisApplicationID);
    else
      vImarisApplication = aImarisApplicationID;
    end

end
