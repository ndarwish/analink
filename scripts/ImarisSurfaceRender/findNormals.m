function [faceNormals] = findNormals(connectivityList, vertices)

TriangleCount = length(connectivityList);

faceNormals = zeros(TriangleCount, 3);

    for triangInd = 1:TriangleCount
        v1 = connectivityList(triangInd, 1);
        v2 = connectivityList(triangInd, 2);
        v3 = connectivityList(triangInd, 3);

        V12 = vertices(v2,:) - vertices(v1,:);
        V13 = vertices(v3,:) - vertices(v1,:);

        N = cross(V12, V13);

        faceNormals(triangInd, :) = N /norm(N);

    end

end
