function [] = QuickAndDirty()

    global RunningParameters
    global ApplicationHandles
    global InputImage InputFile

    % The EM images have incomplete metadata:

    InputImage(RunningParameters.SelectedFile).PixelWidth  = 7;
    InputImage(RunningParameters.SelectedFile).PixelHeight = 7;
    InputImage(RunningParameters.SelectedFile).VoxelDepth  = 70;

    % units are nm, although Imaris handles them as microns. Life is not
    % perfect...

    pageCount = InputImage(RunningParameters.SelectedFile).PageNumber;
    order = InputImage(RunningParameters.SelectedFile).StackOrder;

    indZ =  strfind(order,'Z');
    indT =  strfind(order,'T');
    indC =  strfind(order,'C');

    % This is the theoretical way to do it, but the empty scene occupies too
    % many voxels:

    % rangeX = InputImage(RunningParameters.SelectedFile).SizeX *...
    %          InputImage(RunningParameters.SelectedFile).PixelWidth;
    % rangeY = InputImage(RunningParameters.SelectedFile).SizeY *...
    %          InputImage(RunningParameters.SelectedFile).PixelHeight;
    % rangeZ = InputImage(RunningParameters.SelectedFile).SliceNumber*...
    %          InputImage(RunningParameters.SelectedFile).VoxelDepth;

    % And this is the dirty way. There's no problem allocating objects further
    % than the original scene.

    rangeX = 10;
    rangeY = 10;
    rangeZ = 10;

    [vImarisApplication, vSurpassScene, ImarisPath] = prepareImaris(rangeX, rangeY, rangeZ);
    RunningParameters.ImarisPath = ImarisPath;

    vProgressDisplay = waitbar(0, 'Processing stack');                     % create waitbar

    channelCount = InputImage(RunningParameters.SelectedFile).ChannelNumber;
    frameCount   = InputImage(RunningParameters.SelectedFile).FrameNumber;
    sliceCount   = InputImage(RunningParameters.SelectedFile).SliceNumber;


    for frame = 1:frameCount
        cIndex(indT-2) = frame;
            timeIndex = frame-1;                                       % Imaris starts counting from 0
        for channel = 1:channelCount
            cIndex(indC-2) = channel;
            vertexSet = [];
            for slice = 1:sliceCount
                cIndex(indZ-2) = slice;
                page = InputImage(RunningParameters.SelectedFile).IndexMatrix(cIndex(1),cIndex(2),cIndex(3));
                if(RunningParameters.RAM == 1)
                    block(:,:) = InputImage(RunningParameters.SelectedFile).Hyperstack(:,:,page);
                elseif(RunningParameters.RAM == 0)
                    block(:,:) = imread([InputFile(RunningParameters.SelectedFile).Path,'\',InputFile(RunningParameters.SelectedFile).Name],'Index',page);
                end
                [~,vertexSubset] = findBorders(block);
                if (~isempty(vertexSubset))

                    [sortedVertices, mengerCurvature, objectList] = curveFeatures(vertexSubset);

                    [appendLength, ~] = size(sortedVertices);
                    vertexSubsetApender = ones(appendLength,1);

                    if(appendLength > 0)
                        % add the new components as fields after
                        appendix = cat(2,sortedVertices(:,1),sortedVertices(:,2),...
                                       slice*vertexSubsetApender,...
                                       frame*vertexSubsetApender,...
                                       channel*vertexSubsetApender);
                        % add the new elements as rows below
                        vertexSet = cat(1,vertexSet, appendix);
                    end
                end                                
                
                waitbar(page/pageCount,vProgressDisplay,sprintf('File page %d of %d',page,pageCount));
                
            end
            
            % Current stack processed; now show the last slice and send the
            % surface to Imaris
            
            imshow(block,'Parent',ApplicationHandles.ViewerAxes(RunningParameters.SelectedDisplay));
                        
            if(~isempty(vertexSet))
                
                % Set the appropriate sizes:

                vertexSet(:,1) = InputImage(RunningParameters.SelectedFile).PixelWidth * vertexSet(:,1);
                vertexSet(:,2) = InputImage(RunningParameters.SelectedFile).PixelHeight * vertexSet(:,2);
                vertexSet(:,3) = InputImage(RunningParameters.SelectedFile).VoxelDepth * vertexSet(:,3);

                [triangles, FaceNormals] = buildTriangles(vertexSet(:,:,:,frame,channel));
                vertices = vertexSet(:,1:3);

                addSurfacesToImaris(vImarisApplication, vSurpassScene, triangles, vertices, FaceNormals, timeIndex);

            end
        end
    end

    close(vProgressDisplay);                                               % Close the progress bar        
    
    fprintf('\nFinished!\n');
end













