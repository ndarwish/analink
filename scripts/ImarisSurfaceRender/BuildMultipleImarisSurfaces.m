function [] = BuildMultipleImarisSurfaces()

    global RunningParameters
    global InputImage

    % This is the theoretical way to do it, but the empty scene occupies too
    % many voxels:

    % rangeX = InputImage(objectInd).SizeX *...
    %          InputImage(objectInd).PixelWidth;
    % rangeY = InputImage(objectInd).SizeY *...
    %          InputImage(objectInd).PixelHeight;
    % rangeZ = InputImage(objectInd).SliceNumber*...
    %          InputImage(objectInd).VoxelDepth;

    % And this is the dirty way. There's no problem allocating objects further
    % than the original scene.

    rangeX = 10;
    rangeY = 10;
    rangeZ = 10;

    [vImarisApplication, vSurpassScene, ImarisPath] = prepareImaris(rangeX, rangeY, rangeZ);
    RunningParameters.ImarisPath = ImarisPath;    
    
    
    objectCount = length(InputImage);
    
    for objectInd = 1:objectCount
        buildSurface(objectInd, vImarisApplication, vSurpassScene)
    end        
    
    fprintf('\nFinished!\n');
end


