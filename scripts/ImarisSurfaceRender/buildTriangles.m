
function [connectivityList, FaceNormals] = buildTriangles(compSubset)

    global RunningParameters
    global InputImage
    
    % The vertexSet array contains one row per vertex, with the following
    % columns: [ X Y Z T C ]

    sliceCount   = InputImage(RunningParameters.SelectedFile).SliceNumber;
    
    allowedSliceSeparationPix = 100;
    allowedXYSeparation    = allowedSliceSeparationPix * InputImage(RunningParameters.SelectedFile).PixelWidth;
    allowedXYSeparation2 = allowedXYSeparation^2;        
     
    connectivityList = [];
    
    for slice = 1:sliceCount-1

        lowVal  = (slice)* InputImage(RunningParameters.SelectedFile).VoxelDepth;
        highVal = (slice+1)* InputImage(RunningParameters.SelectedFile).VoxelDepth;

        verticesCurrentSlice = find(compSubset(:,3)== lowVal);
        verticesNextSlice    = find(compSubset(:,3)== highVal);

        currentSliceGroup = compSubset(verticesCurrentSlice,:);
        nextSliceGroup    = compSubset(verticesNextSlice,   :);                

        consideredVertices = cat(1,verticesCurrentSlice,verticesNextSlice);

        currentSliceCount  = length(verticesCurrentSlice);
        localVertexCount   = length(consideredVertices);

        % current slice:
        for currentIndex = 1:currentSliceCount-1
            % On the same plane:
            vertex1 = consideredVertices(currentIndex);
            vertex2 = verticesCurrentSlice(currentIndex + 1);

            P1 = compSubset(vertex1,:);
            P2 = compSubset(vertex2,:);
            distanceP1P2 = sum((P2(1:2)-P1(1:2)).*(P2(1:2)-P1(1:2)));


            % to the next plane:
            distances1 = bsxfun(@minus, P1, nextSliceGroup);
            sqrDistances1 = sum(distances1 .* distances1,2);
            nonNulls1 = sqrDistances1 > 0;
            min1 = min(sqrDistances1(nonNulls1));

            distances2 = bsxfun(@minus, P2, nextSliceGroup);
            sqrDistances2 = sum(distances2 .* distances2,2);
            nonNulls2 = sqrDistances2 > 0;
            min2 = min(sqrDistances2(nonNulls2));

            if(isempty(min1) || isempty(min2))
                vertex3 = [];
            elseif(distanceP1P2>allowedXYSeparation2)
                vertex3 = [];
            elseif(min(min1,min2)>allowedXYSeparation2)
                vertex3 = [];
            elseif(min1<min2)
                mp1 = find(sqrDistances1(nonNulls1) == min1, 1);
                vertex3 = verticesNextSlice(mp1(1));
                newTriangle = [vertex1, vertex2, vertex3];
                connectivityList = cat(1,connectivityList, newTriangle);                        
            else
                mp2 = find(sqrDistances2(nonNulls2) == min2);
                vertex3 = verticesNextSlice(mp2(1));
                newTriangle = [vertex1, vertex2, vertex3];
                connectivityList = cat(1,connectivityList, newTriangle);                        
            end
            % Complementary triangles filling the gaps:

            if(currentIndex>1 && ~isempty(vertex3) && ~isempty(Cvertex1) && ~isempty(Cvertex3))
                Cvertex2 = vertex3;
                P1 = compSubset(Cvertex1,:);
                P2 = compSubset(Cvertex2,:);
                P3 = compSubset(Cvertex3,:);
                sqD12 = sum((P2(1:2)-P1(1:2)).*(P2(1:2)-P1(1:2)));
                sqD13 = sum((P3(1:2)-P1(1:2)).*(P3(1:2)-P1(1:2)));
                sqD23 = sum((P3(1:2)-P2(1:2)).*(P3(1:2)-P2(1:2)));
                sideLengths = [sqD12,sqD13,sqD23];
                if(max(sideLengths)<allowedXYSeparation2);
                    newTriangle = [Cvertex1, Cvertex2, Cvertex3];
                    connectivityList = cat(1,connectivityList, newTriangle);      
                end
            end
            Cvertex1 = vertex3;
            Cvertex3 = vertex2;
        end
        % next slice:
        for currentIndex = currentSliceCount+1:localVertexCount-1

            % On the same plane:
            vertex1 = consideredVertices(currentIndex);
            vertex2 = verticesNextSlice(currentIndex + 1-currentSliceCount);

            P1 = compSubset(vertex1,:);
            P2 = compSubset(vertex2,:);
            distance = sum((P2(1:2)-P1(1:2)).*(P2(1:2)-P1(1:2)));

            % to the next plane:                    
            distances1 = bsxfun(@minus, P1, currentSliceGroup);
            sqrDistances1 = sum(distances1 .* distances1,2);
            nonNulls1 = sqrDistances1 > 0;
            min1 = min(sqrDistances1(nonNulls1));                                                

            distances2 = bsxfun(@minus, P2, currentSliceGroup);
            sqrDistances2 = sum(distances2 .* distances2,2);
            nonNulls2 = sqrDistances2 > 0;
            min2 = min(sqrDistances2(nonNulls2));

            if(isempty(min1) || isempty(min2))                        
                vertex3 = [];
            elseif(distance>allowedXYSeparation2)
                vertex3 = [];
            elseif(min(min1,min2)>allowedXYSeparation2)
                vertex3 = [];
            elseif(min1<min2)
                mp1 = find(sqrDistances1(nonNulls1) == min1, 1);
                vertex3 = verticesCurrentSlice(mp1(1));
                newTriangle = [vertex1, vertex2, vertex3];
                connectivityList = cat(1,connectivityList, newTriangle);
            else
                mp2 = find(sqrDistances2(nonNulls2) == min2);
                vertex3 = verticesCurrentSlice(mp2(1));
                newTriangle = [vertex1, vertex2, vertex3];
                connectivityList = cat(1,connectivityList, newTriangle);                        
            end
        % Complementary triangles filling the gaps:
        if((currentIndex>currentSliceCount+1) && ~isempty(Cvertex1) && ~isempty(vertex3))
            Cvertex2 = vertex3;
            P1 = compSubset(Cvertex1,:);
            P2 = compSubset(Cvertex2,:);
            P3 = compSubset(Cvertex3,:);
            sqD12 = sum((P2(1:2)-P1(1:2)).*(P2(1:2)-P1(1:2)));
            sqD13 = sum((P3(1:2)-P1(1:2)).*(P3(1:2)-P1(1:2)));
            sqD23 = sum((P3(1:2)-P2(1:2)).*(P3(1:2)-P2(1:2)));
            sideLengths = [sqD12,sqD13,sqD23];
            if(max(sideLengths)<allowedXYSeparation2);
                newTriangle = [Cvertex1, Cvertex2, Cvertex3];
                connectivityList = cat(1,connectivityList, newTriangle);      
            end                    
        end
        Cvertex1 = vertex3;
        Cvertex3 = vertex2;
        end                
    end

    
    
    % A smoothing algorithm from matlab exchange:
%     compSubset(:,1:3) = lpflow_trismooth(compSubset(:,1:3),connectivityList);
    
    
    % After the connectivity list we run a standard triangulation to get
    % the normals.
    
    % Matlab triangulation method:
%     tr = triangulation(connectivityList, compSubset(:,1:3));
%     faceNormals = faceNormal(tr);
    
    
% 
%     
    % Alternate method:
    [FaceNormals] = findNormals(connectivityList, compSubset(:,1:3));


%     % Normals might be randomly oriented:
     [FaceNormals] = reorientNormals(FaceNormals, connectivityList, compSubset(:,1:3));

     
     % Averaging normals smoothens the surfaces
[facecount, ~] = size(FaceNormals);
     
for k = 2:facecount -1
    FaceNormals(k,1) = mean(FaceNormals(k-1:k+1,1));
    FaceNormals(k,2) = mean(FaceNormals(k-1:k+1,2));
    FaceNormals(k,3) = mean(FaceNormals(k-1:k+1,3));
FaceNormals(k,:) = FaceNormals(k,:)/ norm(FaceNormals(k,:));
end
     

    figure(13)
    axis equal;
    hold on;
    trisurf(connectivityList,compSubset(:,1),compSubset(:,2),compSubset(:,3), ...
         'FaceColor', 'cyan', 'faceAlpha', 0.8);
%     scatter3(compSubset(:,1),compSubset(:,2),compSubset(:,3));
% %     Display the normal vectors on the surface.
    quiver3(compSubset(connectivityList(:),1),compSubset(connectivityList(:),2),compSubset(connectivityList(:),3), ...
            FaceNormals(connectivityList(:),1),FaceNormals(connectivityList(:),2),FaceNormals(connectivityList(:),3),0.5,...
            'color','r');
end