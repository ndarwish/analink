function [layerShell,vertexSubset] = findBorders(inputBinaryImage)

    layerShell = zeros(size(inputBinaryImage));    
    edgeImage = bwboundaries(inputBinaryImage);

    numberOfObjects = length(edgeImage);
    vertexSubsetCount = 0;
    sz = zeros(numberOfObjects,1);
    for ind = 1:numberOfObjects
        [sz(ind), ~] = size(edgeImage{ind});
        vertexSubsetCount = vertexSubsetCount + sz(ind);
    end
    
    if (vertexSubsetCount>0)
        vertexSubset = zeros(vertexSubsetCount,2);

        startInd = 1;
        for ind = 1:numberOfObjects        
            buff = edgeImage{ind};
            vertexSubset(startInd:startInd+sz(ind)-1,:) = buff;
            startInd = startInd + sz(ind);
            for ind2 = 1:sz(ind)
                layerShell(buff(ind2,1),buff(ind2,2)) = 1;
            end

        end
    else
        vertexSubset = [];
    end

end